{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_image",
  "bbox_bottom": 31,
  "bbox_left": 0,
  "bbox_right": 31,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": true,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"ca6e7e49-6db6-4dfd-b949-a704e5b98087",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 32,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"8ab472b3-3bba-4f21-a589-baeb61f962cc","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": {
    "resourceType": "GMNineSliceData",
    "resourceVersion": "1.0",
    "bottom": 0,
    "enabled": false,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "left": 0,
    "right": 0,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "top": 0,
  },
  "origin": 4,
  "parent": {
    "name": "Png",
    "path": "folders/GUI_System/Asset/Sprites/Png.yy",
  },
  "preMultiplyAlpha": true,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_image",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 1.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"ca6e7e49-6db6-4dfd-b949-a704e5b98087","path":"sprites/spr_image/spr_image.yy",},},},"Disabled":false,"id":"29f93f42-0e0a-42b6-836b-88c1a8f503fb","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 16,
    "yorigin": 16,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 32,
}