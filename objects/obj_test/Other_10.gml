
a_time = 5; // game_get_speed(gamespeed_fps) * 0.02;
alarm[0] = a_time;

var _h = 800;
var _r = GG_DISPLAY_RATIO;
var _wm = GG_WINDOW_IS.WINDOWED;

gg_screen_adaptation(_h * _r, _h, _wm, false);
show_debug_overlay(true);
gc_enable(true);
randomize();
draw_set_font(gg_def_fonts.mono_family);

cursor_sprite = gg_def_sprites.cursor;
