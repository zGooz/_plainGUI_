
event_user(0);


#region editor init

_ox = 400;
_oy = 300;
_etalon = string_repeat("Q", GG_TEXT_LENGTH.DEFAULT);
_etalon_w = string_width(_etalon) * 2;
_etalon_h = string_height(_etalon) * 12;

transform = new GgTransform();
transform.set_position(_ox, _oy);
transform.resize(_etalon_w, _etalon_h);
transform.build();
transform.recalculate_shape_property();

area = new GgWorkArea(transform);

txt_block = new GgTextBlock(transform);
txt_block.format.string_width = _etalon_w;
txt_block.brush.color = c_black;

editor = new GgTextEditor(area, txt_block);

var _s = 
@"Переехать уголь кот мобильник
книга лягушка маринованные огурцы
красная плесень матрешка чебурашка холодильник репа магазин ёж
лужа снег комбала язык колба кружка свадьба пикник
вторник свадьба пикник
книга лягушка маринованные огурцы
синее половодье матрешка морг пробирка
холодильник магазин ёж
йогурт снег сугробы язык программирования
кружка свадьба пикник";

_s = string_replace_all(_s, GG_CRLF, GG_LF);
var _len = string_length(_s);

editor.max_length = _len;
editor.length = _len;
editor.bearing_cursor.set(_len + 1, editor.max_length + 1);
editor.type_cursor.set(_len + 1, editor.max_length + 1);

txt_block.set_text(_s);
editor.buffer.update(_s);

global.temp_text = ""

#endregion editor init
