
/// @func GgEditPointer()
///
/// @desc
///    A component that is used by the textbox engine
///    as a cursor and/or special reference pointer.
///
/// @context undefined
///
function GgEditPointer() constructor
{
    #region methods

    #region public

    /// @func set_position(_pos, _total)
    ///
    /// @desc
    ///    Sets the cursor|pointer position in the line.
    ///
    /// @arg {Real} _pos
    ///    Position in line (starting at 1)
    ///
    /// @arg {Real} _total
    ///    The position of the last character in the string.
    ///
    /// @context GgEditPointer
    ///
    static set_position = function(_pos, _total)
    {
        position = clamp(_pos, 1, _total + 1);
    }

    /// @func to_left(_value)
    ///
    /// @desc
    ///    Shifts the cursor to the left by
    ///    the amount specified in the argument.
    ///
    /// @arg {Real} _value
    ///    Shift amount.
    ///
    /// @context GgEditPointer
    ///
    static to_left = function(_value)
    {
        position = max(position - _value, 1);
    }

    /// @func to_right(_value, _total)
    ///
    /// @desc
    ///    Shifts the cursor to the right by
    ///    the amount specified in the argument.
    ///
    /// @arg {Real} _value
    ///    Shift amount.
    ///
    /// @arg {Real} _total
    ///    The position of the last character in the string.
    ///
    /// @context GgEditPointer
    ///
    static to_right = function(_value, _total)
    {
        position = min(position + _value, _total + 1);
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    position = 1;

    #endregion public

    #endregion variables
}
