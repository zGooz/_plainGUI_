
/// @func GgTextbox()
function GgTextbox(): GgWidget() constructor
{
#region methods

#region public



#endregion public

#region private



#endregion private

#endregion methods

#region variables

#region public



#endregion public

#region private



#endregion private

#endregion variables

    /// @ignore
    length = 0;

    /// @ignore
    max_length = GG_TEXT_LENGTH.DEFAULT;

    /// @ignore
    bearing_cursor = new GgEditPointer();

    /// @ignore
    cursor = new GgEditPointer();

    /// @ignore
    block = new GgBlock();

    /// @ignore
    text = "";

    /// @ignore
    chars = new GgCharsProvider();
    chars.give_new_row_symbol = false;
    chars.give_tab_symbol = false;

    /// @ignore
    commands = new GgCommands();

    is_overwrite = false;
    delay = 0.8;

    /// @ignore
    static delete_fragment = function()
    {
        text = string_delete(text, block.position, block.count);
        bearing_cursor.set_position(block.position, length);
        cursor.set_position(block.position, length);
        block.reset();
        length = string_length(text);
    }

    /// @ignore
    static get_anchor_pos = function()
    {
        var _pointer = block.position < bearing_cursor.position
                     ? block.position
                     : block.position + block.count;

        return block.has_select()
               ? _pointer
               : bearing_cursor.position;
    }

    /// @ignore
    static try_sym_offseted_at = function(_symbol, _pos)
    {
        var _char = string_char_at(text, _pos - 1);
        return _char == _symbol;
    }

    /// @ignore
    static try_sym_at = function(_symbol, _pos)
    {
        var _char = string_char_at(text, _pos);
        return _char == _symbol;
    }

    /// @ignore
    static is_sym_space_pair = function()
    {
        var _pos = get_anchor_pos();
        var _char_place = string_char_at(text, _pos - 1);
        var _space_place = string_char_at(text, _pos - 2);
        var _new_line_symbol = chars.new_row_symbol;

        return _char_place != _new_line_symbol
            && _char_place != " "
            && _space_place == " ";
    }

    /// @ignore
    static is_space_char_pair = function()
    {
        var _pos = get_anchor_pos();
        var _char_place = string_char_at(text, _pos + 1);
        var _space_place = string_char_at(text, _pos);
        var _new_line_symbol = chars.new_row_symbol;

        return _char_place != _new_line_symbol
            && _char_place != " "
            && _space_place == " ";
    }

    /// @ignore
    static dist_to_left_word_or_row = function(_symbol)
    {
        var _pos = get_anchor_pos();
        var _anchor = _pos;

        while try_sym_offseted_at(_symbol, _pos)
        {
            if _pos == 1
            {
                break;
            }

            _pos -= 1;
        }

        _pos = string_last_pos_ext(_symbol, text, _pos - 1);

        return max(0, _anchor - _pos - 1);
    }

    /// @ignore
    static dist_to_right_word_or_row = function(_symbol)
    {
        var _pos = get_anchor_pos();
        var _anchor = _pos;

        if string_char_at(text, _pos + 1) != _symbol
        {
            _pos = string_pos_ext(_symbol, text, _pos + 1);

            if _pos == 0
            {
                _pos = length + 1;
            }
        }

        do
        {
            if _pos > length
            {
                _pos = length + 1;
                break;
            }

            _pos += 1;
        }
        until ! try_sym_at(_symbol, _pos);

        return max(0, _pos - _anchor);
    }

    /// @ignore
    static dist_to_left_free_place_for_cursor = function()
    {
        var _new_line_symbol = chars.new_row_symbol;
        var _pos = get_anchor_pos();

        if try_sym_offseted_at(_new_line_symbol, _pos)
        {
            return 1;
        }
        else
        {
            if is_sym_space_pair()
            {
                return 1;
            }
            else
            {
                var _row_end_dist = dist_to_left_word_or_row(_new_line_symbol);
                var _space_dist = dist_to_left_word_or_row(" ");
                var _short = min(_space_dist, _row_end_dist);

                return _short;
            }
        }
    }

    /// @ignore
    static dist_to_right_free_place_for_cursor = function()
    {
        var _new_line_symbol = chars.new_row_symbol;
        var _pos = get_anchor_pos();

        if try_sym_at(_new_line_symbol, _pos)
        {
            return 1;
        }
        else
        {
            if is_space_char_pair()
            {
                return 1;
            }
            else
            {
                var _row_end_dist = dist_to_right_word_or_row(_new_line_symbol);
                var _space_dist = dist_to_right_word_or_row(" ");
                var _short = min(_space_dist, _row_end_dist);

                if _row_end_dist == _short
                {
                    if _pos + _short != length + 1
                    {
                        _short -= 1;
                    }
                }

                return _short;
            }
        }
    }

    /// @ignore
    static select_left_symbols = function(_count)
    {
        if block.has_select()
        {
            var _block_pos = block.position;

            if _block_pos == 1
            {
                return;
            }

            if _block_pos < bearing_cursor.position
            {
                var _pos = max(1, _block_pos - _count);
                select_text(_pos, _count);
            }
            else
            {
                block.collapse(_count);
            }
        }
        else
        {
            var _pos = bearing_cursor.position - _count;
            select_text(_pos, _count);
        }
    }

    /// @ignore
    static select_right_symbols = function(_count)
    {
        if block.has_select()
        {
            if block.position < bearing_cursor.position
            {
                var _new_block_pos = block.position + _count;

                block.set_position(_new_block_pos, length);
                block.collapse(_count);
            }
            else
            {
                block.expand(_count, length);
            }
        }
        else
        {
            select_text(bearing_cursor.position, _count);
        }      
    }

    /// @ignore
    static on_left = function()
    {
        if block.has_select()
        {
            bearing_cursor.set_position(block.position, length);
            cursor.set_position(block.position, length);
        }
        else
        {
            bearing_cursor.to_left(1);
            cursor.to_left(1);
        }

        block.reset();
    }

    /// @ignore
    static on_right = function()
    {
        if block.has_select()
        {
            var _block_pos = block.position;
            var _block_count = block.count;
            var _pos = _block_pos + _block_count;

            bearing_cursor.set_position(_pos, length);
            cursor.set_position(_pos, length);
        }
        else
        {
            bearing_cursor.to_right(1, length);
            cursor.to_right(1, length);
        }

        block.reset();
    }

    /// @ignore
    static on_control_left = function()
    {
        var _distance = dist_to_left_free_place_for_cursor();

        bearing_cursor.to_left(_distance);
        cursor.set_position(bearing_cursor.position, length);
        block.reset();
    }

    /// @ignore
    static on_control_right = function()
    {
        var _distance = dist_to_right_free_place_for_cursor();

        bearing_cursor.to_right(_distance, length);
        cursor.set_position(bearing_cursor.position, length);
        block.reset();
    }

    /// @ignore
    static on_shift_left = function()
    {
        select_left_symbols(1);
        cursor.to_left(1);
    }

    /// @ignore
    static on_shift_right = function()
    {
        select_right_symbols(1);
        cursor.to_right(1, length);
    }

    /// @ignore
    static on_control_shift_left = function()
    {
        var _distance = dist_to_left_free_place_for_cursor();

        cursor.to_left(_distance);
        select_left_symbols(_distance);
    }

    /// @ignore
    static on_control_shift_right = function()
    {
        var _distance = dist_to_right_free_place_for_cursor();

        cursor.to_right(_distance, length);
        select_right_symbols(_distance);
    }

    /// @ignore
    static on_home = function()
    {
        var _new_line_symbol = chars.new_row_symbol;
        var _pos = get_anchor_pos();

        if _pos == 1
        {
            return;
        }

        if try_sym_offseted_at(_new_line_symbol, _pos)
        {
            return;
        }

        var _distance = dist_to_left_word_or_row(_new_line_symbol);
        var _new_pos = bearing_cursor.position - _distance;

        bearing_cursor.set_position(_new_pos, length);
        cursor.set_position(_new_pos, length);
        block.reset();
    }

    /// @ignore
    static on_end = function()
    {
        var _new_line_symbol = chars.new_row_symbol;
        var _pos = get_anchor_pos();

        if try_sym_at(_new_line_symbol, _pos)
        {
            return;
        }

        var _distance = dist_to_right_word_or_row(_new_line_symbol);
        var _new_pos = _pos + _distance - 1;

        bearing_cursor.set_position(_new_pos, length);
        cursor.set_position(_new_pos, length);
        block.reset();
    }

    /// @ignore
    static on_control_home = function()
    {
        bearing_cursor.set_position(1, length);
        cursor.set_position(1, length);
        block.reset();
    }

    /// @ignore
    static on_control_end = function()
    {
        bearing_cursor.to_right(infinity, length);
        cursor.to_right(infinity, length);
        block.reset();
    }

    /// @ignore
    static on_shift_home = function()
    {
        block.reset();

        var _pos = get_anchor_pos();
        var _new_line_symbol = chars.new_row_symbol;
        var _distance = dist_to_left_word_or_row(_new_line_symbol);
        var _new_pos = _pos - _distance;

        cursor.set_position(_new_pos, length);
        select_text(_new_pos, _pos - _new_pos);
    }

    /// @ignore
    static on_shift_end = function()
    {
        block.reset();

        var _pos = get_anchor_pos();
        var _new_line_symbol = chars.new_row_symbol;
        var _distance = dist_to_right_word_or_row(_new_line_symbol);
        var _new_pos = _pos + _distance - 1;

        cursor.set_position(_new_pos, length);
        select_text(_pos, _distance - 1);
    }

    /// @ignore
    static on_control_shift_home = function()
    {
        var _previous_pos = bearing_cursor.position;

        on_control_home();
        bearing_cursor.set_position(_previous_pos, length);
        select_text(1, _previous_pos);
    }

    /// @ignore
    static on_control_shift_end = function()
    {
        var _previous_pos = bearing_cursor.position + 1;

        on_control_end();
        bearing_cursor.set_position(_previous_pos, length);

        var _count = length - _previous_pos;
        select_text(_previous_pos, _count);
    }

    /// @ignore
    static on_backspace = function()
    {
        if block.has_select()
        {
            delete_fragment();
        }
        else
        {
            var _pos = bearing_cursor.position - 1;

            text = string_delete(text, _pos, 1);
            length = string_length(text);
            bearing_cursor.to_left(1);
            cursor.to_left(1);
        }
    }

    /// @ignore
    static on_delete = function()
    {
        if block.has_select()
        {
            delete_fragment();
        }
        else
        {
            var _pos = bearing_cursor.position;
            text = string_delete(text, _pos, 1);
            length = string_length(text);
        }
    }

    /// @ignore
    static on_control_backspace = function()
    {
        on_control_shift_left();
        delete_fragment();
    }

    /// @ignore
    static on_control_delete = function()
    {
        on_control_shift_right();
        delete_fragment();
    }

    /// @ignore
    static on_control_shift_backspace = function()
    {
        on_shift_home();
        delete_fragment();
    }

    /// @ignore
    static on_control_shift_delete = function()
    {
        on_shift_end();
        delete_fragment();
    }

    /// @ignore
    static on_select_all = function()
    {
        select_text(1, length);
    }

    /// @ignore
    static on_copy = function()
    {
        var _pos = block.position;
        var _count = block.count;
        var _text = string_copy(text, _pos, _count);

        clipboard_set_text(_text);
    }

    /// @ignore
    static on_paste = function()
    {
        if clipboard_has_text()
        {
            if block.has_select()
            {
                delete_fragment();
            }

            var _clip = clipboard_get_text();
            var _clip_length = string_length(_clip);
            var _pos = bearing_cursor.position;

            text = string_insert(_clip, text, _pos);
            length = string_length(text);

            if length > max_length
            {
                text = string_copy(text, 1, max_length);
            }

            length = string_length(text);
            bearing_cursor.to_right(_clip_length, length);
            cursor.to_right(_clip_length, length);
        }
    }

    /// @ignore
    static on_cut = function()
    {
        on_copy();
        delete_fragment();
    }

    /// @ignore
    static on_up = function()
    {
        //
    }

    /// @ignore
    static on_down = function()
    {
        //
    }

    /// @ignore
    static on_shift_up = function()
    {
        //
    }

    /// @ignore
    static on_shift_down = function()
    {
        //
    }

    commands.ev_up.subscribe (
        "to_up", method(self, on_up)
    );

    commands.ev_down.subscribe (
        "to_down", method(self, on_down)
    );

    commands.ev_shift_up.subscribe (
        "select_cur&up_rows_part",
        method(self, on_shift_up)
    );

    commands.ev_shift_down.subscribe (
        "select_cur&down_rows_part",
        method(self, on_shift_down)
    );

    commands.ev_left.subscribe (
        "to_left", method(self, on_left)
    );

    commands.ev_right.subscribe (
        "to_right", method(self, on_right)
    );

    commands.ev_control_left.subscribe (
        "to_left_at_word", method(self, on_control_left)
    );

    commands.ev_control_right.subscribe (
        "to_right_at_word", method(self, on_control_right)
    );

    commands.ev_shift_left.subscribe (
        "select_left_symbol", method(self, on_shift_left)
    );

    commands.ev_shift_right.subscribe (
        "select_right_symbol", method(self, on_shift_right)
    );

    commands.ev_control_shift_left.subscribe (
        "select_left_worl_part",
        method(self, on_control_shift_left)
    );

    commands.ev_control_shift_right.subscribe (
        "select_right_worl_part",
        method(self, on_control_shift_right)
    );

    commands.ev_home.subscribe (
        "to_start_row", method(self, on_home)
    );

    commands.ev_end.subscribe (
        "to_end_row", method(self, on_end)
    );

    commands.ev_shift_home.subscribe("select_left_row_part", self, on_shift_home);

    commands.ev_shift_end.subscribe("select_right_row_part", self, on_shift_end);

    commands.ev_control_home.subscribe("go_to_text_start", self, on_control_home);

    commands.ev_control_end.subscribe("go_to_text_end", self, on_control_end);

    commands.ev_control_shift_home.subscribe("select_left_text_part", self, on_control_shift_home);

    commands.ev_control_shift_end.subscribe("select_right_text_part", self, on_control_shift_end);

    commands.ev_shift_home.subscribe("select_left_text_part", self, on_shift_home);

    commands.ev_shift_end.subscribe("select_right_text_part", self, on_shift_end);

    commands.ev_backspace.subscribe("backspace", self, on_backspace);

    commands.ev_delete.subscribe("delete", self, on_delete);

    commands.ev_control_backspace.subscribe("clear_left_word_part", self, on_control_backspace);

    commands.ev_control_shift_backspace.subscribe("clear_left_text_part", self, on_control_shift_backspace);

    commands.ev_control_delete.subscribe("clear_right_word_part", self, on_control_delete);

    commands.ev_control_shift_delete.subscribe("clear_right_text_part", self, on_control_shift_delete);

    commands.ev_select_all.subscribe("select_all", self, on_select_all);

    commands.ev_cut.subscribe("cut", self, on_cut);

    commands.ev_copy.subscribe("copy", self, on_copy);

    commands.ev_paste.subscribe("paste", self, on_paste);

    /*
    type_command.ev_page_up.subscribe("", self, undefined);
    type_command.ev_page_down.subscribe("", self, undefined);
    type_command.ev_shift_page_up.subscribe("", self, undefined);
    type_command.ev_shift_page_down.subscribe("", self, undefined);
    */

    /// @ignore
    static type_char = function(_id)
    {
        if _id.length == _id.max_length
        {
            return;
        }

        var _char = _id.chars.provide();

        if _char != ""
        {
            if _id.block.has_select()
            {
                _id.delete_fragment();
                _id.length = string_length(_id.text);
            }
            else
            {
                if _id.is_overwrite
                {
                    var _pos = _id.bearing_cursor.position;
                    _id.text = string_delete(_id.text, _pos, 1);
                }
            }

            var _pos = _id.bearing_cursor.position;
            var _symbol_length = string_length(_char);

            _id.text = string_insert(_char, _id.text, _pos);
            _id.length = string_length(_id.text);
            _id.bearing_cursor.to_right(_symbol_length, _id.length);
            _id.cursor.to_right(_symbol_length, _id.length);
        }
    }

    /// @ignore
    timer = time_source_create (
        time_source_game, delay,
        time_source_units_seconds, type_char, [self], -1
    );

    /// @func select_text(_index, _count)
    /// @arg {Real} _index
    /// @arg {Real} _count
    static select_text = function(_index, _count)
    {
        block.set_position(_index, length);
        block.expand(_count, length);
    }

    /// @func get_fragment(_index, _count)
    /// @arg {Real} _index
    /// @arg {Real} _count
    static get_fragment = function(_index, _count)
    {
        return string_copy(text, _index, _count);
    }

    static work_process = function()
    {
        if keyboard_key == 0
        {
            time_source_stop(timer);
            type_time = 0;
        }
        else
        {
            var _state = time_source_get_state(timer);

            if _state != time_source_state_active
            {
                type_char(self);
                keyboard_lastkey = -1;
            }

            if _state == time_source_state_stopped
            || _state == time_source_state_initial
            {
                time_source_start(timer);
            }

            commands.monitor();

            var _simple_skin = property.skins.simple;
            var _text_block = _simple_skin.get("text_block");

            _text_block.text = text;
        }
    }

    static delete_resouces = function()
    {
        commands.free();
        delete commands;

        delete block;
        delete chars;
        delete bearing_cursor;
        delete cursor;
    }
}
