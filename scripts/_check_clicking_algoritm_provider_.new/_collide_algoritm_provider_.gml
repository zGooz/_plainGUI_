
globalvar PG_CollideAlgorithmProvider;
PG_CollideAlgorithmProvider = {
    PointInEllipce : {
        /// @func Provide() -> function
        Provide : function() {
            return function(d) {
                var dist_to_focus = sqrt(sqr(d.SemiMajorAxis) - sqr(d.SemiMinorAxis));
                var focus_1_x;
                var focus_1_y;
                var focus_2_x;
                var focus_2_y;
                var cx = d.Centr.get_x();
                var cy = d.Centr.get_y();
    
                if (d.Orientation == EllipceOrientation.HORIZONTAL) {
                    focus_1_x = cx - dist_to_focus;
                    focus_1_y = cy;
                    focus_2_x = cx + dist_to_focus;
                    focus_2_y = cy;
                } else if (d.Orientation == EllipceOrientation.VERTICAL) {
                    focus_1_x = cx;
                    focus_1_y = cy - dist_to_focus;
                    focus_2_x = cx;
                    focus_2_y = cy + dist_to_focus;
                }
    
                var dist_to_focus_1 = point_distance(focus_1_x, focus_1_y, MOUSE_GUI_X, MOUSE_GUI_Y);
                var dist_to_focus_2 = point_distance(focus_2_x, focus_2_y, MOUSE_GUI_X, MOUSE_GUI_Y);

                return (dist_to_focus_1 + dist_to_focus_2) <= d.SemiMajorAxis * 2;
            }
        }
    },

    PointInSector : {
        /// @func Provide() -> function
        Provide : function() {
            return function(d) {
                var cx = d.Centr.get_x();
                var cy = d.Centr.get_y();
                var dir = point_direction(cx, cy, MOUSE_GUI_X, MOUSE_GUI_Y);

                return (dir >= d.Alpha) && (dir <= d.Omega);
            }
        }
    },

    PointInEllipticalTorSector : {
        /// @func Provide() -> function
        Provide : function() {
            return function(d) {
                var pi_ellipce = PG_CollideAlgorithmProvider.PointInEllipce.Provide();
                var pi_sector = PG_CollideAlgorithmProvider.PointInSector.Provide();

                return (PG_CheckingWhetherPointBelongsToShape.Check(pi_ellipce, d.BoundOut)
                       ^^ PG_CheckingWhetherPointBelongsToShape.Check(pi_ellipce, d.BoundIn))
                       && PG_CheckingWhetherPointBelongsToShape.Check(pi_sector, d.Sector);
            }
        }
    },

    PointInEllipticalSegment : {
        /// @func Provide() -> function
        Provide : function() {
            return function(d) {
                var pi_ellipce = PG_CollideAlgorithmProvider.PointInEllipce.Provide();
                var pi_sector = PG_CollideAlgorithmProvider.PointInSector.Provide();

                return PG_CheckingWhetherPointBelongsToShape.Check(pi_ellipce, d.Ellipce)
                        && PG_CheckingWhetherPointBelongsToShape.Check(pi_sector, d.Sector)
                        && (! d.PointInTriangle(d.Ellipce, d.Sector));
            }
        }
    },

    PoinInPolygon : {
        /// @func Provide() -> function
        Provide : function() {
            return function(d) {
                var check = d.PointCheck;
                var points = d.Points;
                var size = array_length(points);
                var result = 1;

                if (size > 2) {
                    /// TODO: throw exception
                }

                for (var i = 0; i < size; i++) {
                    var j = ( i + 1 == size ) ? 0 : i + 1;
                    result *= check(points[@ i], points[@ j]);
                }

                return result == -1;
            }
        }
    }
}
