
function GgTextSelector() constructor
{
    static has_select = function()
    {
        return how_many > 0;
    }

    static select = function(_where, _how_many, _limit)
    {
        if _where < 1 || _where > _limit
        {
            // exception
        }

        if _how_many < 1
        {
            // exception
        }

        if _where + _how_many > _limit
        {
            _how_many = _limit - _where;
        }

        where = _where;
        how_many = _how_many;
    }

    static deselect = function()
    {
        where = 1;
        how_many = 0;
    }

    static to_left = function(_cursor, _step, _limit)
    {
        if has_select()
        {
            if where == 1
            {
                return;
            }

            if where < _cursor
            {
                var _pos = max(where - _step, 1);
                var _count = max(_cursor - _pos, 0);

                select(_pos, _count, _limit);
            }
            else
            {
                how_many = max(how_many - _step, 0);
            }
        }
        else
        {
            var _pos = max(_cursor - _step, 1);
            select(_pos, _step, _limit);
        }
    }

    static to_right = function(_cursor, _step, _limit)
    {
        if has_select()
        {
            if where < _cursor
            {
                where = min(where + _step, _limit);
                how_many = max(how_many - _step, 0);
            }
            else
            {
                var _max = _limit - _cursor;
                how_many = min(how_many + _step, _max);
            }
        }
        else
        {
            select(_cursor, _step, _limit);
        }
    }

    where = 1;
    how_many = 0;
}
