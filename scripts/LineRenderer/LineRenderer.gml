
/// @func GgLineRenderer(_transform)
///
/// @desc
///    Component, for drawing lines,
///    as a rule, broken lines, but with enough
///    dense arrangement of points, used
///    for drawing smooth curves.
///
/// @arg {Struct.GgTransform} _transform
///    Transform, for orientation primitive
///    that is being used for drawing lines.
///
/// @context undefined
///
function GgLineRenderer(_transform) constructor
{
    #region methods

    #region public

    /// @func build(_source, [_width])
    ///
    /// @desc
    ///    Draws a line along the path with a given thickness.
    ///    Note: Choose the best number of
    ///    points to find the balance
    ///    between curve smoothness and performance.
    ///
    /// @arg {Asset.GmPath} _source
    ///    The path to draw the line.
    ///
    /// @arg {Real} _width
    ///    Line thickness.
    ///
    /// @context GgLineRenderer
    ///
    static build = function(_source, _width = 1)
    {
        primitive.clear();
        path_rotate(_source, transform.rotation);

        var _corner = gg_path_top_left_corner_pos (
            _source
        );

        var _points = gg_dist_to_top_left_corner (
            _source, _corner
        );

        gg_shift_to_top_left_corner(_points);

        delete _corner;

        var _half_width = _width / 2;
        var _length = array_length(_points);
        var _head_color = head_brush.color;
        var _head_alpha = head_brush.alpha;
        var _tail_color = tail_brush.color;
        var _tail_alpha = tail_brush.alpha;

        for (var _i = 0; _i < _length - 2; _i += 2)
        {
            var _cx = _points[@ _i + 0];
            var _cy = _points[@ _i + 1];
            var _nx = _points[@ _i + 2];
            var _ny = _points[@ _i + 3];

            var _t = _i / _length;
            var _color = merge_color(_head_color, _tail_color, _t);
            var _alpha = lerp(_head_alpha, _tail_alpha, _t);
            var _len_dirs = get_len_dirs(_half_width, _cx, _cy, _nx, _ny);

            if _i == 0
            {
                build_block(_cx, _cy, _len_dirs, _color, _alpha);
            }

            build_block(_nx, _ny, _len_dirs, _color, _alpha);

            delete _len_dirs;
        }
    }

    /// @func draw()
    ///
    /// @desc
    ///    Draw line.
    ///
    /// @context GgLineRenderer
    ///
    static draw = function()
    {
        primitive.draw();
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgLineRenderer
    ///
    static free = function()
    {
        primitive.free();
        delete primitive;

        delete head_brush;
        delete tail_brush;
    }

    #endregion public

    #region private

    /// @func get_len_dirs(_half_width, _cx, _cy, _nx, _ny)
    ///
    /// @desc
    ///    Returns lengthdirs, both sides from a
    ///    straight line, to form a primitive.
    ///
    /// @arg {Real} _half_width
    ///    Half line width.
    ///
    /// @arg {Real} _cx
    ///    Coordinate of the first point along the x axis.
    ///
    /// @arg {Real} _cy
    ///    Coordinate of the first point along the y axis.
    ///
    /// @arg {Real}_nx
    ///    Coordinate of the second point along the x axis.
    ///
    /// @arg {Real} _ny
    ///    Coordinate of the second point along the y axis.
    ///
    /// @return {Struct}
    ///
    /// @context GgLineRenderer
    ///
    /// @ignore
    ///
    static get_len_dirs = function(_half_width, _cx, _cy, _nx, _ny)
    {
        var _angle = point_direction(_cx, _cy, _nx, _ny);
        var _left_lendir_x = lengthdir_x(_half_width, _angle + 90);
        var _left_lendir_y = lengthdir_y(_half_width, _angle + 90);
        var _right_lendir_x = lengthdir_x(_half_width, _angle - 90);
        var _right_lendir_y = lengthdir_y(_half_width, _angle - 90);

        return {
            left_lendir_x: _left_lendir_x,
            left_lendir_y: _left_lendir_y,
            right_lendir_x: _right_lendir_x,
            right_lendir_y: _right_lendir_y,
        };
    }

    /// @func build_block(_px, _py, _len_dirs, _color, _alpha)
    ///
    /// @desc
    ///    Creates and lines up a pair of vertices
    ///    opposite each other. And saves them.
    ///
    /// @arg {Real} _px
    ///    Position along the "x" axis.
    ///
    /// @arg {Real} _py
    ///    Position along the "y" axis.
    ///
    /// @arg {Struct} _len_dirs
    ///    Indents for block construction.
    ///
    /// @arg {Constant.Colour} _color
    ///    Vertex color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha-channel value (transparency) of the vertex.
    ///
    /// @return {Struct}
    ///
    /// @context GgLineRenderer
    ///
    /// @ignore
    ///
    static build_block = function(_px, _py, _len_dirs, _color, _alpha)
    {
        var _fields = [
            "left_lendir_x", "left_lendir_y",
            "right_lendir_x", "right_lendir_y",
        ];

        for (var _i = 0; _i < array_length(_fields); _i += 2)
        {
            var _vertex_x = _px + _len_dirs[$ _fields[_i + 0]];
            var _vertex_y = _py + _len_dirs[$ _fields[_i + 1]];

            var _vertex = new GgVertex (
                new GgPosition(_vertex_x, _vertex_y),
                new GgTexturePosition(0, 0),
                new GgBrush(_color, _alpha)
            );

            primitive.append(_vertex);
        }
    }

    #endregion private

    #endregion methods

    #region variables

    #region public

    head_brush = new GgBrush();
    tail_brush = new GgBrush();

    #endregion public

    #region private

    /// @ignore
    primitive = new GgPrimitiveRenderer(_transform);

    /// @ignore
    transform = _transform;

    #endregion private

    #endregion variables

    #region settings

    primitive.type = pr_trianglestrip;

    #endregion settings
}
