
/// GgRings(_position)
///
/// @desc
///    Collection of rings coming from the center
///    to the edges, to check for collision
///    with separate rings.
///
/// @arg {Struct.GgPosition} _position
///    The point that is used as the center.
///
/// @context undefined
///
function GgRings(_position)
: GgRadianShape(_position) constructor
{
    #region methods

    #region public

    /// @func append([_thickness])
    ///
    /// @desc
    ///    Adds a radius, forming the outer ring.
    ///
    /// arg {Real} _thickness
    ///    Thickness of the outer ring.
    ///
    /// @context GgRings
    ///
    static append = function(_thickness = 1)
    {
        var _index = storage.count() - 1;
        var _radius = storage.get(_index);

        storage.append(_radius + _thickness);
    }

    /// @func check(_index)
    ///
    /// @desc
    ///    Function to check if whether there was a
    ///    mouse click within the given boundaries.
    ///
    /// @arg {Real} _index
    ///    Sector position in storage.
    ///
    /// @return {Bool}
    ///
    /// @context GgRings
    ///
    static check = function(_index = 1)
    {
        if _index < 1
        {
            // exception
        }

        var _mx = GG_GUI_MOUSE_X;
        var _my = GG_GUI_MOUSE_Y;
        var _out_radius = storage.get(_index);
        var _in_radius = storage.get(_index - 1);

        if is_undefined(_out_radius)
        {
            return false;
        }

        var _out_check = point_in_circle (
            _mx, _my, position.x, position.y, _out_radius
        );

        var _in_check = point_in_circle (
            _mx, _my, position.x, position.y, _in_radius
        );

        return _out_check ^^ _in_check;
    }

    #endregion public

    #endregion methods

    #region settings

    storage.append(0);

    #endregion settings

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgSectors
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);

        storage.foreach(draw_ring, position);
        position.draw_gizmos(_color, _alpha);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    /// @func draw_ring(_radius, _point)
    ///
    /// @desc
    ///    Draws a circle, with the radius of the ring.
    ///
    /// @arg {Real} _radius
    ///    Ring radius.
    ///
    /// @arg {Struct.GgPosition} _point
    ///    General center.
    ///
    /// @context GgSectors
    ///
    /// @ignore
    ///
    static draw_ring = function(_radius, _point)
    {
        draw_circle(_point.x, _point.y, _radius, true);
    }

    #endregion debug
}
