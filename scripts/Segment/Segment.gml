
/// @func GgSegment(_x, _y, [_direction])
///
/// @desc
///    Component created to create more
///    complex processing for ownership
///    points to some figure.
///
/// @context undefined
///
function GgSegment(_x, _y, _direction = 0) constructor
{
    #region methods

    #region public

    /// @func check(_condition)
    ///
    /// @desc
    ///    Calculate which condition satisfies
    ///    satisfies the skew product
    ///    between "carrying|stationary" vector and a
    ///    vector indicating the GUI position of the mouse.
    ///
    /// @arg {Real} _condition
    ///    The condition we are targeting it is a sign
    ///    skew product (enum GG_SKEW_PRODUCT).
    ///
    /// @return {Bool}
    ///
    /// @context GgSegment
    ///
    static check = function(_condition)
    {
        var _mx = GG_GUI_MOUSE_X;
        var _my = GG_GUI_MOUSE_Y;

        dinamic_vector.to.set_position(_mx, _my);

        var _skew_product = static_vector.skew(dinamic_vector);

        switch _condition
        {
            case GG_SKEW_PRODUCT.LESS_ZERO:
            {
                return _skew_product < 0;
            }

            case GG_SKEW_PRODUCT.EQUALS_ZERO:
            {
                return _skew_product == 0;
            }

            case GG_SKEW_PRODUCT.ABOVE_ZERO:
            {
                return _skew_product > 0
            }

            default:
            {
                // exception
            }
        }
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgSegment
    ///
    static free = function()
    {
        static_vector.free();
        delete static_vector;

        dinamic_vector.free();
        delete dinamic_vector;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    static_vector = new GgVector2();
    dinamic_vector = new GgVector2();

    #endregion public

    #endregion variables

    #region settings

    /**
     *  NOTE:
     *  The vector length of 100 units is taken arbitrarily,
     *  the length is not particularly important
     *  to us, only the direction of the vector is important.
     */

    var _sx = _x + lengthdir_x(100, _direction);
    var _sy = _y + lengthdir_y(100, _direction);

    static_vector.from.set_position(_x, _y);
    static_vector.to.set_position(_sx, _sy);
    dinamic_vector.from.set_position(_x, _y);

    #endregion settings

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgPosition
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        dinamic_vector.draw_gizmos(_color, _alpha);
        static_vector.draw_gizmos(_color, _alpha);
    }

    #endregion debug
}
