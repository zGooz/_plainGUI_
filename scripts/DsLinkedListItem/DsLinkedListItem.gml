
/// @func GgDsLinkedListItem(_value)
///
/// @desc
///    Linked list element.
///
/// @arg {Any} _value
///    The value to pass.
///
/// @context undefined
///
/// @ignore
///
function GgDsLinkedListItem(_value) constructor
{
    #region variables

    #region public

    value = _value;
    previous = undefined;
    next = undefined;

    #endregion public

    #endregion variables
}
