
/// @func ds_linked_list_closed_create();
///
/// @desc
///    This function will create a new "closed" linked
///    list data-structure and return the index value.
///
/// @return {Struct.GgDsLinkedList}
///
/// @context undefined
///
function ds_linked_list_closed_create()
{
    return new GgDsLinkedList(true);
}

/// @func ds_linked_list_create();
///
/// @desc
///    This function will create a new linked list
///    data-structure and return the index value.
///
/// @return {Struct.GgDsLinkedList}
///
/// @context undefined
///
function ds_linked_list_create()
{
    return new GgDsLinkedList(false);
}

/// @func ds_linked_list_add(_id, _value)
///
/// @desc
///    This function can be used to add a new value to the
///    linked list, which will be added on at the end. 
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to add to.
///
/// @arg {Any} _value
///    The value to add to the linked list.
///
/// @context undefined
///
function ds_linked_list_add(_id, _value)
{
    _id.add(_value);
}

/// @func ds_linked_list_insert(_id, _pos, _value)
///
/// @desc
///    This function will add the given value
///    into the list at the given position. 
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to add to.
///
/// @arg {Real} _pos
///    The position to add the value.
///
/// @arg {Any} _value
///    The value to add to the linked list.
///
/// @context undefined
///
function ds_linked_list_insert(_id, _pos, _value)
{
    _id.insert(_pos, _value);
}

/// @func ds_linked_list_delete(_id, _pos)
///
/// @desc
///    With this function you can remove the value
///    stored at a specific position within the linked list.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to change.
///
/// @arg {Real} _pos
///    Where in the linked list to delete the value.
///
/// @context undefined
///
function ds_linked_list_delete(_id, _pos)
{
    _id.remove(_pos);
}

/// @func ds_linked_list_replace(_id, _pos, _value)
///
/// @desc
///    This function will replace the value at
///    the given position for another one.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to change.
///
/// @arg {Real} _pos
///    The position to replace the value.
///
/// @arg {Any} _value
///    The new value to replace the given value with.
///
/// @context undefined
///
function ds_linked_list_replace(_id, _pos, _value)
{
    _id.replace(_pos, _value);
}

/// @func ds_linked_list_find_value(_id, _pos)
///
/// @desc
///    With this function you can check the given
///    linked list position and the value held within
///    the list for that position will be returned.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to use.
///
/// @arg {Real} _pos
///    	The position to look at.
///
/// @context undefined
///
function ds_linked_list_find_value(_id, _pos)
{
    var _item = _id.get(_pos);
    return is_undefined(_item) ? _item : _item.value;
}

/// @func ds_linked_list_find_index(_id, _value)
///
///    With this function you can check the given
///    linked list for a value and the position within
///    the linked list for that value will be returned.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to use.
///
/// @arg {Any} _value
///    The value to find.
///
/// @return {Real}
///
/// @context undefined
///
function ds_linked_list_find_index(_id, _value)
{
    return _id.find(_value);
}

/// @func ds_linked_list_size(_id)
///
/// @desc
///    This function will return the "size" of the linked list,
///    ie: the number of items that have been added into it.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the linked list to use.
///
/// @context undefined
///
function ds_linked_list_size(_id)
{
    return _id.length();
}

/// @func ds_linked_list_empty(_id)
///
/// @desc
///    With this function you can check the
///    given DS list to see if it is empty
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the data structure to check.
///
/// @return {Bool}
///
/// @context undefined
///
function ds_linked_list_empty(_id)
{
    return _id.length() == 0;
}

/// @func ds_linked_list_clear(_id)
///
/// @desc
///    With this function you can clear all data
///    from the given linked list data-structure.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the data structure to clear.
///
/// @context undefined
///
function ds_linked_list_clear(_id)
{
    _id.clear();
}

/// @func ds_linked_list_destroy(_id)
///
/// @desc
///    This function will remove the given
///    linked list data-structure from memory.
///
/// @arg {Struct.GgDsLinkedList} _id
///    The id of the data structure to remove.
///
/// @context undefined
///
function ds_linked_list_destroy(_id)
{
    _id.clear();
    delete _id;
}
