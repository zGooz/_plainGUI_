
/// @func GgAdapterStoragePriorityQueue([_filter])
///
/// @arg {Any} _filter
///
/// @context GgAdapterStorage
///
function GgAdapterStoragePriorityQueue(_filter = GG_PRIORITY.GET_MAX)
: GgAdapterStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = ds_priority_create();

    /// @ignore
    filter = _filter;

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func push(_item)
    ///
    /// @desc
    ///    Pushes an item into storage.
    ///    
    /// @arg {Any} _item
    ///    The element to add.
    ///
    /// @context GgAdapterStoragePriorityQueue
    ///
    static push = function(_item)
    {
        ds_priority_add(content, _item.value, _item.priority);
    }

    /// @func get()
    ///
    /// @desc
    ///    Gets the element, but does not remove it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStoragePriorityQueue
    ///
    static get = function()
    {
        var _item;

        if filter == GG_PRIORITY.GET_MAX
        {
            _item = ds_priority_find_max(content);
        }
        else if filter == GG_PRIORITY.GET_MIN
        {
            _item = ds_priority_find_min(content);
        }

        return _item;
    }

    /// @func put()
    ///
    /// @desc
    ///    Gets an element and removes it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStoragePriorityQueue
    ///
    static put = function()
    {
        var _item;

        if filter == GG_PRIORITY.GET_MAX
        {
            _item = ds_priority_delete_max(content);
        }
        else if filter == GG_PRIORITY.GET_MIN
        {
            _item = ds_priority_delete_min(content);
        }

        return _item;
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgAdapterStoragePriorityQueue
    ///
    static count = function()
    {
        return ds_priority_size(content);
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    The procedure that is applied to
    ///    an element when cleared|deleted.
    ///
    /// @context GgSequenceStorage
    ///
    static clear = function(_proc)
    {
        process_all(_proc);
        ds_priority_clear(content);
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgAdapterStoragePriorityQueue
    ///
    static free = function()
    {
        ds_priority_destroy(content);
        content = -1;
    }

    #endregion public

    #endregion methods
}
