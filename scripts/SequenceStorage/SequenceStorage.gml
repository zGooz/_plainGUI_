
/// @func GgSequenceStorage()
///
/// @desc
///    Parent class of the enum (iterable) store.
///
/// @context undefined
///
/// @ignore
///
function GgSequenceStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = undefined;

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func append(_item, [_id])
    ///
    /// @desc
    ///    Adds an element to the store.
    ///
    /// @arg {Any} _item
    ///    Element to add.
    ///
    /// @arg {real, string} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgStorage
    ///
    static append = function(_item, _id = -1){}

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element from the storage.
    ///
    /// @arg {real, string} _id
    ///    ID of the element to remove.
    ///
    /// @context GgStorage
    ///
    static remove = function(_id = -1){}

    /// @func change(_item, _id)
    ///
    /// @desc
    ///    Edits an element or
    ///    replaces one element with another.
    ///
    /// @arg {Any} _item
    ///    Element to edit.
    ///
    /// @arg {real, string} _id
    ///    Element ID, to edit|replace.
    ///
    /// @context GgStorage
    ///
    static change = function(_item, _id){}

    /// @func get(_id)
    ///
    /// @desc
    ///    Returning an element from storage.
    ///
    /// @arg {real, string} _id
    ///    Element ID.
    ///
    /// @return {Any}
    ///
    /// @context GgStorage
    ///
    static get = function(_id){}

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgStorage
    ///
    static count = function(){}

    /// @func foreach(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgStorage
    ///
    static foreach = function(_proc, _data = undefined){}

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    element when clearing|deleting.
    ///
    /// @context GgStorage
    ///
    static clear = function(_proc){}

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgStorage
    ///
    static free = function(){}

    #endregion public

    #endregion methods
}
