
/// @func GgSpace()
///
/// @desc
///    This component is used to store and
///    set the location of the transforms.
///
/// @context undefined
///
function GgSpace() constructor
{
    #region methods

    #region public

    /// @func append(_transform, [_id])
    ///
    /// @desc
    ///    Adds an transform to the space (field).
    ///
    /// @arg {Struct.GgTransform} _transform
    ///    Transform to add.
    ///
    /// @arg {Real} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgSpace
    ///
    static append = function(_transform, _id = -1)
    {
        storage.append(_transform, _id);
    }

    /// @func place(_data)
    ///
    /// @desc
    ///    A method designed to place transforms in
    ///    space according to an algorithm.
    ///
    /// @arg {Struct} _data
    ///    Data containing the algorithm
    ///    for placing his signature
    ///    "function(number) -> GgPosition"
    ///
    /// @context GgSpace
    ///
    static place = function(_data)
    {
        var _ext_data = {
            addition_data: _data,
            iteration: 0,
        };

        storage.foreach(set_transform_pos, _ext_data);

        delete _ext_data;
    }

    /// @func reset_place_algorithm()
    ///
    /// @desc
    ///    Resets the placing algorithm
    ///    to the default algorithm.
    ///
    /// @context GgSpace
    ///
    static reset_calculate_algorithm = function()
    {
        get_place = gg_get_def_point;
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgSpace
    ///
    static free = function()
    {
        storage.free();
        delete storage;
    }

    #endregion public

    #region private

    /// @func set_transform_pos(_transform, _data)
    ///
    /// @desc
    ///    A function that sets the position of the transform.
    ///
    /// @arg {Struct.GgTransform} _transform
    ///    Transform whose position is being changed.
    ///
    /// @arg {Struct} _data
    ///    Data containing the algorithm
    ///    for placing his signature
    ///    "function(number) -> GgPosition"   
    ///
    /// @context GgSpace
    ///
    /// @ignore
    ///
    static set_transform_pos = function(_transform, _data)
    {
        var _addition_data = _data.addition_data;
        var _n = _data.iteration;
        var _pos = get_place(_n, _addition_data);

        _transform.set_position(_pos.x, _pos.y);
        _data.iteration += 1;

        delete _pos;
    }

    #endregion private

    #endregion methods

    #region variables

    #region public

    get_place = gg_get_def_point;

    #endregion public

    #region private

    /// @ignore
    storage = new GgSequenceStorageArray();

    #endregion private

    #endregion variables
}
