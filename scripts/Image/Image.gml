
/// @func GgImageRenderer(_transform)
///
/// @desc
///    Component for drawing images directly.
///    Supports regular and skeletal animation
///
/// @arg {Struct.GgTransform} _transform
///    Transform to orient the image.
///
/// @context undefined
///
function GgImageRenderer(_transform) constructor
{
    #region methods

    #region public

    /// @func set_sprite(_sprite, [_sub_image])
    ///
    /// @desc
    ///    Specifies a sprite with a specific frame.
    ///    WARNING: Parameters of the "part" structure
    ///    are reset to adapt to the size of the sprite.
    ///
    /// @arg {Asset.GmSprite} _sprite
    ///    Sprite to draw.
    ///
    /// @arg {Real} _sub_image
    ///    Sets the frame of the sprite.
    ///
    /// @context GgImageRenderer
    ///
    static set_sprite = function(_sprite, _sub_image = 0)
    {
        var _spr_width = sprite_get_width(_sprite);
        var _spr_height = sprite_get_height(_sprite);

        sprite = _sprite;
        frame = _sub_image;
        part.left = 0;
        part.top = 0;
        part.width = _spr_width;
        part.height = _spr_height;
    }

    /// @func set_view([_left], [_top], [_width], [_height])
    ///
    /// @desc
    ///    Set the area to draw.
    ///
    /// @arg {Real} _left
    ///    The x position on the sprite of the
    ///    top left corner of the area to draw.
    ///
    /// @arg {Real} _top
    ///    The y position on the sprite of the
    ///    top left corner of the area to draw.
    ///
    /// @arg {Real} _width
    ///    The width of the area to draw.
    ///
    /// @arg {Real} _height
    ///    The height of the area to draw.
    ///
    /// @context GgImageRenderer
    ///
    static set_view = function
    (_left = 0, _top = 0, _width = 1, _height = 1)
    {
        part.left = _left;
        part.top = _top;
        part.width = _width;
        part.height = _height;
    }

    /// @func draw()
    ///
    /// @desc
    ///    Renders an image.
    ///
    /// @context GgImageRenderer
    ///
    static draw = function()
    {
        if sprite < 0
        {
            return;
        }

        if is_skeleton
        {
            draw_skeleton_sprite();
        }
        else
        {
            draw_pixel_sprite();
        }

        next_frame();
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgImageRenderer
    ///
    static free = function()
    {
        delete brush;
        delete part;
        delete skeleton;
    }

    #endregion public

    #region private

    /// @func draw_skeleton_sprite()
    ///
    /// @desc
    ///    Renders an skeleton-image.
    ///
    /// @context GgImageRenderer
    ///
    /// @ignore
    ///
    static draw_skeleton_sprite = function()
    {
        var _x = transform.position.x;
        var _y = transform.position.y;
        var _angle = transform.rotation;
        var _factor_x = transform.scale.factor_x;
        var _factor_y = transform.scale.factor_y;
        var _color = brush.color;
        var _alpha = brush.alpha;
        var _anim_name = skeleton.animation_name;
        var _skin_name = skeleton.skin_name;

        if is_use_time
        {
            var _time = skeleton.time;

            draw_skeleton_time (
                sprite, _anim_name, _skin_name, _time,
                _x, _y, _factor_x, _factor_y, _angle,
                _color, _alpha
            );
        }
        else  // use frame (sub-image)
        {
            draw_skeleton (
                sprite, _anim_name, _skin_name, frame,
                _x, _y, _factor_x, _factor_y, _angle,
                _color, _alpha
            );
        }
    }

    /// @func draw_pixel_sprite()
    ///
    /// @desc
    ///    Renders an pixel-image.
    ///
    /// @context GgImageRenderer
    ///
    /// @ignore
    ///
    static draw_pixel_sprite = function()
    {
        var _x = transform.position.x;
        var _y = transform.position.y;
        var _angle = transform.rotation;
        var _factor_x = transform.scale.factor_x;
        var _factor_y = transform.scale.factor_y;
        var _color = brush.color;
        var _alpha = brush.alpha;

        if is_stretch  // or nineslice
        {
            var _width = transform.size.width;
            var _height = transform.size.height;

            draw_sprite_stretched_ext (
                sprite, frame,
                _x, _y, _width, _height,
                _color, _alpha
            );
        }
        else
        {
            var _left = part.left;
            var _top = part.top;
            var _width = part.width;
            var _height = part.height;

            draw_sprite_general (
                sprite, frame, _left, _top, _width, _height,
                _x, _y, _factor_x, _factor_y, _angle,
                _color, _color, _color, _color, _alpha
            );
        }
    }

    /// @func next_frame()
    ///
    /// @desc
    ///    Moves to the next frame.
    ///
    /// @context GgImageRenderer
    ///
    /// @ignore
    ///
    static next_frame = function()
    {
        if is_use_time
        {
            var _delta = GG_SKELETON_TIME_FRAME_SPEED;
            skeleton.time += _delta * boost;
        }
        else  // use frame (sub-image)
        {
            frame += image_speed;
        }
    }

    #endregion private

    #endregion methods

    #region variables

    #region public

    is_skeleton = false;
    is_use_time = false;
    is_stretch = false;
    brush = new GgBrush();
    transform = _transform;
    boost = 1;
    image_speed = 1;

    part = {
        left: 0,
        top: 0,
        width: 1,
        height: 1,
    };

    skeleton = {
        animation_name: "",
        skin_name: "",
        time: 0,
    };

    #endregion public

    #region private

    /// @ignore
    sprite = -1;

    /// @ignore
    frame = 0;

    #endregion private

    #endregion variables
}
