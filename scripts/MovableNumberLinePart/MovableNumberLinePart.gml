
/// @func GgMovableNumberLinePart([_offset], [_chuck])
///
/// @desc
///    This component is required to store
///    and moving the fragment of the number line.
///
/// @arg {Real} _offset
///    From what date the fragment of the number line starts.
///
/// @arg {Real} _chuck
///    The size of the fragment of the number line.
///
/// @context undefined
///
function GgMovableNumberLinePart
(_offset = 0, _chuck = 0) constructor
{
    #region methods

    #region public

    /// @func get()
    ///
    /// @desc
    ///    Returns the range of captured values
    ///
    /// @return {Struct}
    ///
    /// @context GgMovableNumberLinePart
    ///
    static get = function()
    {
        return {
            from: offset,
            to: offset + chunk,
        };
    }

    /// @func expand([_value])
    ///
    /// @desc
    ///    Expands the range of affected values.
    ///
    /// @arg {Real} _value
    ///    The amount by which to expand the
    ///    range of affected values.
    ///
    /// @context GgMovableNumberLinePart
    ///
    static expand = function(_value = 1)
    {
        chunk += _value;
    }

    /// @func collapse([_value])
    ///
    /// @desc
    ///    Сollapce the range of affected values.
    ///
    /// @arg {Real} _value
    ///    The amount by which to collapse the
    ///    range of affected values.
    ///
    /// @context GgMovableNumberLinePart
    ///
    static collapse = function(_value = 1)
    {
        chunk = max(0, chunk - _value);
    }

    /// @func shift(_offset)
    ///
    /// @desc
    ///    Shift the range of affected values.
    ///
    /// @arg {Real} _offset
    ///    The amount by which to shift the
    ///    range of affected values.
    ///
    /// @context GgMovableNumberLinePart
    ///
    static shift = function(_offset)
    {
        offset += _offset;
    }

    #endregion public

    #endregion methods

    #region verification

    if _chuck < 0
    {
        // exception
    }

    #endregion verification

    #region variables

    #region private

    /// @ignore
    offset = _offset;

    /// @ignore
    chunk = _chuck;

    #endregion private

    #endregion variables
}
