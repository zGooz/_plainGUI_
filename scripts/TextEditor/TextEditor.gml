
function GgTextEditor(_area, _text_renderer)
: GgWidget(_area) constructor
{
    #region methods

    #region public

    static work_process = function()
    {
        var _state = time_source_get_state(type_timer);

        if is_input_series
        {
            var _from = bearing_cursor.position;
            var _to = _from;

            buffer.remember(_text_old, _from);
        }

        if keyboard_check_pressed(vk_anykey)
        {
            type();
            manager.monitor();
        }

        if _state == time_source_state_stopped
        || _state == time_source_state_initial
        {
            time_source_start(type_timer);
            time_source_start(command_timer);
        }

        if ! is_input_series
        {
            var _to = type_cursor.position;
            buffer.remember(,, _to);
        }

        text_renderer.set_text(buffer.get());

        if keyboard_check_released(vk_anykey)
        {
            time_source_stop(type_timer);
            time_source_stop(command_timer);
        }
    }

    /// @func draw()
    ///
    /// @desc
    ///    The procedure for displaying a textbox on the screen.
    ///
    /// @context GgTextEditor
    ///
    static draw = function()
    {
        text_renderer.draw();
    }

    static delete_resouces = function()
    {
        time_source_destroy(type_timer);
        time_source_destroy(command_timer);

        manager.free();
        delete manager;

        delete char_source;
        delete info;
        delete block;
        delete buffer;
        delete bearing_cursor;
        delete type_cursor;
    }

    #endregion public

    #region private

    /// @ignore
    static on_left = function()
    {
        if block.has_select()
        {
            bearing_cursor.set(block.where, length);
            type_cursor.set(block.where, length);
        }
        else
        {
            bearing_cursor.to_left(1);
            type_cursor.to_left(1);
        }

        block.deselect();
    }

    /// @ignore
    static on_right = function()
    {
        if block.has_select()
        {
            var _pos = block.where + block.how_many;

            bearing_cursor.set(_pos, length);
            type_cursor.set(_pos, length);
        }
        else
        {
            bearing_cursor.to_right(1, length);
            type_cursor.to_right(1, length);
        }

        block.deselect();
    }

    /// @ignore
    static on_control_left = function()
    {
        var _text = buffer.get();
        var _crlf = char_source.new_row_symbol;

        var _distance = info.dist_to_left_free_place (
            self, _text, _crlf
        );

        bearing_cursor.to_left(_distance);

        var _cursor = bearing_cursor.position;

        type_cursor.set(_cursor, length);
        block.deselect();
    }

    /// @ignore
    static on_control_right = function()
    {
        var _text = buffer.get();
        var _crlf = char_source.new_row_symbol;

        var _distance = info.dist_to_right_free_place (
            self, _text, _crlf, length
        );

        bearing_cursor.to_right(_distance, length);

        var _cursor = bearing_cursor.position;

        type_cursor.set(_cursor, length);
        block.deselect();
    }

    /// @ignore
    static on_shift_left = function()
    {
        var _cursor = bearing_cursor.position;

        block.to_left(_cursor, 1, length);
        type_cursor.to_left(1);
    }

    /// @ignore
    static on_shift_right = function()
    {
        var _cursor = bearing_cursor.position;

        block.to_right(_cursor, 1, length);
        type_cursor.to_right(1, length);
    }

    /// @ignore
    static on_control_shift_left = function()
    {
        var _text = buffer.get();
        var _cursor = bearing_cursor.position;
        var _crlf = char_source.new_row_symbol;

        var _distance = info.dist_to_left_free_place (
            self, _text, _crlf
        );

        block.to_left(_cursor, _distance, length);
        type_cursor.to_left(_distance);
    }

    /// @ignore
    static on_control_shift_right = function()
    {
        var _text = buffer.get();
        var _cursor = bearing_cursor.position;
        var _crlf = char_source.new_row_symbol;

        var _distance = info.dist_to_right_free_place (
            self, _text, _crlf, length
        );

        block.to_right(_cursor, _distance, length);
        type_cursor.to_right(_distance, length);
    }

    /// @ignore
    static on_up = function()
    {
        var _text = buffer.get();
        var _support = get_support_pos();
        var _crlf = char_source.new_row_symbol;
        var _fragment = string_copy(_text, 1, _support);

        var _dist = info.dist_to_up_free_place (
            _fragment, _support, _crlf
        );

        type_cursor.to_left(_dist);

        var _cursor = type_cursor.position;

        bearing_cursor.set(_cursor, length);
        block.deselect();
    }

    /// @ignore
    static on_down = function()
    {
        var _text = buffer.get();
        var _support = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        var _fragment = string_copy (
            _text, _support, length - _support
        );

        var _dist_to_row = info.dist_to_left_word_or_row (
            self, _text, _crlf
        );

        var _dist = info.dist_to_down_free_place (
            _fragment, _support, _crlf, _dist_to_row, length
        );

        type_cursor.to_right(_dist, length);

        var _cursor = type_cursor.position;

        bearing_cursor.set(_cursor, length);
        block.deselect();
    }

    /// @ignore
    static on_shift_up = function()
    {
        var _text = buffer.get();
        var _support = get_support_pos();

        var _crlf = char_source.new_row_symbol;
        var _fragment = string_copy(_text, 1, _support);

        var _dist = info.dist_to_up_free_place (
            _fragment, _support, _crlf
        );

        type_cursor.to_left(_dist);

        var bearing_cursor_pos = bearing_cursor.position;
        var type_cursor_pos = type_cursor.position;
        var _count = bearing_cursor_pos - type_cursor_pos;

        block.select(type_cursor_pos, _count, length);
    }

    /// @ignore
    static on_shift_down = function()
    {
        var _text = buffer.get();
        var _support = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        var _fragment = string_copy (
            _text, _support, length - _support
        );

        var _dist_to_row = info.dist_to_left_word_or_row (
            self, _text, _crlf
        );

        var _dist = info.dist_to_down_free_place (
            _fragment, _support, _crlf, _dist_to_row, length
        );

        type_cursor.to_right(_dist, length);

        var bearing_cursor_pos = bearing_cursor.position;
        var type_cursor_pos = type_cursor.position;
        var _count = type_cursor_pos - bearing_cursor_pos;

        block.select(bearing_cursor_pos, _count, length);
    }

    /// @ignore
    static on_page_up = function()
    {
        repeat page_size
        {
            on_up();
        }
    }

    /// @ignore
    static on_page_down = function()
    {
        repeat page_size
        {
            on_down();
        }
    }

    /// @ignore
    static on_shift_page_up = function()
    {
        repeat page_size
        {
            on_shift_up();
        }
    }

    /// @ignore
    static on_shift_page_down = function()
    {
        repeat page_size
        {
            on_shift_down();
        }
    }

    /// @ignore
    static on_home = function()
    {
        var _text = buffer.get();
        var _pos = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        if pos == 1
        {
            return;
        }

        if info.check_left_symbol(_text, _crlf, _pos)
        {
            return;
        }

        var _dist = info.dist_to_left_word_or_row (
            self, _text, _crlf
        );

        var _place = bearing_cursor.position - _dist;

        bearing_cursor.set(_place, length);
        type_cursor.set(_place, length);
        block.deselect();
    }

    /// @ignore
    static on_end = function()
    {
        var _text = buffer.get();
        var _pos = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        if info.check_symbol(_text, _crlf, _pos)
        {
            return;
        }

        var _dist = info.dist_to_right_word_or_row (
            self, _text, _crlf, length
        );

        var _place = _pos + _dist - 1;

        bearing_cursor.set(_place, length);
        type_cursor.set(_place, length);
        block.deselect();
    }

    /// @ignore
    static on_control_home = function()
    {
        bearing_cursor.set(1, length);
        type_cursor.set(1, length);
        block.deselect();
    }

    /// @ignore
    static on_control_end = function()
    {
        var _pos = length + 1;

        bearing_cursor.set(_pos, length);
        type_cursor.set(_pos, length);
        block.deselect();
    }

    /// @ignore
    static on_shift_home = function()
    {
        block.deselect();

        var _text = buffer.get();
        var _pos = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        var _dist = info.dist_to_left_word_or_row (
            self, _text, _crlf
        );

        var _place = _pos - _dist;
        var _how_many = _pos - _place;

        type_cursor.set(_place, length);
        block.select(_place, _how_many, length);
    }

    /// @ignore
    static on_shift_end = function()
    {
        block.deselect();

        var _text = buffer.get();
        var _pos = get_support_pos();
        var _crlf = char_source.new_row_symbol;

        var _dist = info.dist_to_right_word_or_row (
            self, _text, _crlf, length
        );

        var _place = _pos + _dist - 1;

        type_cursor.set(_place, length);
        block.select(_pos, _dist - 1, length);
    }

    /// @ignore
    static on_control_shift_home = function()
    {
        var _pos_old = bearing_cursor.position;

        on_control_home();
        bearing_cursor.set(_pos_old, length);
        block.select(1, _pos_old, length);
    }

    /// @ignore
    static on_control_shift_end = function()
    {
        var _pos_old = bearing_cursor.position;

        on_control_end();
        bearing_cursor.set(_pos_old, length);

        var _how_many = length - _pos_old;

        block.select(_pos_old, _how_many, length);
    }

    /// @ignore
    static on_backspace = function()
    {
        if is_input_series
        {
            is_input_series = false;
        }

        if block.has_select()
        {
            erase();
        }
        else
        {
            var _text_old = buffer.get();
            var _pos = bearing_cursor.position - 1;
            var _text = string_delete(_text_old, _pos, 1);

            bearing_cursor.to_left(1);
            type_cursor.to_left(1);
            buffer.update(_text);
            length = string_length(_text);
        }
    }

    /// @ignore
    static on_delete = function()
    {
        if is_input_series
        {
            is_input_series = false;
        }

        if block.has_select()
        {
            erace();
        }
        else
        {
            var _text_old = buffer.get();
            var _pos = bearing_cursor.position;
            var _text = string_delete(_text_old, _pos, 1);

            buffer.update(_text);
            length = string_length(_text);
        }
    }

    /// @ignore
    static on_control_backspace = function()
    {
        on_control_shift_left();
        erase();
    }

    /// @ignore
    static on_control_delete = function()
    {
        on_control_shift_right();
        erase();
    }

    /// @ignore
    static on_control_shift_backspace = function()
    {
        on_shift_home();
        erase();
    }

    /// @ignore
    static on_control_shift_delete = function()
    {
        on_shift_end();
        erase();
    }

    /// @ignore
    static on_select_all = function()
    {
        block.select(1, length, length);
    }

    /// @ignore
    static on_copy = function()
    {
        var _text = string_copy (
            buffer.get(),
            block.where,
            block.how_many
        );

        clipboard_set_text(_text);
    }

    /// @ignore
    static on_paste = function()
    {
        if ! clipboard_has_text()
        {
            return;
        }

        if ! is_input_series
        {
            is_input_series = true;
        }

        if block.has_select()
        {
            erase();
        }

        var _clip = clipboard_get_text();
        var _len = string_length(_clip);
        var _pos = bearing_cursor.position;
        var _text = buffer.get();

        _text = string_insert(_clip, _text, _pos);
        length = string_length(_text);

        if length > max_length
        {
            _text = string_copy(_text, 1, max_length);
            length = max_length;
        }

        bearing_cursor.to_right(_len, length);
        type_cursor.to_right(_len, length);
        buffer.update(_text);
    }

    /// @ignore
    static on_cut = function()
    {
        on_copy();
        erase();
    }

    /// @ignore
    static on_undo = function()
    {
        if is_actual
        {
            is_actual = false;
            buffer.swap();
            text_renderer.set_text(buffer.get());
        }
    }

    /// @ignore
    static on_redo = function()
    {
        if ! is_actual
        {
            is_actual = true;
            buffer.swap();
            text_renderer.set_text(buffer.get());
        }
    }

    /// @ignore
    static get_support_pos = function()
    {
        var _pos = block.where < bearing_cursor.position
                 ? block.where
                 : block.where + block.how_many;

        return block.has_select()
            ? _pos : bearing_cursor.position;
    }

    /// @ignore
    static erase = function()
    {
        if is_input_series
        {
            is_input_series = false;
        }

        var _text = buffer.get();

        _text = string_delete(_text, block.where, block.how_many);
        length = string_length(_text);

        bearing_cursor.set(block.where, length);
        type_cursor.set(block.where, length);
        block.deselect();
        buffer.update(_text);
    }

    /// @ignore
    static type = function()
    {
        if length == max_length
        {
            return;
        }

        var _char = char_source.provide();

        if _char == ""
        {
            return;
        }

        if block.has_select()
        {
            erase();
        }
        else
        {
            if is_owerwrite
            {
                var _text = buffer.get();
                var _pos = bearing_cursor.position;

                _text = string_delete(_text, _pos, 1);
                buffer.update(_text);
            }
        }

        if ! is_input_series
        {
            is_input_series = true;
        }

        var _text = buffer.get();
        var _pos = bearing_cursor.position;
        var _offset = string_length(_char);

        _text = string_insert(_char, _text, _pos);
        length = string_length(_text);
        bearing_cursor.to_right(_offset, length);
        type_cursor.to_right(_offset, length);
        buffer.update(_text);
    }

    #endregion private

    #endregion methods

    #region variable

    #region public

    is_owerwrite = false;

    #endregion public

    #region private

    /// @ignore
    length = 0;

    /// @ignore
    max_length = GG_TEXT_LENGTH.DEFAULT;

    /// @ignore
    page_size = GG_PAGE_SIZE;

    /// @ignore
    delay = gg_time.type_delay;

    /// @ignore
    text_renderer = _text_renderer;

    /// @ignore
    char_source = new GgCharsProvider();

    /// @ignore
    manager = new GgCommandInvoker();

    /// @ignore
    info = new GgInfoAbotText();

    /// @ignore
    block = new GgTextSelector();

    /// @ignore
    buffer = new GgInputBuffer();

    /// @ignore
    bearing_cursor = new GgTextCursor();

    /// @ignore
    type_cursor = new GgTextCursor();

    /// @ignore
    is_actual = true;

    /// @ignore
    is_input_series = true;

    /// @ignore
    type_timer = time_source_create (
        time_source_game, delay,
        time_source_units_seconds,
        method(self, type)
    );

    /// @ignore
    command_timer = time_source_create (
        time_source_game, delay,
        time_source_units_seconds,
        method(manager, manager.monitor)
    );

    #endregion private

    #endregion variable

    #region settings

    char_source.give_new_row_symbol = false;
    char_source.give_tab_symbol = false;

    manager.ev_left.subscribe (
        "to_left", method(self, on_left)
    );

    manager.ev_right.subscribe (
        "to_right", method(self, on_right)
    );

    manager.ev_control_left.subscribe (
        "to_left_by_word",
        method(self, on_control_left)
    );

    manager.ev_control_right.subscribe (
        "to_right_by_word",
        method(self, on_control_right)
    );

    manager.ev_shift_left.subscribe (
        "select_left_symbol",
        method(self, on_shift_left)
    );

    manager.ev_shift_right.subscribe (
        "select_right_symbol",
        method(self, on_shift_right)
    );

    manager.ev_control_shift_left.subscribe (
        "select_left_words",
        method(self, on_control_shift_left)
    );

    manager.ev_control_shift_right.subscribe (
        "select_right_words",
        method(self, on_control_shift_right)
    );

    manager.ev_up.subscribe (
        "to_up", method(self, on_up)
    );

    manager.ev_down.subscribe (
        "to_down", method(self, on_down)
    );

    manager.ev_shift_up.subscribe (
        "select_cur&prew_rows_part",
        method(self, on_shift_up)
    );

    manager.ev_shift_down.subscribe (
        "select_cur&next_rows_part",
        method(self, on_shift_down)
    );

    manager.ev_page_up.subscribe (
        "page_up", method(self, on_page_up)
    );

    manager.ev_page_down.subscribe (
        "page_down", method(self, on_page_down)
    );

    manager.ev_shift_page_up.subscribe (
        "select_page_to_up",
        method(self, on_shift_page_up)
    );

    manager.ev_shift_page_down.subscribe (
        "select_page_to_down",
        method(self, on_shift_page_down)
    );

    manager.ev_home.subscribe (
        "home", method(self, on_home)
    );

    manager.ev_end.subscribe (
        "end", method(self, on_end)
    );

    manager.ev_control_home.subscribe (
        "to_start", method(self, on_control_home)
    );

    manager.ev_control_end.subscribe (
        "to_finish", method(self, on_control_end)
    );

    manager.ev_shift_home.subscribe (
        "select_left_row_half",
        method(self, on_shift_home)
    );

    manager.ev_shift_end.subscribe (
        "select_right_row_half",
        method(self, on_shift_end)
    );

    manager.ev_control_shift_home.subscribe (
        "select_left_text_half",
        method(self, on_control_shift_home)
    );

    manager.ev_control_shift_end.subscribe (
        "select_right_text_half",
        method(self, on_control_shift_end)
    );

    manager.ev_backspace.subscribe (
        "backspace", method(self, on_backspace)
    );

    manager.ev_delete.subscribe (
        "delete", method(self, on_delete)
    );

    manager.ev_control_backspace.subscribe (
        "erase_left_word",
        method(self, on_control_backspace)
    );

    manager.ev_control_delete.subscribe (
        "erase_right_word",
        method(self, on_control_delete)
    );

    manager.ev_control_shift_backspace.subscribe (
        "erase_left_row_half",
        method(self, on_control_shift_backspace)
    );

    manager.ev_control_shift_delete.subscribe (
        "erase_right_row_half",
        method(self, on_control_shift_delete)
    );

    manager.ev_select_all.subscribe (
        "select_all", method(self, on_select_all)
    );

    manager.ev_copy.subscribe (
        "copy", method(self, on_copy)
    );

    manager.ev_cut.subscribe (
        "cut", method(self, on_cut)
    );

    manager.ev_paste.subscribe (
        "paste", method(self, on_paste)
    );

    manager.ev_undo.subscribe (
        "undo", method(self, on_undo)
    );

    manager.ev_redo.subscribe (
        "redo", method(self, on_redo)
    );

    #endregion settings
}
