
/// @func GgAdapterStorageQueue()
///
/// @context GgAdapterStorage
///
function GgAdapterStorageQueue(): GgAdapterStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = ds_queue_create();

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func push(_item)
    ///
    /// @desc
    ///    Pushes an item into storage.
    ///    
    /// @arg {Any} _item
    ///    The element to add.
    ///
    /// @context GgAdapterStorageQueue
    ///
    static push = function(_item)
    {
        ds_queue_enqueue(content, _item);
    }

    /// @func get()
    ///
    /// @desc
    ///    Gets the element, but does not remove it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorageQueue
    ///
    static get = function()
    {
        return ds_queue_head(content);
    }

    /// @func put()
    ///
    /// @desc
    ///    Gets an element and removes it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorageQueue
    ///
    static put = function()
    {
        return ds_queue_dequeue(content);
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgAdapterStorageQueue
    ///
    static count = function()
    {
        return ds_queue_size(content);
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    The procedure that is applied to
    ///    an element when cleared|deleted.
    ///
    /// @context GgAdapterStorageQueue
    ///
    static clear = function(_proc)
    {
        process_all(_proc);
        ds_queue_clear(content);
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgAdapterStorageQueue
    ///
    static free = function()
    {
        ds_queue_destroy(content);
        content = -1;
    }

    #endregion public

    #endregion methods
}
