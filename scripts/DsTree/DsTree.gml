
/// @func GgDsTree()
///
/// @desc
///    Data structure representing from itself a tree.
///
/// @context undefined
///
function GgDsTree() constructor
{
    #region variables

    #region private

    root = new GgDsTreeNode(, "root", true);

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func include(_node, [_parent])
    ///
    /// @desc
    ///    Adds a node (and its children) to the tree.
    ///
    /// @arg {Struct.GgDsTreeNode} _node
    ///    The node of the tree to add to.
    ///
    /// @arg {Struct.GgDsTreeNode} _parent
    ///    the node that is used as the parent node.
    ///
    /// @context GgDsTree
    ///
    static include = function(_node, _parent = root)
    {
        if is_undefined(_parent)
        {
            // exception
        }

        _parent.add(_node);
    }

    /// @func exclude(_node, _parent)
    ///
    /// @desc
    ///    Removes a node (and its children) from the tree.
    ///
    /// @arg {Struct.GgDsTreeNode} _node
    ///    The node of the tree to remove to.
    ///
    /// @arg {Struct.GgDsTreeNode} _parent
    ///    the node that is used as the parent node.
    ///
    /// @context GgDsTree
    ///
    static exclude = function(_node, _parent = root)
    {
        if is_undefined(_parent)
        {
            // exception
        }

        _parent.remove(_node);
    }

    /// @func count([_with_closed])
    ///
    /// @desc
    ///    Returns the number of items and folders.
    ///    By default, it skips closed folders.
    ///    But a possible argument changes this behavior.
    ///
    /// @arg {Bool} _with_closed
    ///    Allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTree
    ///
    static count = function(_with_closed = false)
    {
        return root.get_count(_with_closed);
    }

    /// @func height([_with_closed])
    ///
    /// @desc
    ///    Returns the height of the tree (nesting depth).
    ///    By default, it skips closed folders.
    ///    But the passed argument changes this behavior.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTree
    ///
    static height = function(_with_closed = false)
    {
        return root.get_height(0, 0, _with_closed);
    }

    /// @func width([_with_closed])
    ///
    /// @desc
    ///    Returns the width of the tree.
    ///    By default, it skips closed folders.
    ///    But the passed argument changes this behavior.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTree
    ///
    static width = function(_with_closed = false)
    {
        return root.get_width(0, _with_closed);
    }

    /// @func each(_expression, [_with_closed])
    ///
    /// @desc
    ///    Checks if all elements are equal to the given value.
    ///    Works only when boolean values are stored in nodes.
    ///
    /// @arg {Any} _expression
    ///    The value to check.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Bool}
    ///
    /// @context GgDsTree
    ///
    static each = function(_expression, _with_closed = false)
    {
        var _data = {
            expression: _expression,
            value: true,
            with_closed: _with_closed,
        };

        var _result = root.each(_expression, _data, _with_closed);

        delete _data;

        return _result;
    }

    /// @func any(_expression, [_with_closed])
    ///
    /// @desc
    ///    Checks if it is equal to a certain value
    ///    at least one element. Works only when
    ///    boolean values are stored in nodes.
    ///
    /// @arg {Any} _expression
    ///    Value to check.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Bool}
    ///
    /// @context GgDsTree
    ///
    static any = function(_expression, _with_closed = false)
    {
        var _data = {
            expression: _expression,
            value: false,
            with_closed: _with_closed,
        };

        var _result = root.any(_expression, _data, _with_closed);

        delete _data;

        return _result;
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgDsTree
    ///
    static free = function()
    {
        root.free();
        delete root;
    }

    #endregion public

    #endregion methods
}
