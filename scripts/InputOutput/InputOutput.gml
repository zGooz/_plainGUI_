
/// @func gg_check_functional_keys_kc()
///
/// @desc
///    Check if one of the function
///    keys is pressed (f1 - f12).
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_functional_keys_kc()
{
    var _result = false;

    for (var _i = vk_f1; _i <= vk_f12; _i += 1)
    {
        _result = _result || keyboard_check(_i);
    }

    return _result;
}

/// @func gg_check_functional_keys_kk()
///
/// @desc
///    Check if one of the function
///    keys is pressed (f1 - f12).
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_functional_keys_kk()
{
    return keyboard_key >= vk_f1
        && keyboard_key <= vk_f12;
}

/// @func gg_check_arrows_keys_kc()
///
/// @desc
///    Checks if the arrow keys are pressed.
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_arrows_keys_kc()
{
    var _result = false;

    for (var _i = vk_left; _i <= vk_down; _i += 1)
    {
        _result = _result || keyboard_check(_i);
    }

    return _result;
}

/// @func gg_check_arrows_keys_kk()
///
/// @desc
///    Checks if the arrow keys are pressed.
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_arrows_keys_kk()
{
    return keyboard_key >= vk_left
        && keyboard_key <= vk_down;
}

/// @func gg_check_other_keys_kc()
///
/// @desc
///    Checks if other keys are pressed,
///    such as "caps lock" or "num lock".
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_other_keys_kc()
{
    return keyboard_check(91)   // Window key (start button)
        || keyboard_check(144)  // Numeric Lock
        || keyboard_check(12)   // RMB | ENTER simulate clicking
        || keyboard_check(93)   // RMB simulate clicking
        || keyboard_check(20);  // Capitals lock
}

/// @func gg_check_other_keys_kk()
///
/// @desc
///    Checks if other keys are pressed,
///    such as "caps lock" or "num lock".
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_other_keys_kk()
{
    return keyboard_key == 91   // Window key (start button)
        || keyboard_key == 144  // Numeric Lock
        || keyboard_key == 12   // RMB | ENTER simulate clicking
        || keyboard_key == 93   // RMB simulate clicking
        || keyboard_key == 20   // Capitals lock
}

/// @func gg_check_number_kc()
///
/// @desc
///    Checks if number keys are pressed.
///
/// @return {Bool}
///
/// @context ubdefined
///
function gg_check_number_kc()
{
    if keyboard_check(vk_shift)
    {
        return false;
    }

    var _numpad_state = false;
    var _numrow_state = false;

    for (var _i = vk_numpad0; _i <= vk_numpad9; _i += 1)
    {
        _numpad_state = _numpad_state || keyboard_check(_i);
    }

    for (var _i = 48; _i <= 57; _i += 1)
    {
        _numrow_state = _numrow_state || keyboard_check(_i);
    }

    return _numpad_state || _numrow_state;
}

/// @func gg_check_number_kk()
///
/// @desc
///    Checks if number keys are pressed.
///
/// @return {Bool}
///
/// @context ubdefined
///
function gg_check_number_kk()
{
    if keyboard_check(vk_shift)
    {
        return false;
    }

    var _numpad_state = keyboard_key >= vk_numpad0
                     && keyboard_key <= vk_numpad9;

    var _numrow_state = keyboard_key >= 48
                     && keyboard_key <= 57;

    return _numpad_state || _numrow_state;
}

/// @func gg_check_non_printable_characters(_char)
///
/// @desc
///    Checking if it's a non-printable character.
///
/// @arg {String} _char
///    The character to check.
///
/// @return {Bool}
///
/// @context undefined
///
function gg_check_non_printable_characters(_char)
{
    return _char == "\f"  // Form Feed (new page)
        || _char == "\a"  // Alert (bell)
        || _char == "\v"  // Vertical tabulation
        || _char == "\t"  // Horizontal tabulation
        || _char == "\n"  // Newline
        || _char == "\r"  // Carriage return
        || _char == "\b"  // Backspace
        || _char == " ";  // Space
}
