
/// @func GgWidget([_area])
///
/// @desc
///    Base object for all widgets.
///
/// @arg {Struct.GgWorkArea} _area
///    Widget workspace.
///
/// @context undefined
///
function GgWidget(_area = undefined) constructor
{
    #region variables

    #region public

    is_enable = true;
    is_visible = true;
    theme = new GgTheme();
    super_free = undefined;

    #endregion public

    #region private

    /// @ignore
    area = _area;

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func delete_resouces()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgWidget
    ///
    static delete_resouces = function(){}

    /// @func work_process()
    ///
    /// @desc
    ///    Procedure that updates the states
    ///    of the widget. And sends its events.
    ///
    /// @context GgWidget
    ///
    static work_process = function(){}

    /// @func update()
    ///
    /// @desc
    ///    Procedure that updates the states
    ///    of the widget. And sends its events.
    ///
    /// @context GgWidget
    ///
    static update = function()
    {
        if is_undefined(area)
        {
            return;
        }

        if is_enable
        {
            area.monitor();

            if area.has_focus
            {
                work_process();
            }
        }
    }

    /// @func draw()
    ///
    /// @desc
    ///    The procedure for displaying a widget on the screen.
    ///
    /// @context GgWidget
    ///
    static draw = function()
    {
        if is_undefined(area)
        {
            return;
        }

        if ! is_visible
        {
            return;
        }

        if PLATFORM_PC
        {
            draw_platform_pc();
        }
        else if PLATFORM_MOBILE
        {
            draw_platform_mobile();
        }
        else
        {
            // exception
        }
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgWidget
    ///
    static free = function()
    {
        theme.free();
        delete theme;
        delete_resouces();
    }

    #endregion public

    #region private

    /// @func draw_platform_pc()
    ///
    /// @desc
    ///    Draws skins depending on whether Is the
    ///    cursor hovered, is the mouse button pressed...
    ///
    /// @context GgWidget
    ///
    /// @ignore
    ///
    static draw_platform_pc = function()
    {
        if ! is_enable
        {
            theme.disable.draw();
            return;
        }

        if area.mouse_cursor_on
        {
            if area.has_mb_hold
            {
                theme.hold.draw();
            }
            else
            {
                theme.over.draw();
            }
        }
        else
        {
            if area.has_focus && area.has_mb_hold
            {
                theme.hold.draw();
            }
            else
            {
                theme.normal.draw();
            }
        }
    }

    /// @func draw_platform_mobile()
    ///
    /// @desc
    ///    Draws skins depending on whether Is your finger
    ///    on the screen, hover your finger over the widget...
    ///
    /// @context GgWidget
    ///
    /// @ignore
    ///
    static draw_platform_mobile = function()
    {
        if ! is_enable
        {
            theme.disable.draw();
            return;
        }

        if area.has_focus && area.has_mb_hold
        {
            theme.hold.draw();
        }
        else
        {
            theme.normal.draw();
        }
    }

    #endregion private

    #endregion methods
}
