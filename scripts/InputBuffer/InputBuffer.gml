
function GgInputBuffer() constructor
{
    static remember = function(_text = "", _from = NaN, _to = NaN)
    {
        if ! is_nan(_from)
        {
            before.from = _from;
        }

        if ! is_nan(_to)
        {
            before.to = _to;
        }

        if _text != ""
        {
            before.text = _text;
        }
    }

    static update = function(_text)
    {
        after.text = _text;
        text = _text;
    }

    static get = function()
    {
        return text;
    }

    static swap = function()
    {
        text = (text == before.text)
                ? after.text
                : before.text;
    }

    before = {
        from: 1,
        to: 1,
        text: "",
    };

    after = {
        from: 1,
        to: 1,
        text: "",
    };

    text = ""
}
