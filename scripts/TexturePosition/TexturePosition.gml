
/// @func GgTexturePosition([_u], [_v])
///
/// @desc
///    This component is required for
///    store texture coordinates
///
/// @arg {Real} _u
///    horizontal coordinate from 0 to 1,
///    but let's go beyond the limits,
///    for example, with "gpu_get_texrepeat() == true"
///
/// @arg {Real} _v
///    vertical coordinate from 0 to 1,
///    but let's go beyond the limits,
///    for example, with "gpu_get_texrepeat() == true"
///
/// @context undefined
///
function GgTexturePosition(_u = 0, _v = 0) constructor
{
    #region methods

    #region public

    /// @func set_position([_u], [_v])
    ///
    /// @desc
    ///    Set the position on the texture.
    ///
    /// @arg {Real} _u
    ///    horizontal coordinate from 0 to 1,
    ///    but let's go beyond the limits,
    ///    for example, with "gpu_get_texrepeat() == true"
    ///
    /// @arg {Real} _v
    ///    vertical coordinate from 0 to 1,
    ///    but let's go beyond the limits,
    ///    for example, with "gpu_get_texrepeat() == true"
    ///
    /// @context GgTexturePosition
    ///
    static set_position = function(_u, _v)
    {
        u = _u;
        v = _v;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    u = _u;
    v = _v;

    #endregion public

    #endregion variables
}
