
/// @func GgVertex(_position, _uvs, _brush)
///
/// @desc
///    This component is required to store
///    primitive vertex parameters:
///    position in space, position on the texture,
///    and brush (color and transparency)
///
/// @arg {Struct.GgPosition} _position
///    Position in space (XY).
///
/// @arg {Struct.GgTexturePosition} _uvs
///    Texture Position (UVS).
///
/// @arg {Struct.GgBrush} _brush
///    A brush that stores color and transparency.
///
/// @context undefined
///
function GgVertex(_position, _uvs, _brush) constructor
{
    #region methods

    #region public

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgVertex
    ///
    static free = function()
    {
        delete position;
        delete texture_pos;
        delete brush;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    position = _position;
    texture_pos = _uvs;
    brush = _brush;

    #endregion public

    #endregion variables
}
