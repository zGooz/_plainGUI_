
/// @func GgRangeGradient(_edge_left, _edge_right)
///
/// @desc
///    This component stores a pair of arbitrary
///    values (let's call them "boundaries").
///    Also provides a value return mechanism
///    depending on the "bounds" and the passed parameter
///
/// @arg {Real} _edge_left
///    leftmost value|boundary.
///
/// @arg {Real} _edge_right
///    rightmost value|boundary.
///
/// @context undefined
///
function GgRangeGradient(_edge_left, _edge_right) constructor
{
    #region methods

    #region public

    /// @func set_edges(_edge_left, _edge_right)
    ///
    /// @desc
    ///    Sets limit values.
    ///
    /// @arg {Real} _edge_left
    ///    leftmost value|boundary.
    ///
    /// @arg {Real} _edge_right
    ///    rightmost value|boundary.
    ///
    /// @context GgRangeGradient
    ///
    static set_edges = function(_edge_left, _edge_right)
    {
        edge_left = _edge_left;
        edge_right = _edge_right;
    }

    /// @func get_value(_t)
    ///
    /// @desc
    ///    Returns a value within the given limits,
    ///    according to the provided algorithm,
    ///    depending on the parameter.
    ///    Calculated via lerp(a, b, t) by default.
    ///
    /// @arg {Real} _t
    ///    Parameter, meaning values from 0 to 1,
    ///    but there is no prohibition to exit this range.
    ///
    /// @return {Real}
    ///
    /// @context GgRangeGradient
    ///
    static get_value = function(_t)
    {
        return value_from_param(_t);
    }

    /// @func reset_calculate_algorithm()
    ///
    /// @desc
    ///    Resets the value calculation
    ///    algorithm to the default algorithm.
    ///
    /// @context GgRangeGradient
    ///
    static reset_calculate_algorithm = function()
    {
        value_from_param = def_value_from_param;
    }

    #endregion public

    #region private

    /// @func def_value_from_param(_t)
    ///
    /// @desc
    ///    WARNING: This is the default algorithm.
    ///    Returns a value within the given limits,
    ///    according to the provided algorithm,
    ///    depending on the parameter.
    ///
    /// @arg {Real} _t
    ///    Parameter, meaning values from 0 to 1,
    ///    but there is no prohibition to exit this range.
    ///
    /// @return {Real}
    ///
    /// @context GgRangeGradient
    ///
    /// @ignore
    ///
    def_value_from_param = function(_t)
    {
        return lerp(edge_left, edge_right, _t);
    }

    #endregion private

    #endregion methods

    #region variables

    #region public

    value_from_param = def_value_from_param;

    #endregion public

    #region private

    /// @ignore
    edge_left = _edge_left;

    /// @ignore
    edge_right = _edge_right;

    #endregion private

    #endregion variables
}
