
/// GgSectors(_start_angle, _position)
///
/// @desc
///    Collection of shares coming from
///    starting angle, to check for collision
///    with separate shares.
///
/// @arg {Real} _start_angle
///    Angle from which beats start.
///
/// @arg {Struct.GgPosition} _position
///    The point that is used as the center.
///
/// @context undefined
///
function GgSectors(_start_angle, _position)
: GgRadianShape(_position) constructor
{
    #region methods

    #region public

    /// @func append([_angle])
    ///
    /// @desc
    ///    Adds an angle, forming a new slice.
    ///
    /// arg {Real} _angle
    ///    The angular size of the share.
    ///
    /// @context GgSectors
    ///
    static append = function(_angle = 1)
    {
        var _index = storage.count() - 1;
        var _rotation = storage.get(_index);

        storage.append(_rotation + _angle);
    }

    /// @func check(_index)
    ///
    /// @desc
    ///    Function to check if whether there was a
    ///    mouse click within the given boundaries.
    ///
    /// @arg {Real} _index
    ///    Sector position in storage.
    ///
    /// @return {Bool}
    ///
    /// @context GgSectors
    ///
    static check = function(_index = 1)
    {
        if _index < 1
        {
            // exception
        }

        var _mx = GG_GUI_MOUSE_X;
        var _my = GG_GUI_MOUSE_Y;

        var _direction = point_direction (
            position.x, position.y, _mx, _my
        );

        var _current_angle = storage.get(_index);
        var _previous_angle = storage.get(_index - 1);

        if is_undefined(_current_angle)
        {
            var _start_angle = storage.get(0);

            return _direction > _previous_angle
                && _direction > _start_angle;
        }

        return _direction > _previous_angle
            && _direction < _current_angle;
    }

    #endregion public

    #endregion methods

    #region settings

    storage.replace(_start_angle, 0);

    #endregion settings

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgSectors
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);

        storage.foreach(draw_direction, position);
        position.draw_gizmos(_color, _alpha);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    /// @func draw_direction(_direction, _point)
    ///
    /// @desc
    ///    Draws an arrow in the given direction.
    ///
    /// @arg {Real} _direction
    ///    Direction that is busy share.
    ///
    /// @arg {Struct.GgPosition} _point
    ///    General center.
    ///
    /// @context GgSectors
    ///
    /// @ignore
    ///
    static draw_direction = function(_direction, _point)
    {
        var _x = _point.x + lengthdir_x(200, _direction);
        var _y = _point.y + lengthdir_y(200, _direction);

        draw_arrow(_point.x, _point.y, _x, _y, 15);
    }

    #endregion debug
}
