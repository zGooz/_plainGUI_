/*
/// @func GgException(_data, _type)
/// @arg {Struct} _data
/// @arg {Real} _type
function GgException(_data = undefined, _type) constructor
{
    self.data = _data;
    self.type = _type ?? GG_EXCEPTION_TYPE.UNHANDLED;

    /// @func get_info()
    /// @return {String}
    static get_info = function()
    {
        return _data.message ?? "Unknow error. Exception message is empty.";
    }
}

/// @func gg_get_parsed_callstack()
/// @return {String}
function gg_get_parsed_callstack()
{
    var _parsed_staak = "";
    var _callstack = debug_get_callstack();

    for (var _i = 0; _i < array_length(_callstack); _i += 1)
    {
        _parsed_staak += string(_callstack[@ _i]) + "\n\n";
    }

    return _parsed_staak;
}

/// @func gg_get_invalid_argument_data(_index, _value, conditon, _from)
/// @arg {Real} _index
/// @arg {Any} _value
/// @arg {String} _condition
/// @arg {String} _from
/// @return {Struct}
function gg_get_invalid_argument_data(_index, _value, _condition, _from)
{
    var _parsed_stack = gg_get_parsed_callstack();

    return {
        message : @"Invalid argument _value with _index: ["
                  + string(_index)
                  + "]\n\n\nYou should give is\n{\n\t"
                  + _condition
                  + "\n}\n\n"
                  + "You give is:\n{\n\t"
                  + string(_value)
                  + "\n}"
                  + "\n\n\nexception throwed _from "
                  + _from
                  + "\n\n\ncallstack\n"
                  + _parsed_stack,
    };
}

/// @func gg_get_id_attribute_not_found_data(_index, _value, _condition, _from)
/// @arg {Struct} _id
/// @arg {String} _field_name
/// @arg {String} _from
/// @return {Struct}
function gg_get_id_attribute_not_found_data(_id, _field_name, _from)
{
    var _parsed_stack = gg_get_parsed_callstack();

    return {
        message : @"Field with name: ["
                  + _field_name
                  + "] IN struct \n\t"
                  + string(_id)
                  + "\nnot exists"
                  + "\n\n\n\nexception throwed _from "
                  + _from
                  + "\n\n\ncallstack\n"
                  + _parsed_stack,
    };
}

/// @func gg_get_out_of_range_data(_index, _value, _condition, _from)
/// @arg {Real} _index
/// @arg {Any} _value
/// @arg {String} _condition
/// @arg {String} _from
/// @return {Struct}
function gg_get_out_of_range_data(_index, _value, _condition, _from)
{
    var _parsed_stack = gg_get_parsed_callstack();

    return {
        message : @"Argument _value OUT of range, argument _index is: ["
                  + string(_index)
                  + "]\n\n\nYou should give is\n{\n\t"
                  + _condition
                  + "\n}\n\n"
                  + "You give is:\n{\n\t"
                  + string(_value)
                  + "\n}"
                  + "\n\n\nexception throwed _from "
                  + _from
                  + "\n\n\ncallstack\n"
                  + _parsed_stack,
    };
}
