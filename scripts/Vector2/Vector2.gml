
/// @func GgVector2()
///
/// @desc
///    The component will be applied in
///    tasks for using vectors.
///
/// @context undefined
///
function GgVector2() constructor
{
    #region methods

    #region public

    /// @func get_local_x()
    ///
    /// @desc
    ///    Returns the distance along the "x" axis
    ///    between start and end point.
    ///
    /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static get_local_x = function()
    {
        return to.x - from.x;
    }

    /// @func get_local_y()
    ///
    /// @desc
    ///    Returns the distance along the "y" axis
    ///    between start and end point.
    ///
    /// /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static get_local_y = function()
    {
        return to.y - from.y;
    }

    /// @func get_direction([_in_radisans])
    ///
    /// @desc
    ///    Returns the direction from the
    ///    start to end point of the vector.
    ///    The default is in degrees.
    ///
    /// @arg {Bool} _in_radisans
    ///    Whether to return the value in radians.
    ///
    /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static get_direction = function(_in_radisans = false)
    {
        var _angle = point_direction(from.x, from.y, to.x, to.y);

        if _in_radisans
        {
            _angle = degtorad(_angle);
        }

        return _angle;
    }

    /// @func get_length()
    ///
    /// @desc
    ///    Return distance bettween start and end points.
    ///
    /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static get_length = function()
    {
        return point_distance(from.x, from.y, to.x, to.y);
    }

    /// @func set([_x], [_y], [_len], [_dir])
    ///
    /// @desc
    ///    Specifies the position of the vector
    ///    (its start and end points) directly.
    ///
    /// @arg {Real} _x;
    ///    Position along the "x" axis for origin.
    ///
    /// @arg {Real} _y
    ///    Position along the "y" axis for origin.
    ///
    /// @arg {Real} _len
    ///    Distance to origin.
    ///
    /// @arg {Real} _dir
    ///    Direction from start to end point.
    ///
    /// @context GgVector2
    ///
    static set = function(_x = 0, _y = 0, _len = 1, _dir = 0)
    {
        var _tx = _x + lengthdir_x(_len, _dir);
        var _ty = _y + lengthdir_y(_len, _dir);

        from.set_position(_x, _y);
        to.set_position(_tx, _ty);
    }

    /// @func set_position([_x], [_y])
    ///
    /// @desc
    ///    Specifies the position of the vector directly.
    ///
    /// @arg {Real} _x;
    ///    Position along the "x" axis for origin.
    ///
    /// @arg {Real} _y
    ///    Position along the "y" axis for origin.
    ///
    /// @context GgVector2
    ///
    static set_position = function(_x = 0, _y = 0)
    {
        var _length = get_length();
        var _direction = get_direction();

        set(_x, _y, _length, _direction);
    }

    /// @func translate_at(_shift_x, _shift_y)
    ///
    /// @desc
    ///    Offsets the vector.
    ///
    /// @arg {Real} _shift_x
    ///    The amount of horizontal offset.
    ///
    /// @arg {Real} _shift_y
    ///    The amount of vertical offset.
    ///
    /// @context GgVector2
    ///
    static translate_at = function(_shift_x, _shift_y)
    {
        from.translate_at(_shift_x, _shift_y);
        to.translate_at(_shift_x, _shift_y);
    }

    /// @func rotate_at(_angle)
    ///
    /// @desc
    ///    Rotate the vector.
    ///
    /// @arg {Real} _angle
    ///    Angle of rotation.
    ///
    /// @context GgVector2
    ///
    static rotate_at = function(_angle)
    {
        var _dir = get_direction();
        var _dist = get_length();

        set(from.x, from.y, _dist, _dir + _angle);
    }

    /// @func skew(_another)
    ///
    /// @desc
    ///    Returns the skew product
    ///    between it and the vector passed as an argument.
    ///
    /// @arg {Struct.GgVector2} _another
    ///    Another vector to calculate.
    ///
    /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static skew = function(_another)
    {
        var _x = get_local_x();
        var _y = get_local_y();
        var _ax = _another.get_local_x();
        var _ay = _another.get_local_y();

        return _x * _ay - _y * _ax;
    }

    /// @func dot(_another)
    ///
    /// @desc
    ///    Returns the dot product
    ///    between it and the vector passed as an argument.
    ///
    /// @arg {Struct.GgVector2} _another
    ///    Another vector to calculate.
    ///
    /// @return {Real}
    ///
    /// @context GgVector2
    ///
    static dot = function(_another)
    {
        var _x = get_local_x();
        var _y = get_local_y();
        var _ax = _another.get_local_x();
        var _ay = _another.get_local_y();

        return dot_product(_x, _y, _ax, _ay);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgSegment
    ///
    static free = function()
    {
        delete from;
        delete to;
    }

    #endregion public

    #endregion methods

    #region variables

    #region private

    /// @ignore
    from = new GgPosition(0, 0);

    /// @ignore
    to = new GgPosition(1, 0);

    #endregion private

    #endregion variables

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgPosition
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);
        draw_arrow(from.x, from.y, to.x, to.y, 16);

        from.draw_gizmos(_color, _alpha);
        to.draw_gizmos(_color, _alpha);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    #endregion debug
}
