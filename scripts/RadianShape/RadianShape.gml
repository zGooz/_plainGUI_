
/// @func GgRadianShape(_position)
///
/// @desc
///    Parent component of circles
///    ownership handlers mouse
///    coordinates for geometric shapes.
///
/// @arg {Struct.GgPosition} _position
///    The point that is used as the center.
///
/// @context GgShape
///
function GgRadianShape(_position): GgShape() constructor
{
    #region variables

    #region public

    position = _position;

    #endregion public

    #region private

    /// @ignore
    storage = new GgSequenceStorageArray();

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func append([_value])
    ///
    /// @desc
    ///    Adds an element to complicate geometric shape.
    ///
    /// arg {Real}_value
    ///    A value that complicates the geometric shape.
    ///
    /// @context GgRadianShape
    ///
    static append = function(_value) {}

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgRadianShape
    ///
    static free = function()
    {
        storage.free();
        delete storage;
    }

    #endregion public

    #endregion methods
}
