
/// @func GgNumberLocker()
///
/// @desc
///    This component is used to "lock"
///    numbers within the given limits, more precisely,
///    they provide methods to implement them
///
/// @context undefined
///
function GgNumberLocker() constructor
{
    #region methods

    #region public

    /// @func set_chunk(_chunk)
    ///
    /// @desc
    ///    Sets the upper limit of the restrictions.
    ///
    /// @arg {Real} _chunk
    ///    upper bound.
    ///
    /// @context GgNumberLocker
    ///
    static set_chunk = function(_chunk)
    {
        if ! _chunk
        {
            // exception
        }

        chunk = _chunk;
    }

    /// @func get_morphed_number(_number)
    ///
    /// @desc
    ///    Changes the index (closes it in frames).
    ///
    /// @arg {Real} _number
    ///    Number to change.
    ///
    /// @return {Real}
    ///
    /// @context GgNumberLocker
    ///
    static get_morphed_number = function(_number)
    {
        var _value =  _number % chunk;

        if _value < 0
        {
            _value += chunk;
        }

        return _value;
    }

    #endregion public

    #endregion methods

    #region variables

    #region private

    /// @ignore
    chunk = 0;

    #endregion private

    #endregion variables
}
