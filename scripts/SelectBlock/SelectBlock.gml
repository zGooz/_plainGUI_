
/// @func GgBlock()
///
/// @desc
///    This component is needed by the text
///    editing engine to select text.
///
/// @context undefined
///
function GgBlock() constructor
{
    #region methods

    #region public

    /// @func has_select()
    ///
    /// @desc
    ///    Check if text is selected.
    ///
    /// @return {Bool}
    ///
    /// @context GgBlock
    ///
    static has_select = function()
    {
        return count > 0;
    }

    /// @func set_position(_pos, _total)
    ///
    /// @desc
    ///    Check if text is selected.
    ///
    /// @arg {Real} _pos
    ///    Specifies the position at which the selection starts.
    ///
    /// @arg {Real} _total
    ///    The position of the last character in the selected text.
    ///
    /// @return {Bool}
    ///
    /// @context GgBlock
    ///
    static set_position = function(_pos, _total)
    {
        position = clamp(_pos, 1, _total);
    }

    /// @func expand(_value, _total)
    ///
    /// @desc
    ///    Check if text is selected.
    ///
    /// @arg {Real} _value
    ///    Increment the number of selected
    ///    characters by the given argument.
    ///
    /// @arg {Real} _total
    ///    The position of the last character in the selected text.
    ///
    /// @return {Bool}
    ///
    /// @context GgBlock
    ///
    static expand = function(_value, _total)
    {
        if count + _value <= _total
        {
            count += _value;
        }
        else
        {
            count += max(0, count + _value - _total);
        }
    }

    /// @func collapse(_value)
    ///
    /// @desc
    ///    Check if text is selected.
    ///
    /// @arg {Real} _value
    ///    Decrement the number of selected
    ///    characters by the given argument.
    ///
    /// @return {Bool}
    ///
    /// @context GgBlock
    ///
    static collapse = function(_value)
    {
        count = max(count - _value, 0);
    }

    /// @func reset()
    ///
    /// @desc
    ///    Resets selection.
    ///
    /// @return {Bool}
    ///
    /// @context GgBlock
    ///
    static reset = function()
    {
        position = 1;
        count = 0;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    position = 1;
    count = 0;

    #endregion public

    #endregion variables
}
