
/// @func GgPrimitiveRenderer([_transform])
///
/// @desc
///    A component that is used to
///    form and draw primitives.
///
/// @arg {Struct.GgTransform} _transform
///    Transform, to orient the primitive.
///
/// @context undefined
///
function GgPrimitiveRenderer(_transform = undefined) constructor
{
    #region methods

    #region public

    /// @func set_visible_part(_percent)
    ///
    /// @desc
    ///    Specifies how many percent of
    ///    the primitive will be visible.
    ///
    /// @arg {Real} _percent
    ///    Percentage of visible area.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static set_visible_part = function(_percent)
    {
        part.collapse(infinity);

        var _len = storage.count();
        var _count = floor(_len * _percent);

        part.expand(_count);
    }

    /// @func shift_visible_part(_offset)
    ///
    /// @desc
    ///    Moves the visible area of the primitive.
    ///    For example, an area of 20% -60% was visible,
    ///    became visible 30% -70%.
    ///
    /// @arg {Real} _offset
    ///    Shift amount.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static shift_visible_part = function(_offset)
    {
        part.shift(_offset);
    }

    /// @func append(_vertex, [_id])
    ///
    /// @desc
    ///    Adds an vertex to the primitive.
    ///
    /// @arg {Any} _vertex
    ///    Vertex to add.
    ///
    /// @arg {Real} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static append = function(_vertex, _id = -1)
    {
        storage.append(_vertex, _id);
        locker.set_chunk(storage.count());
        part.expand();
    }

    /// @func draw()
    ///
    /// @desc
    ///    Renders a primitive based on vertex data.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static draw = function()
    {
        if is_undefined(transform)
        {
            return;
        }

        var _prev_tex_repeat = gpu_get_texrepeat();
        var _px = transform.position.x;
        var _py = transform.position.y;

        var _range = part.get();
        var _from = _range.from;
        var _to = _range.to;

        delete _range;

        gpu_set_texrepeat(is_repeat_texture);
        draw_primitive_begin_texture(type, texture);

        for (var _i = _from; _i < _to; _i += 1)
        {
            var _j = locker.get_morphed_number(_i);
            var _vertex = storage.get(_j);

            var _vx = _px + _vertex.position.x;
            var _vy = _py + _vertex.position.y;
            var _u = _vertex.texture_pos.u;
            var _v = _vertex.texture_pos.v;
            var _color = _vertex.brush.color;
            var _alpha = _vertex.brush.alpha;

            draw_vertex_texture_color (
                _vx, _vy, _u, _v, _color, _alpha
            );
        }

        draw_primitive_end();
        gpu_set_texrepeat(_prev_tex_repeat);
    }

    /// @func clear()
    ///
    /// @desc
    ///    Clears the primitive of vertexs.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static clear = function()
    {
        storage.clear(gg_free_item);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgPrimitiveRenderer
    ///
    static free = function()
    {
        clear();

        storage.free();
        delete storage;

        delete locker;
        delete part;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    transform = _transform;
    is_repeat_texture = true;
    type = pr_linestrip;
    texture = -1;

    #endregion public

    #region private

    /// @ignore
    locker = new GgNumberLocker();

    /// @ignore
    part = new GgMovableNumberLinePart();

    /// @ignore
    storage = new GgSequenceStorageArray();

    #endregion private

    #endregion variables
}
