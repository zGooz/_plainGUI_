
/// @func GgSequenceStorageArray()
///
/// @context GgSequenceStorage
///
function GgSequenceStorageArray(): GgSequenceStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = [];

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func append(_item)
    ///
    /// @desc
    ///    Adds an element to the store.
    ///
    /// @arg {Any} _item
    ///    Element to add.
    ///
    /// @context GgSequenceStorageArray
    ///
    static append = function(_item)
    {
        array_push(content, _item);
    }

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element from the storage.
    ///
    /// @arg {Real} _id
    ///    ID of the element to remove.
    ///
    /// @context GgSequenceStorageArray
    ///
    static remove = function(_id = -1)
    {
        try
        {
            array_delete(content, _id, 1);
        }
        catch (_error)
        {
            // exception
        }
    }

    /// @func replace(_item, _id)
    ///
    /// @desc
    ///    Edits an element or replaces
    ///    one element with another.
    ///
    /// @arg {Any} _item
    ///    Element to edit.
    ///
    /// @arg {Real} _id
    ///    Element ID, to edit|replace.
    ///
    /// @context GgSequenceStorageArray
    ///
    static replace = function(_item, _id = -1)
    {
        try
        {
            content[@ _id] =_item;
        }
        catch (_error)
        {
            // exception
        }
    }

    /// @func get(_id)
    ///
    /// @desc
    ///    Returning an element from storage.
    ///
    /// @arg {Real} _id
    ///    Element ID.
    ///
    /// @return {Any}
    ///
    /// @context GgSequenceStorageArray
    ///
    static get = function(_id = -1)
    {
        var _item;

        try
        {
            _item = content[@ _id];
        }
        catch (_error)
        {
            _item = undefined;
        }

        return _item;
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgSequenceStorageArray
    ///
    static count = function()
    {
        return array_length(content);
    }

    /// @func foreach(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgSequenceStorageArray
    ///
    static foreach = function(_proc, _data = undefined)
    {
        for (var _id = 0; _id < count(); _id += 1)
        {
            _proc(get(_id), _data);
        }
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    element when clearing|deleting.
    ///
    /// @context GgSequenceStorageArray
    ///
    static clear = function(_proc)
    {
        array_foreach(content, _proc);
        content = [];
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgSequenceStorageArray
    ///
    static free = function()
    {
        content = -1;
    }

    #endregion public

    #endregion methods
}
