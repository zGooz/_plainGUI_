
// @func GgDsTreeNode(_value, _name, _is_folder)
///
/// @desc
///    Tree element.
///
/// @arg {Any} _value
///    The value to pass.
///
/// @arg {Any} _name
///    The node name.
///
/// @arg {Bool} _is_folder
///    Whether the node is a container.
///
/// @context undefined
///
function GgDsTreeNode
(_value = undefined, _name = "", _is_folder = false)
constructor
{
    #region variables

    #region private

    /// @ignore
    name = _name;

    /// @ignore
    value = _value;

    /// @ignore
    is_folder = _is_folder;

    /// @ignore
    is_open = true;

    /// @ignore
    childs = new GgSequenceStorageStruct();

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func add(_node)
    ///
    /// @desc
    ///    Put the node in a container|storage|folder.
    ///
    /// @arg {Struct.GgDsTreeNode} _node
    ///    The node of the tree to add to.
    ///
    /// @context GgDsTreeNode
    ///
    static add = function(_node)
    {
        if is_folder
        {
            childs.append(_node, _node.name);
        }
        else
        {
            // exception
        }
    }

    /// @func remove(_node)
    ///
    /// @desc
    ///    Kick the node in a container|storage|folder.
    ///
    /// @arg {Struct.GgDsTreeNode} _node
    ///    The node of the tree to remove to.
    ///
    /// @context GgDsTreeNode
    ///
    static remove = function(_node)
    {
        childs.remove(_node.name);
    }

    /// @func expand([_with_child])
    ///
    /// @desc
    ///    Expand (open) the folder.
    ///
    /// @arg {Bool} _with_child
    ///    Whether to expand child elements.
    ///
    /// @context GgDsTreeNode
    ///
    static expand = function(_with_child = false)
    {
        if ! is_folder
        {
            return;
        }

        is_open = true;

        if _with_child
        {
            childs.foreach (
                function(_item) {
                    _item.expand(true);
                }
            );
        }
    }

    /// @func collapse([_with_child])
    ///
    /// @desc
    ///    Minimizes (closes) the folder.
    ///
    /// @arg {Bool} _with_child
    ///    Whether to collapse child elements.
    ///
    /// @context GgDsTreeNode
    ///
    static collapse = function(_with_child = false)
    {
        if ! is_folder
        {
            return;
        }

        is_open = false;

        if _with_child
        {
            childs.foreach (
                function(_item) {
                    _item.collapse(true);
                }
            );
        }
    }

    /// @func get_count([_with_closed])
    ///
    /// @desc
    ///    Returns the number of items and folders.
    ///    By default, it skips closed folders.
    ///    But the passed argument changes this behavior.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTreeNode
    ///
    static get_count = function(_with_closed = false)
    {
        var _count = childs.count();

        var _data = {
            count: _count,
            with_closed: _with_closed,
        };

        var _get_count = function(_item, _data)
        {
            if ! _item.is_folder
            {
                return;
            }

            if _data.with_closed || _item.is_open
            {
                _data.count += _item.get_count (
                    _data.with_closed
                );
            }
        }

        childs.foreach(_get_count, _data);
        _count = _data.count;

        delete _data;

        return _count;
    }

    /// @func get_width(_width, [_with_closed])
    ///
    /// @desc
    ///    Returns the width of the tree.
    ///    By default, it skips closed folders.
    ///    But the passed argument changes this behavior.
    ///
    /// @arg {Real}_width
    ///    The current width of the tree.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTreeNode
    ///
    static get_width = function(_width, _with_closed = false)
    {
        var _data = {
            width: _width,
            with_closed: _with_closed,
        };

        var _get_width = function(_item, _data)
        {
            if ! _item.is_folder
            {
                _data.width += 1;

                return;
            }

            if _data.with_closed || _item.is_open
            {
                _data.width = _item.get_width (
                    _data.width, _data.with_closed
                );
            }
        }

        childs.foreach(_get_width, _data);

        var _result_width = _data.width;

        delete _data;

        return _result_width;
    }

    /// @func get_height
    ///(_check_height, _heig_current_heightht, [_with_closed])
    ///
    /// @desc
    ///    Returns the height of the subtree (nesting depth).
    ///    By default, closed folders are skipped.
    ///    But the passed argument changes this behavior.
    ///
    /// @arg {Real} _check_height
    ///    Whether to allow bypassing closed folders.
    ///
    /// @arg {Real} _current_height
    ///    Whether to allow bypassing closed folders.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Real}
    ///
    /// @context GgDsTreeNode
    ///
    static get_height = function
    (_check_height, _current_height, _with_closed = false)
    {
        var _data = {
            level: _current_height,
            level_up: _check_height + 1,
            with_closed: _with_closed,
        };

        var _get_height = function(_item, _data)
        {
            if ! _item.is_folder
            {
                return;
            }

            var _up = _data.level_up;

            if _data.with_closed || _item.is_open
            {
                _up = _item.get_height (
                    _data.deep, _up, _data.with_closed
                );
            }

            _data.level = max(_up, _data.level);
        }

        childs.foreach(_get_height, _data);

        var _height = _data.level;

        delete _data;

        return _height;
    }

    /// @func each(_expression, _data, [_with_closed])
    ///
    /// @desc
    ///    Checks if all elements are equal to the given value.
    ///    Works only when boolean values are stored in nodes.
    ///
    /// @arg {Any} _expression
    ///    The value to check.
    ///
    /// @arg {Struct} _data
    ///    Auxiliary data.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Bool}
    ///
    /// @context GgDsTreeNode
    ///
    static each = function(_expression, _data, _with_closed = false)
    {
        var _any = function(_item, _data)
        {
            var _value = _data.value;

            if ! _value
            {
                return;
            }

            if _item.is_folder
            {
                if _data.with_closed || _item.is_open
                {
                    _data.value = _item.each (
                        _data.expression,
                        _data,
                        _data.with_closed
                    );
                }
            }
            else
            {
                if _item.value != _data.expression
                {
                    _data.value = false;
                }
            }
        }

        childs.foreach(_any, _data);

        return _data.value;
    }

    /// @func once(_expression, _data, [_with_closed])
    ///
    /// @desc
    ///    Checks if it is equal to a certain value
    ///    at least one element. Works only when
    ///    boolean values are stored in nodes.
    ///
    /// @arg {Any} _expression
    ///    Value to check.
    ///
    /// @arg {Struct} _data
    ///    Auxiliary data.
    ///
    /// @arg {Bool} _with_closed
    ///    Whether to allow bypassing closed folders.
    ///
    /// @return {Bool}
    ///
    /// @context GgDsTreeNode
    ///
    static once = function(_expression, _data, _with_closed = false)
    {
        var _once = function(_item, _data)
        {
            var _value = _data.value;

            if _value
            {
                return;
            }

            if _item.is_folder
            {
                if _data.with_closed || _item.is_open
                {
                    _data.value = _item.once (
                        _data.expression,
                        _data,
                        _data.with_closed
                    );
                }
            }
            else
            {
                if _item.value == _data.expression
                {
                    _data.value = true;
                }
            }
        }

        childs.foreach(_once, _data);

        return _data.value;
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgDsTreeNode
    ///
    static free = function()
    {
        childs.foreach(gg_free_item);
        childs.free();
        delete childs;
    }

    #endregion public

    #endregion methods
}
