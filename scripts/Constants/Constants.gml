
#region macroses

#macro GG_GUI_MOUSE_X device_mouse_x_to_gui(0)
#macro GG_GUI_MOUSE_Y device_mouse_y_to_gui(0)
#macro GG_GUI_WIDTH   display_get_gui_width()
#macro GG_GUI_HEIGHT  display_get_gui_height()

#macro GG_DISPLAY_WIDTH  display_get_width()
#macro GG_DISPLAY_HEIGHT display_get_height()

#macro GG_DISPLAY_RATIO \
       (GG_DISPLAY_WIDTH / GG_DISPLAY_HEIGHT)

#macro GG_DEFAULT_TRANSFORM_SIZE_WIDTH  64
#macro GG_DEFAULT_TRANSFORM_SIZE_HEIGHT 64

#macro GG_CR   "\r"
#macro GG_LF   "\n"
#macro GG_CRLF "\r\n"

#macro GG_PAGE_SIZE 14
#macro GG_TABULATION_INDENTATION 4
#macro GG_SKELETON_TIME_FRAME_SPEED \
        (delta_time / 1000000)

#macro PLATFORM_PC               \
      (os_type == os_windows     \
    || os_type == os_linux       \
    || os_type == os_macosx      \
    || os_type == os_uwp         \
    || os_type == os_win8native)

#macro PLATFORM_MOBILE          \
       (os_type == os_android   \
     || os_type ==  os_ios      \
     || os_type == os_winphone)

#endregion macroses

#region enums

enum GG_CURSOR
{
    ENTER,
    LEAVE,
}

enum GG_SKEW_PRODUCT
{
    LESS_ZERO,
    EQUALS_ZERO,
    ABOVE_ZERO,
}

enum GG_PRIORITY
{
    GET_MIN,
    GET_MAX,
}

enum GG_FLOW
{
    TO_HEAD = 0,
    TO_TAIL = 1,
    FORWARD = 1,
    BACKWARD = -1,
}

enum GG_WINDOW_IS
{
    WINDOWED,
    FULL_SCREEN,
}

enum GG_TEXT_LENGTH
{
    DEFAULT = 32,
    MIN = 3,
    MAX = 1000,
}

#endregion enums

#region global variables

globalvar gg_time;
gg_time = {
    interval: 1,
    delay: 0.5,
    type_delay: 0.8,
};

#region TEMP

globalvar gg_def_sprites;
gg_def_sprites = {
    pixel_image: spr_image,
    nineslice_image: spr_nineslice,
    skeleton_image: spr_powerup,
    cursor: spr_cursor,
};

globalvar gg_def_fonts;
gg_def_fonts = {
    mono_family: fnt_en_ru,
    pixel_family: fnt_pixel,
};

globalvar gg_def_paths;
gg_def_paths = {
    curve: pth_curve,
    polygon: pth_polygon,
};

#endregion TEMP

#endregion global variables
