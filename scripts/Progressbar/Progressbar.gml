
/// @func GgProgressbar(_area)
///
/// @arg {Struct.GgWorkArea} _area
///    Widget workspace.
///
/// @context GgWidget
///
function GgProgressbar(_area = undefined)
: GgWidget(_area = undefined) constructor
{
    #region methods

    #region public

    /// @func reset()
    ///
    /// @desc
    ///    Resets the progress bar.
    ///
    /// @context GgProgressbar
    ///
    static reset = function()
    {
        if flow == GG_FLOW.FORWARD
        {
            percent = 0;
        }
        else if flow == GG_FLOW.BACKWARD
        {
            percent = 1;
        }
        else
        {
            // exception
        }

        value = gradient.get_value(percent);
    }

    /// @func run()
    ///
    /// @desc
    ///    Launches the progress bar.
    ///
    /// @context GgProgressbar
    ///
    static run = function()
    {
        if is_auto
        {
            is_run = true;
        }
    }

    /// @func pause()
    ///
    /// @desc
    ///    Pauses the progress bar.
    ///
    /// @context GgProgressbar
    ///
    static pause = function()
    {
        if is_auto
        {
            is_run = false;
        }
    }

    /// @func change_by(_step)
    ///
    /// @desc
    ///    Changes the progress bar. Only works
    ///    when the "is_auto" flag is disabled
    ///
    /// @context GgProgressbar
    ///
    static change_by = function(_step)
    {
        if ! is_auto
        {
            apply_changes(_step);
        }
    }

    /// @func update(_step)
    ///
    /// @desc
    ///    Changes the progress bar. Only works
    ///    when the "is_auto" flag is enabled
    ///
    /// @context GgProgressbar
    ///
    static update = function()
    {
        if is_auto && is_run
        {
            apply_changes(step);
        }
    }

    /// @func delete_resouces()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgProgressbar
    ///
    static delete_resouces = function()
    {
        ev_emptyted.free();
        delete ev_emptyted;

        ev_filled.free();
        delete ev_filled;

        ev_value_changed.free();
        delete ev_value_changed;

        delete gradient;
    }

    #endregion public

    #region private

    /// @func apply_changes(_step, _id)
    ///
    /// @desc
    ///    Changes the progress indication.
    ///    In parallel, notifying that it is full,
    ///    emptied or changed progressbar.
    ///
    /// @context GgProgressbar
    ///
    /// @ignore
    ///
    static apply_changes = function(_step)
    {
        var _percent_old = percent;
        percent += _step * flow;

        if percent < 0
        {
            percent = 0;
            is_run = false;
            ev_emptyted.notify();
        }

        if percent > 1
        {
            percent = 1;
            is_run = false;
            ev_filled.notify();
        }

        value = gradient.get_value(percent);

        if _percent_old != percent
        {
            var _sub = percent - _percent_old;
            ev_value_changed.notify(_sub);
        }
    }

    #endregion private

    #endregion methods

    #region events

    ev_emptyted = new GgEvent();
    ev_filled = new GgEvent();
    ev_value_changed = new GgEvent();

    #endregion events

    #region variables

    #region public

    is_auto = false;
    step = 0.01;
    flow = GG_FLOW.FORWARD;

    #endregion public

    #region private

    /// @ignore
    is_run = false;

    /// @ignore
    percent = 0;

    /// @ignore
    value = 0;

    /// @ignore
    gradient = new GgRangeGradient(0, 100);

    #endregion private

    #endregion variables
}
