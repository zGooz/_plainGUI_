
/// @func GgAdapterStorageStack()
///
/// @context GgAdapterStorage
///
function GgAdapterStorageStack(): GgAdapterStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = ds_stack_create();

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func push(_item)
    ///
    /// @desc
    ///    Pushes an item into storage.
    ///    
    /// @arg {Any} _item
    ///    The element to add.
    ///
    /// @context GgAdapterStorageStack
    ///
    static push = function(_item)
    {
        ds_stack_push(content, _item);
    }

    /// @func get()
    ///
    /// @desc
    ///    Gets the element, but does not remove it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorageStack
    ///
    static get = function()
    {
        return ds_stack_top(content);
    }

    /// @func put()
    ///
    /// @desc
    ///    Gets an element and removes it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorageStack
    ///
    static put = function()
    {
        return ds_stack_pop(content);
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgAdapterStorageStack
    ///
    static count = function()
    {
        return ds_stack_size(content);
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    The procedure that is applied to
    ///    an element when cleared|deleted.
    ///
    /// @context GgAdapterStorageStack
    ///
    static clear = function(_proc)
    {
        process_all(_proc);
        ds_stack_clear(content);
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgAdapterStorageStack
    ///
    static free = function()
    {
        ds_stack_destroy(content);
        content = -1;
    }

    #endregion public

    #endregion methods
}
