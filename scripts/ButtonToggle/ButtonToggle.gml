
/// @fnc GgButtonToggle()
///
/// @desc
///    Button that toggles values on each subsequent click.
///
/// @arg {Struct.GgWorkArea} _area
///    Widget workspace.
///
/// @arg {Struct.GgShape} _precisioned_area
///    A more precise mask to check for a mouse click.
///
/// @context GgButton
///
function GgButtonToggle(_area = undefined, _precisioned_area = undefined)
: GgButton(_area = undefined, _precisioned_area = undefined) constructor
{
    #region methods

    #region public

    /// @func append(_value)
    ///
    /// @desc
    ///    Adds a value to the button.
    ///
    /// @arg {Any} _value
    ///    Value to add to the button.
    ///
    /// @context GgButtonToggle
    ///
    static append = function(_value)
    {
        storage.append(_value);
    }

    /// @func delete_resouces()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgButtonToggle
    ///
    static delete_resouces = function()
    {
        if ! is_method(super_free)
        {
            method_call(super_free, []);
        }

        ev_value_changed.free();
        delete ev_value_changed;

        storage.clear(gg_free_item);
        storage.free();
        delete storage;
    }

    #endregion public

    #region private

    /// @func on_click()
    ///
    /// @desc
    ///    Response to a click.
    ///    This causes the value to change.
    ///
    /// @context GgButtonToggle
    ///
    /// @ignore
    ///
    static on_click = function()
    {
        var _value_old = storage.get_element();
        storage.step();
        var _value_now = storage.get_element();

        if _value_old != _value_now
        {
            ev_value_changed.notify(_value_now.value);
        }
    }

    #endregion private

    #endregion methods

    #region events

    ev_value_changed = new GgEvent();

    #endregion events

    #region variables

    #region private

    /// @ignore
    storage = new GgLinkedListWalker();

    #endregion private

    #endregion variables

    #region settings

    ev_click.subscribe("toggle", method(self, on_click));

    #endregion settings
}
