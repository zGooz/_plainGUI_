
/// @func GgSkin()
///
/// @desc
///    This component, which is used for
///    set the appearance of the widget.
///    The component data group can be used,
///    to create entire skins.
///
/// @context undefined
///
function GgSkin() constructor
{
    #region methods

    #region public

    /// @func append(_iten, _id)
    ///
    /// @desc
    ///    Adds an element, which is used when
    ///    drawing the widget, these elements form
    ///    skins and even themes.
    ///
    /// @arg {Any} _item
    ///    Element, WARNING: it must have
    ///    the "draw" method must be present.
    ///
    /// @arg {String} _id
    ///    The name of the element under which the
    ///    element will be available.
    ///
    /// @context GgSkin
    ///
    static append = function(_item, _id)
    {
        storage.append(_item, _id);
    }

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element with the given name.
    ///
    /// @arg {String} _id
    ///    Element.
    ///
    /// @context GgSkin
    ///
    static remove = function(_id)
    {
        storage.remove(_id);
    }

    /// @func get(_id)
    ///
    /// @desc
    ///    Provides an element for external use.
    ///
    /// @arg {String} _id
    ///    The name of the element under which it is saved.
    ///
    /// @return {Any}
    ///    An element for external use.
    ///
    /// @context GgSkin
    ///
    static get = function(_id)
    {
        return storage.get(_id);
    }

    /// @func draw()
    ///
    /// @desc
    ///    Renders all elements that are in storage.
    ///
    /// @context GgSkin
    ///
    static draw = function()
    {
        storage.foreach(draw_item);
    }

    /// @func free([_destroy_item])
    ///
    /// @desc
    ///    Frees the occupied memory,
    ///    applies to each element.
    ///
    /// @context GgSkin
    ///
    static free = function()
    {
        storage.clear(destroy_item);
        storage.free();
        delete storage;
    }

    #endregion public

    #region private

    /// @func draw_item(_item)
    ///
    /// @desc
    ///    Draws a specific element.
    ///
    /// @arg {Any} _item
    ///    Element to be rendered.
    ///
    /// @context GgSkin
    ///
    /// @ignore
    ///
    static draw_item = function(_item)
    {
        _item.draw();
    }

    #endregion private

    #endregion methods

    #region variables

    #region public

    destroy_item = gg_free_item;

    #endregion public

    #region private

    /// @ignore
    storage = new GgSequenceStorageStruct();

    #endregion private

    #endregion variables
}
