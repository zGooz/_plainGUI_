
/// @func GgButtonRepeat([_area], [_precisioned_area])
///
/// @desc
///    Button which, if pressed, performs
///    the specified action periodically.
///
/// @arg {Struct.GgWorkArea} _area
///    Widget workspace.
///
/// @arg {Struct.GgShape} _precisioned_area
///    A more precise mask to check for a mouse click.
///
/// @context GgButton
///
function GgButtonRepeat(_area = undefined, _precisioned_area = undefined)
: GgButton(_area = undefined, _precisioned_area = undefined) constructor
{
    #region methods

    #region public

    /// @func work_process()
    ///
    /// @desc
    ///    Procedure that updates the states
    ///    of the widget. And sends its events.
    ///
    /// @context GgButtonRepeat
    ///
    static work_process = function()
    {
        if ! area.has_mb_hold
        {
            time_source_stop(repeat_timer);
            time_source_stop(run_timer);
        }
    }

    /// @func delete_resouces()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgButtonRepeat
    ///
    static delete_resouces = function()
    {
        if ! is_method(super_free)
        {
            method_call(super_free, []);
        }

        ev_make.free();
        delete ev_make;

        time_source_destroy(repeat_timer);
        time_source_destroy(run_timer);
    }

    #endregion public

    #region private

    /// @func run(_id)
    ///
    /// @desc
    ///    Triggers the button's action.
    ///    Then starts a self-starting timer,
    ///    what will happen at the end of its work,
    ///    trigger the button action.
    ///
    /// @arg {Struct.GgButtonRepeat} _id
    ///    Button ID.
    ///
    /// @context GgButtonRepeat
    ///
    /// @ignore
    ///
    static run = function(_id)
    {
        time_source_start(_id.repeat_timer);
        _id.ev_make.notify();
    }

    /// @func action(_id)
    ///
    /// @desc
    ///    Triggers a button action.
    ///
    /// @arg {Struct.GgButtonRepeat} _id
    ///    Button ID.
    ///
    /// @context GgButtonRepeat
    ///
    /// @ignore
    ///
    static action = function(_id)
    {
        _id.ev_make.notify();
    }

    /// @func on_poke()
    ///
    /// @desc
    ///    Triggers the button, it will
    ///    work until LMB is released.
    ///
    /// @context GgButtonRepeat
    ///
    /// @ignore
    ///
    static on_poke = function()
    {
        if is_undefined(precisioned_area)
        {
            time_source_start(run_timer);
        }
        else
        {
            if precisioned_area.check()
            {
                time_source_start(run_timer);
            }
        }
    }

    /// @func on_over_start()
    ///
    /// @desc
    ///    Resume button operation if it has been suspended.
    ///
    /// @context GgButtonRepeat
    ///
    /// @ignore
    ///
    static on_over_start = function()
    {
        if area.has_focus
        {
            if is_undefined(precisioned_area)
            {
                if area.has_mb_hold
                {
                    time_source_resume(repeat_timer);
                }
            }
            else
            {
                if precisioned_area.check()
                && area.has_mb_hold
                {
                    time_source_resume(repeat_timer);
                }
            }
        }
    }

    /// @func on_over_end()
    ///
    /// @desc
    ///    Pauses the button under certain conditions.
    ///
    /// @context GgButtonRepeat
    ///
    /// @ignore
    ///
    static on_over_end = function()
    {
        if area.has_focus
        {
            if is_undefined(precisioned_area)
            {
                if area.has_mb_hold
                {
                    time_source_pause(repeat_timer);
                }
            }
            else
            {
                if precisioned_area.check()
                && area.has_mb_hold
                {
                    time_source_pause(repeat_timer);
                }
            }
        }
    }

    #endregion private

    #endregion methods

    #region events

    ev_make = new GgEvent();

    #endregion events

    #region variables

    #region private

    /// @ignore
    interval = gg_time.interval;

    /// @ignore
    delay = gg_time.delay;

    /// @ignore
    run_timer = time_source_create (
        time_source_game, interval,
        time_source_units_seconds,
        run, [self], 1
    );

    /// @ignore
    repeat_timer = time_source_create (
        time_source_game, delay,
        time_source_units_seconds,
        action, [self], -1
    );

    #endregion private

    #endregion variables

    #region settings

    area.ev_click.unsubscribe("click");
    area.ev_poke.subscribe("poke", method(self, on_poke));
    area.ev_over_start.subscribe("enter", method(self, on_over_start));
    area.ev_over_end.subscribe("leave", method(self, on_over_end));

    #endregion settings
}
