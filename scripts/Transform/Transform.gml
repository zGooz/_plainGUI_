
/// @func GgTransform()
///
/// @desc
///    This component is required for
///    positioning and transformations.
///    Possible (in theory) and applications to animations.
///
/// @context undefined
///
function GgTransform() constructor
{
    #region methods

    #region public

    /// @func set_position(_x, _y)
    ///
    /// @desc
    ///    Sets the position of the transform directly.
    ///
    /// @arg {Real} _x
    ///    New horizontal position.
    ///
    /// @arg {Real} _y
    ///    New vartical position.
    ///
    /// @context GgTransform
    ///
    static set_position = function(_x, _y)
    {
        position.set_position(_x, _y);
    }

    /// @func translate_at(_shift_x, _shift_y)
    ///
    /// @desc
    ///    Offsets the transform.
    ///
    /// @arg {Real} _shift_x
    ///    The amount of horizontal offset.
    ///
    /// @arg {Real} _shift_y
    ///    The amount of vertical offset.
    ///
    /// @context GgTransform
    ///
    static translate_at = function(_shift_x, _shift_y)
    {
        position.translate_at(_shift_x, _shift_y);
    }

    /// @func set_rotation(_angle)
    ///
    /// @desc
    ///    Set tranform rotation directky.
    ///
    /// @arg {Real} _angle
    ///    Rotation value.
    ///
    /// @context GgTransform
    ///
    static set_rotation = function(_angle)
    {
        rotation = locker.get_morphed_number(_angle);
    }

    /// @func rotate_at(_angle_addition)
    ///
    /// @desc
    ///    Rotate the transform.
    ///
    /// @arg {Real} _angle_addition
    ///    Angle of rotation.
    ///
    /// @context GgTransform
    ///
    static rotate_at = function(_angle_addition)
    {
        var _angle = rotation + _angle_addition;
        rotation = locker.get_morphed_number(_angle);
    }

    /// @func set_scale(_factor_x, _factor_y)
    ///
    /// @desc
    ///    Changes the size of the transform.
    ///
    /// @arg {Real} _factor_x
    ///    Horizontal scaling factor.
    ///
    /// @arg {Real} _factor_y
    ///    Vertical scaling factor.
    ///
    /// @context GgTransform
    ///
    static set_scale = function(_factor_x, _factor_y)
    {
        if only_positive_scale
        {
            if _factor_x < 0
            {
                // exception
            }

            if _factor_y < 0
            {
                // exception
            }
        }

        scale.factor_x = _factor_x;
        scale.factor_y = _factor_y;
    }

    /// @func rescale_at(_factor_x_add, _factor_y_add)
    ///
    /// @desc
    ///    Scales the transform.
    ///
    /// @arg {Real} _factor_x_add
    ///    Horizontal addition for scaling factor.
    ///
    /// @arg {Real} _factor_y_add
    ///    Vertical addition for scaling factor.
    ///
    /// @context GgTransform
    ///
    static rescale_at = function(_factor_x_add, _factor_y_add)
    {
        var _new_factor_x = scale.factor_x + _factor_x_add;
        var _new_factor_y = scale.factor_y + _factor_y_add;

        if only_positive_scale
        {
            scale.factor_x = max(0, _new_factor_x);
            scale.factor_y = max(0, _new_factor_y);

            return;
        }

        scale.factor_x = _new_factor_x;
        scale.factor_y = _new_factor_y;
    }

    /// @func resize(_width, _height)
    ///
    /// @desc
    ///    Changes the size of the transform.
    ///
    /// @arg {Real} _width
    ///    Transform width.
    ///
    /// @arg {Real} _height
    ///    Transform height.
    ///
    /// @context GgTransform
    ///
    static resize = function(_width, _height)
    {
        if ! _width
        {
            // exception
        }

        if ! _height
        {
            // exception
        }

        size.width = _width;
        size.height = _height;
    }

    /// @func build()
    ///
    /// @desc
    ///    Rebuilds the transform.
    ///
    /// @context GgTransform
    ///
    static build = function()
    {
        var _half_width = (size.width * scale.factor_x) / 2;
        var _half_height = (size.height * scale.factor_y) / 2;
        var _radius = sqrt(sqr(_half_width) + sqr(_half_height));
        var _ratio = _half_height / _half_width;

        var _start_angle = radtodeg(arctan2(_ratio, 1));
        var _small_angle = _start_angle * 2;
        var _big_angle = 180 - _small_angle;

        var _adds = [
            _big_angle,
            _small_angle,
            _big_angle,
            _small_angle,
        ];

        var _add = _adds[0];
        var _number = 0;

        for (var _i = _start_angle; _i < 360; _i += _add)
        {
            var _direction = _i + rotation;
            var _px = position.x + lengthdir_x(_radius, _direction);
            var _py = position.y + lengthdir_y(_radius, _direction);

            path_change_point(bound, _number, _px, _py, 0);

            _add = _adds[_number];
            _number += 1;
        }
    }

    /// @func recalculate_shape_property()
    ///
    /// @desc
    ///    Re-calculates the data needed to
    ///    test mouse clicks on the transform.
    ///
    /// @context GgTransform
    ///
    static recalculate_shape_property = function()
    {
        if ! is_undefined(top_left_corner)
        {
            delete top_left_corner;
            local_positions = -1;
        }

        top_left_corner = gg_path_top_left_corner_pos (
            bound
        );

        local_positions = gg_dist_to_top_left_corner (
            bound, top_left_corner
        );
    }

    /// @func cursor_is_over()
    ///
    /// @desc
    ///    Checks if the mouse is hovering over it.
    ///
    /// @return {Bool}
    ///
    /// @context GgTransform
    ///
    static cursor_is_over = function()
    {
        return gg_point_in_polygon (
            top_left_corner,
            top_left_corner,
            local_positions
        );
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgTransform
    ///
    static free = function()
    {
        delete position;
        delete size;
        delete scale;
        delete locker;

        path_delete(bound);
    }

    #endregion public methods

    #endregion methods

    #region variables

    #region public

    only_positive_scale = false;

    #endregion public

    #region private

    /// @ignore
    position = new GgPosition(0, 0);

    /// @ignore
    rotation = 0;

    /// @ignore
    size = {
        width: GG_DEFAULT_TRANSFORM_SIZE_WIDTH,
        height: GG_DEFAULT_TRANSFORM_SIZE_HEIGHT,
    };

    /// @ignore
    scale = {
        factor_x: 1,
        factor_y: 1,
    };

    /// @ignore
    bound = path_add();

    /// @ignore
    top_left_corner = undefined;

    /// @ignore
    local_positions = [];

    /// @ignore
    locker = new GgNumberLocker();

    #endregion private

    #endregion variables

    #region settings

    repeat 4
    {
        path_add_point(bound, 0, 0, 0);
    }

    build();
    locker.set_chunk(360);

    #endregion settings

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgTransform
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);

        draw_primitive_begin(pr_linestrip);

        var _count = path_get_number(bound);

        for (var _i = 0; _i < _count + 1; _i += 1)
        {
            var _j = _i == _count ? 0 : _i;
            var _vx = path_get_point_x(bound, _j);
            var _vy = path_get_point_y(bound, _j);

            draw_vertex(_vx, _vy);
        }

        draw_primitive_end();

        position.draw_gizmos(_color, _alpha);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    #endregion debug
}
