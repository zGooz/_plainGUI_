/*
/// func GgAnimator()
function GgAnimator() constructor
{
    animations = ds_list_create();
    index = 0;
    param = 0;
    size = 0;
    is_run = false;
    tail_duration = 0;
    tail_batch = undefined;
    ev_animation_doned = new GgEvent();
    ev_stoped = new GgEvent();

    /// @func _create_animation(_target, _type, _duration, _data)
    /// @ignore
    static _create_animation = function(_target, _type, _duration, _data)
    {
        if _duration < 0
        {
            var _from = " [ scrAnimation GgAnimator._create_animation ] ";
            var _exc_data = gg_get_invalid_argument_data(2, _duration, "{ real } [ n >= 0 ]", from);

            throw new GgException(_exc_data, GG_EXCEPTION_TYPE.INVALID_ARGUMENT);
        }

        var _animation = {}

        animation.target = _target;
        animation.type = _type;
        animation.step = 1 / (game_get_speed(gamespeed_fps) * _duration);
        animation.params = _data;

        return animation;
    }

    /// @func append_animation(_target, _type, [_duration], [_data])
    /// @arg {Struct} _target
    /// @arg {Real} _type
    /// @arg {Real} _duration
    /// @arg {Struct} _data
    static append_animation = function(_target, _type, _duration = 1, _data = undefined)
    {
        var _animation = _create_animation(_target, _type, _duration, _data);
        var _batch = ds_list_create();

        ds_list_add(_batch, animation);
        ds_list_add(animations, _batch);
        size += 1;

        if ! is_undefined(_target)
        {
            tail_duration = _duration;
            tail_batch = _batch;
        }
    }

    /// @func join_animation(_target, _type, [_data])
    /// @arg {Struct} _target
    /// @arg {Real} _type
    /// @arg {Struct} _data
    static join_animation = function(_target, _type, _data = undefined)
    {
        var _animation = _create_animation(_target, _type, tail_duration, _data);

        ds_list_add(tail_batch, animation);
    }

    /// @func append_callback(, [_data])
    /// @arg {function} 
    /// @arg {Struct} _data
    static append_callback = function(, _data = undefined)
    {
        var _animation = _create_animation(, GG_ANIMATION_TYPE.CALLBACK, 0, _data);
        var _batch = ds_list_create();

        ds_list_add(_batch, animation);
        ds_list_add(animations, _batch);
        size += 1;
    }

    /// @func append_pause(_duration)
    /// @arg {Real} _duration
    static append_pause = function(_duration)
    {
        append_animation(undefined, GG_ANIMATION_TYPE.PAUSE, _duration);
    }

    /// @func update()
    static update = function()
    {
        if ! is_run
        {
            return;
        }

        if index == size
        {
            is_run = false;
            ev_stoped.notify();
            return;
        }

        var _step = 0;
        var _batch = animations[| index];

        for (var _i = 0; _i < ds_list_size(_batch); _i += 1)
        {
            var _animation = _batch[| _i];
            var _type = _animation.type;
            var _target = _animation.target;
            var _data = _animation.params;
            var _tag = _data.tag;

            _step = _animation.step;

            if _type == GG_ANIMATION_TYPE.SHIFT
            {
                var _offset = _data.offset;

               gg_translate_at(_target, _tag, _step * _offset.x, _step * _offset.y);
            }

            if _type == GG_ANIMATION_TYPE.rotate_by
            {
                var _dir = _data.direction;
                var _amount = _data.turn_sector_value * _step;
                var _axis = _data.rotate_by_axis;

                gg_rotate_by_along_origin(_target, _tag, _axis, _dir, _amount);
            }

            if _type == GG_ANIMATION_TYPE.SCALE
            {
                var _factor = _data.scale_factor;
                var _sx = lerp(1, _factor.x, param);
                var _sy = lerp(1, _factor.y, param);
                var _dx = _data.reverse_factor.x;
                var _dy = _data.reverse_factor.y;

                gg_scale(_target, _tag, _sx, _sy);
                gg_scale(_target, _tag, _dx, _dy);

                _data.reverse_factor.x = 1 / _sx;
                _data.reverse_factor.y = 1 / _sy;
            }

            if _type == GG_ANIMATION_TYPE.SKEW
            {
                var _shear = _data.shear;

                gg_skew(_target, _tag, _shear.x * _step, _shear.y * _step);
            }

            if _type == GG_ANIMATION_TYPE.CALLBACK
            {
                _target(_data);
            }
        }

        param += _step;

        if param >= 1
        {
            param = 0;
            index += 1;
            ev_animation_doned.notify();
        }
    }

    /// @func free()
    static free = function()
    {
        ev_animation_doned.free();
        ev_stoped.free();

        delete ev_animation_doned;
        delete ev_stoped;

        for (var _i = 0; _i < ds_list_size(animations); _i += 1)
        {
            var _batch = animations[| _i];

            for (var _j = 0; _j < ds_list_size(_batch); _j += 1)
            {
                delete _batch[| _j];
            }

            ds_list_destroy(_batch);
        }

        ds_list_destroy(animations);
    }
}
