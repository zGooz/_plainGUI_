
/// @func GgCommandInvoker()
///
/// @desc
///    This component raises events when certain key
///    combinations (commands) are pressed.
///
/// @context undefined
///
function GgCommandInvoker() constructor
{
    #region methods

    #region public

    /// @func monitor()
    ///
    /// @desc
    ///    Checks key combinations and
    ///    invokes the appropriate commands.
    ///
    /// @context GgCommandInvoker
    ///
    static monitor = function()
    {
        if keyboard_check(vk_alt)
        || keyboard_check(vk_escape)
        || keyboard_check(vk_printscreen)
        || keyboard_check(vk_pause)
        || gg_check_functional_keys_kc()
        || gg_check_other_keys_kc()
        {
            return;
        }
        
        var _shift_key_is_hold = keyboard_check(vk_shift);
        var _control_key_is_hold = keyboard_check(vk_control);

        if keyboard_check(vk_left)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_left.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_left.notify();
                return;
            }

            if _shift_key_is_hold
            {
                ev_shift_left.notify();
                return;
            }

            ev_left.notify();
            return;
        }

        if keyboard_check(vk_right)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_right.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_right.notify();
                return;
            }

            if _shift_key_is_hold
            {
                ev_shift_right.notify();
                return;
            }

            ev_right.notify();
            return;
        }

        if keyboard_check(vk_home)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_home.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_home.notify();
                return;
            }

            if _shift_key_is_hold
            {
                ev_shift_home.notify();
                return;
            }

            ev_home.notify();
            return;
        }

        if keyboard_check(vk_end)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_end.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_end.notify();
                return;
            }

            if _shift_key_is_hold
            {
                ev_shift_end.notify();
                return;
            }

            ev_end.notify();
            return;
        }

        if keyboard_check(vk_up)
        {
            if _shift_key_is_hold
            {
                ev_shift_up.notify();
                return;
            }

            ev_up.notify();
            return;
        }

        if keyboard_check(vk_down)
        {
            if _shift_key_is_hold
            {
                ev_shift_down.notify();
                return;
            }

            ev_down.notify();
            return;
        }

        if keyboard_check(vk_pageup)
        {
            if _shift_key_is_hold
            {
                ev_shift_page_up.notify();
                return;
            }

            ev_page_up.notify();
            return;
        }

        if keyboard_check(vk_pagedown)
        {
            if _shift_key_is_hold
            {
                ev_shift_page_down.notify();
                return;
            }

            ev_page_down.notify();
            return;
        }

        if keyboard_check(vk_insert)
        {
            if _shift_key_is_hold
            {
                ev_paste.notify();
                return;
            }

            ev_edit_mode_change.notify();
            return;
        }

        if keyboard_check(vk_backspace)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_backspace.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_backspace.notify();
                return;
            }

            ev_backspace.notify();
            return;
        }

        if keyboard_check(vk_delete)
        {
            if _control_key_is_hold
            && _shift_key_is_hold
            {
                ev_control_shift_delete.notify();
                return;
            }

            if _control_key_is_hold
            {
                ev_control_delete.notify();
                return;
            }

            ev_delete.notify();
            return;
        }

        if keyboard_check(vk_enter)
        || keyboard_check(vk_return)
        {
            ev_new_line.notify();
            return;
        }

        if keyboard_check(vk_tab)
        {
            ev_change_focus.notify();
            ev_tabulation.notify();
            return;
        }

        if _control_key_is_hold
        {
            if keyboard_check(ord("A"))
            {
                ev_select_all.notify();
                return;
            }

            if keyboard_check(ord("D"))
            {
                ev_dublicate_row.notify();
                return;
            }

            if keyboard_check(ord("X"))
            {
                ev_cut.notify();
                return;
            }

            if keyboard_check(ord("C"))
            {
                ev_copy.notify();
                return;
            }

            if keyboard_check(ord("V"))
            {
                ev_paste.notify();
                return;
            }

            if keyboard_check(ord("Z"))
            {
                if _shift_key_is_hold
                {
                    ev_redo.notify();
                    return;
                }

                ev_undo.notify();
                return;
            }

            if keyboard_check(ord("Y"))
            {
                ev_redo.notify();
                return;
            }
        }
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgCommandInvoker
    ///
    static free = function()
    {
        ev_left.free();
        delete ev_left;

        ev_right.free();
        delete ev_right;

        ev_shift_left.free();
        delete ev_shift_left;

        ev_shift_right.free();
        delete ev_shift_right;

        ev_control_left.free();
        delete ev_control_left;

        ev_control_right.free();
        delete ev_control_right;

        ev_control_shift_left.free();
        delete ev_control_shift_left;

        ev_control_shift_right.free();
        delete ev_control_shift_right;

        ev_up.free();
        delete ev_up;

        ev_down.free();
        delete ev_down;

        ev_shift_up.free();
        delete ev_shift_up;

        ev_shift_down.free();
        delete ev_shift_down;

        ev_home.free();
        delete ev_home;

        ev_end.free();
        delete ev_end;

        ev_control_home.free();
        delete ev_control_home;

        ev_control_end.free();
        delete ev_control_end;

        ev_shift_home.free();
        delete ev_shift_home;

        ev_shift_end.free();
        delete ev_shift_end;

        ev_control_shift_home.free();
        delete ev_control_shift_home;

        ev_control_shift_end.free();
        delete ev_control_shift_end;

        ev_page_up.free();
        delete ev_page_up;

        ev_page_down.free();
        delete ev_page_down;

        ev_shift_page_up.free();
        delete ev_shift_page_up;

        ev_shift_page_down.free();
        delete ev_shift_page_down;

        ev_backspace.free();
        delete ev_backspace;

        ev_delete.free();
        delete ev_delete;

        ev_control_backspace.free();
        delete ev_control_backspace;

        ev_control_delete.free();
        delete ev_control_delete;

        ev_control_shift_backspace.free();
        delete ev_control_shift_backspace;

        ev_control_shift_delete.free();
        delete ev_control_shift_delete;

        ev_tabulation.free();
        delete ev_tabulation;

        ev_new_line.free();
        delete ev_new_line;

        ev_edit_mode_change.free();
        delete ev_edit_mode_change;

        ev_select_all.free();
        delete ev_select_all;

        ev_cut.free();
        delete ev_cut;

        ev_copy.free();
        delete ev_copy;

        ev_paste.free();
        delete ev_paste;

        ev_undo.free();
        delete ev_undo;

        ev_redo.free();
        delete ev_redo;

        ev_dublicate_row.free();
        delete ev_dublicate_row;

        ev_change_focus.free();
        delete ev_change_focus;
    }

    #endregion public

    #endregion methods

    #region events

    ev_left = new GgEvent();
    ev_right = new GgEvent();
    ev_shift_left = new GgEvent();
    ev_shift_right = new GgEvent();
    ev_control_left = new GgEvent();
    ev_control_right = new GgEvent();
    ev_control_shift_left = new GgEvent();
    ev_control_shift_right = new GgEvent();

    ev_up = new GgEvent();
    ev_down = new GgEvent();
    ev_shift_up = new GgEvent();
    ev_shift_down = new GgEvent();

    ev_page_up = new GgEvent();
    ev_page_down = new GgEvent();
    ev_shift_page_up = new GgEvent();
    ev_shift_page_down = new GgEvent();

    ev_home = new GgEvent();
    ev_end = new GgEvent();
    ev_control_home = new GgEvent();
    ev_control_end = new GgEvent();
    ev_shift_home = new GgEvent();
    ev_shift_end = new GgEvent();
    ev_control_shift_home = new GgEvent();
    ev_control_shift_end = new GgEvent();

    ev_backspace = new GgEvent();
    ev_delete = new GgEvent();
    ev_control_backspace = new GgEvent();
    ev_control_delete = new GgEvent();
    ev_control_shift_backspace = new GgEvent();
    ev_control_shift_delete = new GgEvent();

    ev_tabulation = new GgEvent();
    ev_new_line = new GgEvent();
    ev_edit_mode_change = new GgEvent();
    ev_change_focus = new GgEvent();

    ev_select_all = new GgEvent();
    ev_cut = new GgEvent();
    ev_copy = new GgEvent();
    ev_paste = new GgEvent();
    ev_undo = new GgEvent();
    ev_redo = new GgEvent();
    ev_dublicate_row = new GgEvent();

    #endregion events
}
