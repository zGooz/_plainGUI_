
/// @func gg_screen_adaptation
///(_width, _height, [_win_mode], [_en_app_surf])
///
/// @desc
///    Changes the size of the game window to the size of the game window
///    windows, "application_surface" is also adjusted.
///    Also allows you to set the transition to full screen mode.
///
/// @arg {Real}_width
///    game window and surface width
///
/// @arg {Real}_height
///    game window and surface height
///
/// @arg {Real} _win_mode
///    windowed or full screen mode (enum GG_WINDOW_IS)
///
/// @arg {Bool} _en_app_surf
///    whether to use application surface
///
/// @context undefined
///
function gg_screen_adaptation
(_width = GG_DISPLAY_WIDTH,
_height = GG_DISPLAY_HEIGHT,
_win_mode = GG_WINDOW_IS.FULL_SCREEN,
_en_app_surf = false)
{
    #region verification

    if ! _width
    {
        // exception
    }

    if ! _height
    {
        // exception
    }

    #endregion verification

    _width = min(GG_DISPLAY_WIDTH, _width);
    _height = min(GG_DISPLAY_HEIGHT, _height);

    display_set_gui_size(_width, _height);
    application_surface_enable(_en_app_surf);
    application_surface_draw_enable(_en_app_surf);
    surface_resize(application_surface, _width, _height);
    window_set_size(_width, _height);
    window_set_fullscreen(_win_mode);

    /**
     *  NOTE:
     *  Position of the window to the center of the display here
     *  set manually, because "window_center()"
     *  works with a delay, as a result
     *  window centering is not happening
     *
     *  P.S. Information about the availability of the above
     *  delay derived from standard GM:S help
     *
     *  Quote
     *-----------------------------------------------------------------
     *  NOTE If you have resized the game window in the current
     *  step (for example, by switching from full screen to windowed
     *  or using window_set_size()),
     *  and wish to center the new window,
     *  this function should be called at least one step later
     *  (for example, by using an alarm)
     *  otherwise it will not work correctly.
     *-----------------------------------------------------------------
     */

    var _window_x = (GG_DISPLAY_WIDTH - _width) * 0.5;
    var _window_y = (GG_DISPLAY_HEIGHT - _height) * 0.5;

    window_set_position(_window_x, _window_y);
}
