
/// @func GgPosition([_x], [_y])
///
/// @desc
///    This component is required
///    for storing|position changes.
///    Primarily used by the "GgTransform" object.
///
/// @arg {Real} _x;
///    Position along the "x" axis.
///
/// @arg {Real} _y
///    Position along the "y" axis.
///
/// @context undefined
///
function GgPosition(_x = 0, _y = 0) constructor
{
    #region methods

    #region public

    /// @func set_position([_x], [_y])
    ///
    /// @desc
    ///    Sets the specified coordinates to the point.
    ///
    /// @arg {Real} _x;
    ///    Position along the "x" axis.
    ///
    /// @arg {Real} _y
    ///    Position along the "y" axis.
    ///
    /// @context GgPosition
    ///
    static set_position = function(_x, _y)
    {
        x = _x;
        y = _y;
    }

    /// @func reset_position()
    ///
    /// @desc
    ///    Sets the position to start.
    ///
    /// @context GgPosition
    ///
    static reset_position = function()
    {
        x = start_x;
        y = start_y;
    }

    /// @func translate_at(_shift_x, _shift_y)
    ///
    /// @desc
    ///    Offsets the point.
    ///
    /// @arg {Real} _shift_x
    ///    The amount of horizontal offset.
    ///
    /// @arg {Real} _shift_y
    ///    The amount of vertical offset.
    ///
    /// @context GgPosition
    ///
    static translate_at = function(_shift_x, _shift_y)
    {
        x += _shift_x;
        y += _shift_y;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    x = _x;
    y = _y;

    #endregion public

    #region private

    start_x = _x;
    start_y = _y;

    #endregion private

    #endregion variables

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgPosition
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);
        draw_circle(x, y, 5, false);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    #endregion debug
}
