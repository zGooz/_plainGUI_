
function GgTextCursor() constructor
{
    static set = function(_pos, _limit)
    {
        if _pos < 1
        {
            // exception
        }

        position = min(_pos, _limit + 1);
    }

    static to_left = function(_offset)
    {
        position = max(position - _offset, 1);
    }

    static to_right = function(_offset, _limit)
    {
        position = min(position + _offset, _limit + 1);
    }

    position = 1;
}
