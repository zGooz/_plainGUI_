
/// @func GgStoragMap()
///
/// @context Storage
///
function GgStoragMap(): GgSequenceStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = ds_map_create();

    #endregion private

    #endregion variables

    #region method

    #region public

    /// @func append(_item, _id)
    ///
    /// @desc
    ///    Adds an element to the store.
    ///
    /// @arg {Any} _item
    ///    Element to add.
    ///
    /// @arg {String} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgStoragMap
    ///
    static append = function(_item, _id)
    {
        if ds_map_exists(content, _id)
        {
            // exception
        }
        else
        {
            content[? _id] = _item;
        }
    }

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element from the storage.
    ///
    /// @arg {String} _id
    ///    ID of the element to remove.
    ///
    /// @context GgStoragMap
    ///
    static remove = function(_id)
    {
        if ds_map_exists(content, _id)
        {
            ds_map_delete(content, _id);
        }
        else
        {
            // exception
        }
    }

    /// @func change(_item, _id)
    ///
    /// @desc
    ///    Edits an element or replaces
    ///    one element with another.
    ///
    /// @arg {Any} _item
    ///    Element to edit.
    ///
    /// @arg {String} _id
    ///    Element ID, to edit|replace.
    ///
    /// @context GgStoragMap
    ///
    static change = function(_item, _id)
    {
        if ds_map_exists(content, _id)
        {
            content[? _id] = _item;
        }
        else
        {
            // exception
        }
    }

    /// @func get(_id)
    ///
    /// @desc
    ///    Returning an element from storage.
    ///
    /// @arg {String} _id
    ///    Element ID.
    ///
    /// @return {Any}
    ///
    /// @context GgStoragMap
    ///
    static get = function(_id)
    {
        var _item;

        try
        {
            _item = content[? _id];
        }
        catch (_error)
        {
            _item = undefined;
        }

        return _item;
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgStoragMap
    ///
    static count = function()
    {
        return ds_map_size(content);
    }

    /// @func foreach(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgStoragMap
    ///
    static foreach = function(_proc, _data = undefined)
    {
        var _id = ds_map_find_first(content);

        repeat count()
        {
            _proc(get(_id), _data);
            _id = ds_map_find_next(content, _id);
        }
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    element when clearing|deleting.
    ///
    /// @context GgStoragMap
    ///
    static clear = function(_proc)
    {
        foreach(_proc);
        ds_map_clear(content);
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgStoragMap
    ///
    static free = function()
    {
        ds_map_destroy(content);
        content = -1;
    }

    #endregion public

    #endregion methods
}
