
/// @func GgTextBlock(_transform)
///
/// @desc
///    Component for drawing text.
///
/// @arg {Struct.GgTransform} _transform
///    Transform, for text orientation.
///
/// @context undefined
///
function GgTextBlock(_transform) constructor
{
    #region methods

    #region public

    /// @func set_text(_text)
    ///
    /// @desc
    ///    Specifies the text to draw.
    ///
    /// @arg {String} _text
    ///    The text to draw.
    ///    WARNING: The value of the "format" structure
    ///    resets to adapt to text size.
    ///
    /// @context GgTextBlock
    ///
    static set_text = function(_text)
    {
        text = _text;
        set_font(font_family);
    }

    /// @func set_font(_font)
    ///
    /// @desc
    ///    Sets the font to change the appearance of.
    ///    WARNING: The value of the "format" structure
    ///    resets to adapt to text size.
    ///
    /// @arg {Asset.GmFont} _font
    ///    Font for decoration.
    ///
    /// @context GgTextBlock
    ///
    static set_font = function(_font)
    {
        font_family = _font;
        draw_set_font(font_family);

        var _rows = string_split_ext (
            text, [GG_CRLF, GG_CR, GG_LF]
        );

        var _row = _rows[0];

        format.separation = string_height(_row);
        format.string_width = string_width(text);
    }

    /// @func draw()
    ///
    /// @desc
    ///    Draws text.
    ///
    /// @context GgTextBlock
    ///
    static draw = function()
    {
        if text == ""
        {
            return;
        }

        var _x = transform.position.x;
        var _y = transform.position.y;
        var _angle = transform.rotation;
        var _factor_x = transform.scale.factor_x;
        var _factor_y = transform.scale.factor_y;
        var _color = brush.color;
        var _alpha = brush.alpha;

        var _separation = format.separation;
        var _width = format.string_width;
        var _hor_align = format.horizontal_alignment;
        var _ver_align = format.vertical_alignment;

        var _hor_aling_old = draw_get_halign();
        var _ver_aling_old = draw_get_valign();
        var _font_old = draw_get_font();

        draw_set_halign(_hor_align);
        draw_set_valign(_ver_align);
        draw_set_font(font_family);

        draw_text_ext_transformed_color (
            _x, _y, text, _separation, _width,
            _factor_x, _factor_y, _angle,
            _color, _color, _color, _color, _alpha
        );

        draw_set_halign(_hor_aling_old);
        draw_set_valign(_ver_aling_old);
        draw_set_font(_font_old);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgTextBlock
    ///
    static free = function()
    {
        delete brush;
        delete format;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    brush = new GgBrush();
    transform = _transform;

    format = {
        separation: 0,
        string_width: 0,
        horizontal_alignment: fa_left,
        vertical_alignment: fa_top,
    };

    #endregion public

    #region private

    /// @ignore
    text = "";

    /// @ignore
    font_family = gg_def_fonts.mono_family;

    #endregion private

    #endregion variables
}
