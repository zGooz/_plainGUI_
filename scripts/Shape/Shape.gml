
/// @func GgShape()
///
/// @desc
///    Parent component ownership
///    handlers mouse coordinates
///    for geometric shapes.
///
/// @context undefined
///
function GgShape() constructor
{
    /// @func check()
    ///
    /// @desc
    ///    Function to check if whether there was a
    ///    mouse click within the given boundaries.
    ///
    /// @return {Bool}
    ///
    /// @context GgShape
    ///
    static check = function() {}
}
