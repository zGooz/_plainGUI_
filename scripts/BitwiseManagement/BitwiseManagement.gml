
/// @func gg_is_set_flag(_mask, _bit)
///
/// @desc
///    Checks if the bit in the given position is set.
///
/// @arg {Real}_mask
///    Bitmask.
///
/// @arg {Real} _bit
///    Flag position, for checking.
///
/// @context undefined
///
function gg_is_set_flag(_mask, _bit)
{
    return (_mask & _bit) != 0;
}

/// @func gg_set_flag(_mask, _bit)
///
/// @desc
///    Sets the bit at the given position.
///
/// @arg {Real}_mask
///    Bitmask.
///
/// @arg {Real} _bit
///    Flag position to set.
///
/// @context undefined
///
function gg_set_flag(_mask, _bit)
{
    return _mask | _bit;
}

/// @func gg_inverse_flag(_mask, _bit)
///
/// @desc
///    Inverts the bit at the given position.
///
/// @arg {Real}_mask
///    Bitmask.
///
/// @arg {Real} _bit
///    Flag position to invert.
///
/// @context undefined
///
function gg_inverse_flag(_mask, _bit)
{
    return _mask ^ _bit;
}

/// @func gg_reset_flag(_mask, _bit)
///
/// @desc
///    Resets the bit at the given position.
///
/// @arg {Real}_mask
///    Bitmask.
///
/// @arg {Real} _bit
///    Flag position to reset.
///
/// @context undefined
///
function gg_reset_flag(_mask, _bit)
{
    return _mask & ~_bit;
}
