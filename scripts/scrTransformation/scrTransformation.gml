/*
/// @func gg_translate_at(_object, _object_type, _x, _y)
/// @arg {Struct.GgShape, struct.GgPrimitiveRenderer} _object
/// @arg {Real} _object_type
/// @arg {Real} _x
/// @arg {Real} _y
function gg_translate_at(_object, _object_type, _x, _y)
{
    if _object_type == GG_TRANSFORMED_TARGET.SHAPE
    {
        path_shift(_object.points, _x, _y);
    }

    if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
    {
        for (var _i = 0; _i < _object.size; _i += 1)
        {
            var _data = _object.vertexes;
            var _vertex = _data[| _i];

            _vertex[@ GG_PRIMITIVE_PARAM.X] += _x;
            _vertex[@ GG_PRIMITIVE_PARAM.Y] += _y;
        }
    }
}

/// @func gg_rotate_by_along_origin(_object, _object_type, _axis, _dir, _value)
/// @arg {Struct.GgShape, struct.GgPrimitiveRenderer} _object
/// @arg {Real} _object_type
/// @arg {Struct.GgPoint} _axis
/// @arg {Real} _dir
/// @arg {Real} _value
function gg_rotate_by_along_origin(_object, _object_type, _axis, 
_dir = GG_ROTATION_DIRECTION.COUNTER_CLOCKWISE, _value = 1)
{
    var _cos_val = cos(_value);
    var _sin_val = sin(_value);
    var _data;

    if _object_type == GG_TRANSFORMED_TARGET.SHAPE
    {
        _data = _object.points;
    }

    if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
    {
        _data = _object.vertexes;
    }

    for (var _i = 0; _i < _object.size; _i += 1)
    {
        var _px = 0, _py = 0, _vertex = [];

        if _object_type == GG_TRANSFORMED_TARGET.SHAPE
        {
            _data = _object.points;
            _px = path_get_point_x(_data, _i);
            _py = path_get_point_y(_data, _i);
        }

        if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
        {
            _data = _object.vertexes;
            _vertex = _data[| _i];
            _px = _vertex[@ GG_PRIMITIVE_PARAM.X];
            _py = _vertex[@ GG_PRIMITIVE_PARAM.Y];
        }

        var _sub_x = _px - _axis.x;
        var _sub_y = _py - _axis.y;
        var _nx = _sub_x * _cos_val + _sub_y * -_dir * _sin_val;
        var _ny = _sub_x * _sin_val * _dir + _sub_y * _cos_val;

        if _object_type == GG_TRANSFORMED_TARGET.SHAPE
        {
            path_change_point(_data, _i, _axis.x + _nx, _axis.y + _ny, 0);
        }

        if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
        {
            _vertex[@ GG_PRIMITIVE_PARAM.X] = _axis.x + _nx;
            _vertex[@ GG_PRIMITIVE_PARAM.Y] = _axis.y + _ny;
        }
    }
}

/// @func gg_scale(_object, _object_type, _x, _y)
/// @arg {Struct.GgShape, struct.GgPrimitiveRenderer} _object
/// @arg {Real} _object_type
/// @arg {Real} _x
/// @arg {Real} _y
function gg_scale(_object, _object_type, _x, _y)
{
    if _object_type == GG_TRANSFORMED_TARGET.SHAPE
    {
        path_rescale_at(_object.points, _x, _y);
    }

    if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
    {
        var _data = _object.vertexes;
        var _ox = _object.origin.x;
        var _oy = _object.origin.y;

        for (var _i = 0; _i < _object.size; _i += 1)
        {
            var _vertex = _data[| _i];
            var _px = _vertex[@ GG_PRIMITIVE_PARAM.X];
            var _py = _vertex[@ GG_PRIMITIVE_PARAM.Y];

            _vertex[@ GG_PRIMITIVE_PARAM.X] = _ox + (_px - _ox) * _x;
            _vertex[@ GG_PRIMITIVE_PARAM.Y] = _oy + (_py - _oy) * _y;
        }
    }
}

/// @func gg_skew(_object, _object_type, _x, _y)
/// @arg {Struct.GgShape, struct.GgPrimitiveRenderer} _object
/// @arg {Real} _object_type
/// @arg {Real} _x
/// @arg {Real} _y
function gg_skew(_object, _object_type, _x, _y)
{
    var _tan_val_x = tan(-_x);
    var _tan_val_y = tan(-_y);
    var _ox = _object.origin.x;
    var _oy = _object.origin.y;
    var _data;

    if _object_type == GG_TRANSFORMED_TARGET.SHAPE
    {
        _data = _object.points;
    }

    if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
    {
        _data = _object.vertexes;
    }

    for (var _i = 0; _i < _object.size; _i += 1)
    {
        var _px = 0, _py = 0, _vertex = [];

        if _object_type == GG_TRANSFORMED_TARGET.SHAPE
        {
            _data = _object.points;
            _px = path_get_point_x(_data, _i);
            _py = path_get_point_y(_data, _i);
        }

        if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
        {
            _data = _object.vertexes;
            _vertex = _data[| _i];
            _px = _vertex[@ GG_PRIMITIVE_PARAM.X];
            _py = _vertex[@ GG_PRIMITIVE_PARAM.Y];
        }

        var _sx = _px - _ox;
        var _sy = _py - _oy;
        var _nx = _ox + _sx + _sy * _tan_val_x;
        var _ny = _oy + _sx * _tan_val_y + _sy;

        if _object_type == GG_TRANSFORMED_TARGET.SHAPE
        {
            path_change_point(_data, _i, _nx, _ny, 0);
        }

        if _object_type == GG_TRANSFORMED_TARGET.PRIMITIVE
        {
            _vertex[@ GG_PRIMITIVE_PARAM.X] = _nx;
            _vertex[@ GG_PRIMITIVE_PARAM.Y] = _ny;
        }
    }
}
