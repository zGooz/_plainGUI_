
/// @func GgPolygon(_source)
///
/// @desc
///    Component to check
///    mouse click on the polygon.
///
/// @arg {Asset.GmPath} _source
///    Resource for polygon formation.
///
/// @context GgShape
///
function GgPolygon(_source): GgShape() constructor
{
    #region variables

    #region public

    offset = new GgPosition();

    #endregion public

    #region private

    /// @ignore
    source = _source;

    /// @ignore
    corner = gg_path_top_left_corner_pos(source);

    /// @ignore
    distances = gg_dist_to_top_left_corner(source, corner); 

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func check()
    ///
    /// @desc
    ///    Function to check if the
    ///    mouse click in the given borders.
    ///
    /// @return {Bool}
    ///
    /// @context GgPolygon
    ///
    static check = function()
    {
        return gg_point_in_polygon(offset, corner, distances);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgPolygon
    ///
    static free = function()
    {
        delete offset;
        delete corner;

        distances = -1;
    }

    #endregion public

    #endregion methods

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgPolygon
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _previouse_alpha = draw_get_alpha();

        draw_set_color(_color);
        draw_set_alpha(_alpha);

        draw_path(source, 0, 0, true);

        draw_set_color(_color_old);
        draw_set_alpha(_previouse_alpha);
    }

    #endregion debug
}
