
/// @func GgEvent()
///
/// @desc
///    This component is required in order to
///    to notify others about some changes or actions.
///    To receive a notification for this
///    component needs to be subscribed.
///
/// @context undefined
///
function GgEvent() constructor
{
    #region methods

    #region public

    /// @func subscribe(_name, _procedure)
    ///
    /// @desc
    ///    Method that allows you to become a
    ///    subscriber of this event.
    ///
    /// @arg {String} _name
    ///    The name of the subscriber, it will be easier
    ///    to unsubscribe by name when needed.
    ///
    /// @arg {function} _method
    ///    (Method|function|procedure) owner (object|struct),
    ///    what will fire when the event is called.
    ///
    /// @context GgEvent
    ///
    static subscribe = function(_name, _method)
    {
        storage.append(_method, _name);
    }

    /// @func unsubscribe(_name)
    ///
    /// @desc
    ///    Method to unsubscribe.
    ///
    /// @arg {String} _name
    ///    The name of the subscriber to unsubscribe.
    ///
    /// @context GgEvent
    ///
    static unsubscribe = function(_name)
    {
        storage.remove(_name);
    }

    /// @func is_follower(_name)
    ///
    /// @desc
    ///    Method to check if there is
    ///    a subscriberwith given name.
    ///
    /// @arg {String} _name
    ///    Name of the potential subscriber to check.
    ///
    /// @return {Bool}
    ///
    /// @context GgEvent
    ///
    static is_follower = function(_name)
    {
        return ! is_undefined(storage.get(_name));
    }

    /// @func notify([_data])
    ///
    /// @desc
    ///    Method allowing to notify subscribers.
    ///    When alerting, transmission is possible
    ///    auxiliary data storing for example some changes.
    ///
    /// @arg {Any} _data
    ///    auxiliary data that is sent when alerted.
    ///
    /// @return {Bool}
    ///
    /// @context GgEvent
    ///
    static notify = function(_data = undefined)
    {
        storage.foreach(notify_follower, _data);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory,
    ///    applies to each element.
    ///
    /// @context GgEvent
    ///
    static free = function()
    {
        storage.free();
        delete storage;
    }

    #endregion public

    #region private

    /// @func notify_follower(_method, _data)
    ///
    /// @desc
    ///    Notifies a specific subscriber.
    ///    By calling the method that the subscriber
    ///    provided when subscribing.
    ///    Also, when notification is passed as an argument
    ///    auxiliary data (if available).
    ///
    /// @arg {Any} _method
    ///    Subscriber.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data.
    ///
    /// @context GgEvent
    ///
    /// @ignore
    ///
    static notify_follower = function(_method, _data)
    {
        method_call(_method, [_data]);
    }

    #endregion private

    #endregion methods

    #region variables

    #region private

    /// @ignore
    storage = new GgSequenceStorageStruct();

    #endregion private

    #endregion variables
}
