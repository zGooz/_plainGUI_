
/// @func GgPin()
///
/// @desc
///    A component that is used for space
///    curvature, i.e. it is possible to
///    change the location of the transforms.
///
/// @context undefined
///
function GgPin() constructor
{
    #region methods

    #region public

    /// @func append(_transform, [_id])
    ///
    /// @desc
    ///    Adds an transform to the pin.
    ///
    /// @arg {Struct.GgTransform} _transform
    ///    Transform to add.
    ///
    /// @arg {Real} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgPin
    ///
    static append = function(_transform, _id = -1)
    {
        var _ox = axis.from.x;
        var _oy = axis.from.y;
        var _tx = _transform.position.x;
        var _ty = _transform.position.y;
        var _direction = point_direction(_ox, _oy, _tx, _ty);
        var _length = point_distance(_ox, _oy, _tx, _ty);

        var _item = {
            transform: _transform,
            length: _length,
            direction: _direction,
        };

        storage.append(_item, _id);
    }

    /// @func translate_at(_shift_x, _shift_y, [_with_me])
    ///
    /// @desc
    ///    Offsets the pin.
    ///
    /// @arg {Real} _shift_x
    ///    The amount of horizontal offset.
    ///
    /// @arg {Real} _shift_y
    ///    The amount of vertical offset.
    ///
    /// @context GgPin
    ///
    static translate_at = function(_shift_x, _shift_y)
    {
        axis.translate_at(_shift_x, _shift_y);
    }

    /// @func rotate_at(_angle)
    ///
    /// @desc
    ///    Rotate the pin.
    ///
    /// @arg {Real} _angle
    ///    Angle of rotation.
    ///
    /// @context GgPin
    ///
    static rotate_at = function(_angle)
    {
        axis.rotate_at(_angle);
    }

    /// @func set([_x], [_y], [_dir])
    ///
    /// @desc
    ///    Specifies the position of the pin
    ///    (its start and end points) directly.
    ///
    /// @arg {Real} _x;
    ///    Position along the "x" axis for origin.
    ///
    /// @arg {Real} _y
    ///    Position along the "y" axis for origin.
    ///
    /// @arg {Real} _dir
    ///    Direction of axis.
    ///
    /// @context GgVector2
    ///
    static set = function(_x = 0, _y = 0, _dir = 0)
    {
        axis.set(_x, _y, 1, _dir);
    }

    /// @func build()
    ///
    /// @desc
    ///    Сhanges the location of the transform.
    ///
    /// @context GgPin
    ///
    static build = function()
    {
        storage.foreach(change_transform_position, axis);
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgPin
    ///
    static free = function()
    {
        storage.clear(gg_free_item);
        storage.free();
        delete storage;

        axis.free();
        delete axis;
    }

    #endregion public

    #region private

    /// @func change_transform_position(_item, _axis)
    ///
    /// @desc
    ///    Сhanges the location of the transform.
    ///
    /// @arg {Struct} _item
    ///    Basic data to convert.
    ///    Contains a transform, and a position
    ///    relative to the origin.
    ///
    /// @arg {Struct.GgVector2} _axis
    ///    Axis of rotation. 
    ///
    /// @context GgPin
    ///
    /// @ignore
    ///
    static change_transform_position = function(_item, _axis)
    {
        var _transform = _item.transform;
        var _axis_rotation = _axis.get_direction();

        _transform.set_rotation(_axis_rotation);

        var _ox = _axis.from.x;
        var _oy = _axis.from.y;
        var _rotation = _axis_rotation + _item.direction;
        var _lx = lengthdir_x(_item.length, _rotation);
        var _ly = lengthdir_y(_item.length, _rotation);

        _transform.set_position(_ox + _lx, _oy + _ly);
        _transform.build();
    }

    #endregion private

    #endregion methods

    #region variables

    #region private

    /// @ignore
    storage = new GgSequenceStorageArray();

    /// @ignore
    axis = new GgVector2();

    #endregion private

    #endregion variables

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgPin
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _brush = new GgBrush(_color, _alpha);
        storage.foreach(draw_transforms, _brush);
        delete _brush;
        axis.draw_gizmos(_color, _alpha);
    }

    /// @func draw_transforms(_transform, _brush)
    ///
    /// @desc
    ///    Draws all transforms and axis.
    ///
    /// @arg {Struct.GgTransform} _transform
    ///    Transform for rendering.
    ///
    /// @arg {Struct.GgBrush} _brush
    ///    Brush for coloring the transform.
    ///
    /// @context GgPin
    ///
    /// @ignore
    ///
    static draw_transforms = function(_transform, _brush)
    {
        _transform.draw_gizmos(_brush.color, _brush.alpha);
    }

    #endregion debug
}
