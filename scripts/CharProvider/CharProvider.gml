
/// @func GgCharsProvider()
///
/// @desc
///    This component is required for
///    providing characters as a result
///    print. Some characters are displayed
///    with appropriate settings.
///
/// @context undefined
///
function GgCharsProvider() constructor
{
    #region methods

    #region public

    /// @func provide()
    ///
    /// @desc
    ///    Return a character by checking the
    ///    keyboard state needed for printing.
    ///
    /// @return {String}
    ///
    /// @context GgCharsProvider
    ///
    static provide = function()
    {
        if keyboard_check(vk_alt)
        || keyboard_check(vk_control)
        || keyboard_check(vk_escape)
        || keyboard_check(vk_home)
        || keyboard_check(vk_end)
        || keyboard_check(vk_pageup)
        || keyboard_check(vk_pagedown)
        || keyboard_check(vk_printscreen)
        || keyboard_check(vk_pause)
        || keyboard_check(vk_backspace)
        || keyboard_check(vk_delete)
        || keyboard_check(vk_insert)
        || gg_check_other_keys_kc()
        || gg_check_functional_keys_kc()
        || gg_check_arrows_keys_kc()
        {
            keyboard_lastchar = "";
            return "";
        }

        if keyboard_check(vk_shift)
        {
            if keyboard_lastkey == vk_shift
            || keyboard_lastkey == vk_lshift
            || keyboard_lastkey == vk_rshift
            {
                return "";
            }
            else
            {
                if numbers_only
                {
                    keyboard_lastchar = "";
                    return "";
                }

                //  !, @, #, ...
                var _char = keyboard_lastchar;
                keyboard_lastchar = "";

                return _char;
            }
        }

        if keyboard_key == vk_nokey
        {
            keyboard_lastchar = "";
            return "";
        }

        if keyboard_key == vk_enter
        || keyboard_key == vk_return
        {
            keyboard_lastchar = "";

            if give_new_row_symbol
            {
                return new_row_symbol;
            }

            return "";
        }

        if keyboard_key == vk_tab
        {
            keyboard_lastchar = "";

            if give_tab_symbol
            {
                return "\t";
            }

            return string_repeat(" ", tab_indent);
        }

        if keyboard_key == vk_backspace
        {
            if give_backspace_symbol
            {
                return "\b";
            }

            return "";
        }

        if numbers_only
        {
            if ! gg_check_number_kk()
            {
                keyboard_lastchar = "";
                return "";
            }
        }

        var _char = keyboard_lastchar;

        if gg_check_non_printable_characters(_char)
        {
            keyboard_lastchar = "";
            return "";
        }

        return _char;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    numbers_only = false;
    give_tab_symbol = true;
    give_new_row_symbol = true;
    give_backspace_symbol = false;
    new_row_symbol = GG_LF;
    tab_indent = GG_TABULATION_INDENTATION;

    #endregion public

    #endregion variables
}
