
/// @func gg_get_def_point(_number, _data)
///
/// @desc
///    Returns the default position. The number
///    is used to calculate the position.
///
/// @arg {Real} _number
///    A number that affects position settings.
///
/// @arg {Struct} _data
///    Auxiliary data.
///
/// @context undefined
///
function gg_get_def_point(_number, _data)
{
    return new GgPosition(0, 0);
}
