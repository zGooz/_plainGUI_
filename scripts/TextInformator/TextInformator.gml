
function GgInfoAbotText() constructor
{
    static check_left_symbol = function(_text, _symbol, _pos)
    {
        var _char = string_char_at(_text, _pos - 1);
        return _char == _symbol;
    }

    static check_symbol = function(_text, _symbol, _pos)
    {
        var _char = string_char_at(_text, _pos);
        return _char == _symbol;
    }

    static is_left_pair_sym_space = function(_text, _pos, _crlf)
    {
        var _nearest = string_char_at(_text, _pos - 1);
        var _furthest = string_char_at(_text, _pos - 2);

        return _nearest != _crlf
            && _nearest != " "
            && _furthest == " ";
    }

    static is_right_pair_space_sym = function(_text, _pos, _crlf)
    {
        var _nearest = string_char_at(_text, _pos);
        var _furthest = string_char_at(_text, _pos + 1);

        return _furthest != _crlf
            && _furthest != " "
            && _nearest == " ";
    }

    static dist_to_left_word_or_row = function(_editor, _text, _symbol)
    {
        var _pos = _editor.get_support_pos();
        var _static_pos = _pos;

        while check_left_symbol(_text, _symbol, _pos)
        {
            if _pos == 1
            {
                break;
            }

            _pos -= 1;
        }

        _pos = string_last_pos_ext(_symbol, _text, _pos - 1);

        return max(0, _static_pos - _pos - 1);
    }

    static dist_to_right_word_or_row =
    function(_editor, _text, _symbol, _length)
    {
        var _pos = _editor.get_support_pos();
        var _static_pos = _pos;

        if ! check_symbol(_text, _symbol, _pos + 1)
        {
            _pos = string_pos_ext(_symbol, _text, _pos + 1);

            if _pos == 0
            {
                _pos = _length + 1;
            }
        }

        do
        {
            if _pos > _length
            {
                _pos = _length + 1;
                break;
            }

            _pos += 1;
        }
        until ! check_symbol(_text, _symbol, _pos);
        
        return max(0, _pos - _static_pos);
    }

    static dist_to_left_free_place = function(_editor, _text, _crlf)
    {
        var _pos = _editor.get_support_pos();

        if check_left_symbol(_text, _crlf, _pos)
        {
            return 1;
        }

        if is_left_pair_sym_space(_text, _pos, _crlf)
        {
            return 1;
        }

        var _dist_to_row_end = dist_to_left_word_or_row (
            _editor, _text, _crlf
        );

        var _dist_to_space = dist_to_left_word_or_row (
            _editor, _text, " "
        );

        return min(_dist_to_space, _dist_to_row_end);
    }

    static dist_to_right_free_place =
    function(_editor, _text, _crlf, _length)
    {
        var _pos = _editor.get_support_pos();

        if check_symbol(_text, _crlf, _pos)
        {
            return 1;
        }

        if is_right_pair_space_sym(_text, _pos, _crlf)
        {
            return 1;
        }

        var _dist_to_row_end = dist_to_right_word_or_row (
            _editor, _text, _crlf, _length
        );

        var _dist_to_space = dist_to_right_word_or_row (
            _editor, _text, " ", _length
        );

        var _dist = min(_dist_to_space, _dist_to_row_end);

        if _dist == _dist_to_row_end
        {
            if _pos + _dist != _length + 1
            {
                _dist -= 1;
            }
        }

        return _dist;
    }

    static dist_to_up_free_place =
    function(_fragment, _cursor, _crlf)
    {
        var _rows = string_split(_fragment, _crlf);
        var _count = array_length(_rows);
        var _temp_pos = _cursor;

        while check_symbol(_fragment, _crlf, _temp_pos)
        {
            array_delete(_rows, _count - 1, 1);

            _count -= 1;
            _temp_pos -= 1;

            if _temp_pos == 1
            {
                break;
            }
        }

        switch _count
        {
            case 0:
            case 1:
            {
                return 0;
            }

            default:
            {
                var _current = _rows[_count - 1];
                var _previous = _rows[_count - 2];
                var _prev_len = string_length(_previous);
                var _prev_pos = string_last_pos(_previous, _fragment);
                var _cur_pos = string_last_pos(_current, _fragment);
                var _offset = min(_cursor - _cur_pos, _prev_len);
                var _new_pos = _prev_pos + _offset;

                return _cursor - _new_pos;
            }
        }
    }

    static dist_to_down_free_place =
    function(_fragment, _cursor, _crlf, _offset, _length)
    {
        var _rows = string_split(_fragment, _crlf);
        var _count = array_length(_rows);
        var _temp_pos = _cursor;

        while check_symbol(_fragment, _crlf, _temp_pos)
        {
            array_delete(_rows, 0, 1);

            _count -= 1;
            _temp_pos += 1;

            if _temp_pos == _length + 1
            {
                break;
            }
        }

        switch _count
        {
            case 0:
            case 1:
            {
                return 0;
            }

            default:
            {
                var _current = _rows[0];
                var _next = _rows[1];
                var _cur_len = string_length(_current);
                var _next_len = string_length(_next);

                if _count == 2 && _offset > _next_len
                {
                    _cur_len += 1;
                }

                _offset = min(_offset, _next_len);

                return _cur_len + _offset + 1;
            }
        }
    }
}
