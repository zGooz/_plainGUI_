
/// @func GgLinkedList()
///
/// @context GgSequenceStorage
///
function GgLinkedList(): GgSequenceStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = ds_linked_list_create();

    #endregion private

    #endregion variable

    #region methods

    #region public

    /// @func append(_value)
    ///
    /// @desc
    ///    Adds an element to the store.
    ///
    /// @arg {Any} _value
    ///    Element to add.
    ///
    /// @context GgLinkedList
    ///
    static append = function(_value)
    {
        ds_linked_list_add(content, _value);
    }

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element from the storage.
    ///
    /// @arg {Real} _id
    ///    ID of the element to remove.
    ///
    /// @context GgLinkedList
    ///
    static remove = function(_id = -1)
    {
        try
        {
            ds_linked_list_destroy(_id);
        }
        catch (_error)
        {
            // exception
        }
    }

    /// @func change(_value, _id)
    ///
    /// @desc
    ///    Replace the value at the given
    ///    position for another one.
    ///
    /// @arg {Any} _value
    ///    Element to edit.
    ///
    /// @arg {Real} _id
    ///    Element ID, to edit|replace.
    ///
    /// @context GgLinkedList
    ///
    static change = function(_value, _id = -1)
    {
        try
        {
            ds_linked_list_replace(content, _id, _value);
        }
        catch (_error)
        {
            // exception
        }
    }

    /// @func get(_id)
    ///
    /// @desc
    ///    Returning an element from storage.
    ///
    /// @arg {Real} _id
    ///    Element ID.
    ///
    /// @return {Struct.GgDsLinkedListItem}
    ///
    /// @context GgLinkedList
    ///
    static get = function(_id = -1)
    {
        var _item = undefined;

        try
        {
            _item = ds_linked_list_find_value(content, _id);
        }
        catch (_error)
        {
            _item = undefined;
        }

        return _item;
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgLinkedList
    ///
    static count = function()
    {
        return ds_linked_list_size(content);
    }

    /// @func foreach(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgLinkedList
    ///
    static foreach = function(_proc, _data = undefined)
    {
        var _item = content.get(0);

        repeat count()
        {
            _proc(_item.value, _data);
            _item = _item.next;
        }
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    element when clearing|deleting.
    ///
    /// @context GgSequenceStorageList
    ///
    static clear = function(_proc)
    {
        foreach(_proc);
        ds_linked_list_clear(content);
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgSequenceStorageList
    ///
    static free = function()
    {
        ds_linked_list_destroy(content);
        content = -1;
    }

    #endregion public

    #region private

    #endregion private

    #endregion methods
}
