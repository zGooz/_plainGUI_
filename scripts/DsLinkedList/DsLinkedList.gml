
/// @func GgDsLinkedList([_is_closed])
///
/// @desc
///    Data structure representing
///    from itself a linked list
///
/// @arg {Bool} _is_closed
///    Whether the list is circular.
///
/// @context undefined
///
function GgDsLinkedList(_is_closed = false) constructor
{
    #region variables

    #region private

    /// @ignore
    head = undefined;

    /// @ignore
    tail = undefined;

    /// @ignore
    is_closed = _is_closed;

    /// @ignore
    count = 0;

    /// @ignore
    cursor = undefined;

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func add(_value)
    ///
    /// @desc
    ///    Used to add a new value to the linked list
    ///    which will added on the end
    ///
    /// @arg {Any} _value
    ///
    /// @context GgDsLinkedList
    ///
    static add = function(_value)
    {
        var _item = new GgDsLinkedListItem(_value);

        if ! count
        {
            head = _item;
            tail = _item;
            cursor = _item;

            if is_closed
            {
                head.previous = head;
                head.next = tail;
            }
        }
        else
        {
            tail.next = _item;
            _item.previous = tail;
            tail = _item;

            if is_closed
            {
                _item.next = head;
                head.previous = _item;
            }
        }

        count += 1;
    }

    /// @func insert(_index, _value)
    ///
    /// @desc
    ///    Add the given value into the
    ///    linked list at the given position.
    ///
    /// @arg {Real} _index
    ///    The position to add the value.
    ///
    /// @arg {Any} _value
    ///    The value to add to the linked list.
    ///
    /// @context GgDsLinkedList
    ///
    static insert = function(_index, _value)
    {
        if _index < 0 || _index > count
        {
            // exception
        }

        if _index == count
        {
            add(_value);
        }
        else
        {
            var _item = new GgDsLinkedListItem(_value);
            var _desired = get(_index);
            var _previous = _desired.previous;

            if _index != 0
            {
                _previous.next = _item;
            }
            else
            {
                head = _item;
                cursor = _item;

                if is_closed
                {
                    tail.next = head;
                    head.previous = tail;
                }
            }

            _item.previous = _previous;
            _desired.previous = _item;
            _item.next = _desired;
            count += 1;
        }
    }

    /// @func remove(_index)
    ///
    /// @desc
    ///    Remove the value stored at a specific
    ///    position within the linked list.
    ///
    /// @arg {Real} _index
    ///    Where in the linked list to delete the value.
    ///
    /// @context GgDsLinkedList
    ///
    static remove = function(_index)
    {
        if ! count
        {
            // exception
        }

        if _index < 0 || _index > count - 1
        {
            // exception
        }

        var _desired = get(_index);
        var _previous = _desired.previous;
        var _next = _desired.next;

        if _desired == tail
        {
            tail = _previous;
        }

        if _desired == head
        {
            head = _next;
        }

        _previous.next = next;
        _next.previous = _previous;

        if _desired == cursor
        {
            cursor = head;
        }

        delete _desired;
        count -= 1;
    }

    /// @func replace(_index, _value)
    ///
    /// @desc
    ///    Replace the value at the given
    ///    position for another one.
    ///
    /// @arg {Real} _index
    ///    The position to replace the value.
    ///
    /// @arg {Any} _value
    ///    The new value to replace the given value with.
    ///
    /// @context GgDsLinkedList
    ///
    static replace = function(_index, _value)
    {
        if ! count
        {
            // exception
        }

        if _index < 0 || _index > count - 1
        {
            // exception
        }

        var _desired = get(_index);
        _desired.value = _value;
    }

    /// @func get(_index)
    ///
    /// @desc
    ///    Check the given linked list position
    ///    and the value held within the linked
    ///    list for that position will be returned.
    ///
    /// @arg {Real} _index
    ///    The position to lool at.
    ///
    /// @return {Struct.GgDsLinkedListItem}
    ///
    /// @context GgDsLinkedList
    ///
    static get = function(_index)
    {
        if ! count
        {
            return undefined;
        }

        var _item = undefined;

        try
        {
            var _half = count / 2;

            if _index <= _half
            {
                _item = head;

                repeat _index
                {
                    _item = _item.next;
                }
            }
            else
            {
                _item = tail;

                repeat count - _index - 1
                {
                    _item = _item.previous;
                }
            }
        }
        catch (_error)
        {
            _item = undefined;
        }

        return _item;
    }

    /// @func find(_value)
    ///
    /// @desc
    ///    Check the given linked list for a value
    ///    and the position within the linked list
    ///    for that value will be returned.
    ///
    /// @arg {Any} _value
    ///    The value to find.
    ///
    /// @return {Real}
    ///
    static find = function(_value)
    {
        var _item = head;
        var _index = 0;

        while true
        {
            if is_undefined(_item)
            {
                break;
            }

            if _item.value == value
            {
                break;
            }

            _index += 1;
            _item = _item.next;
        }

        if ! is_undefined(_item)
        {
            return _index;
        }

        return -1;
    }

    /// @func next()
    ///
    /// @desc
    ///    Return next element from storage.
    ///
    /// @context GgDsLinkedList
    ///
    static next = function()
    {
        if is_undefined(cursor)
        {
            cursor = head;
        }
        else
        {
            cursor = cursor.next;
        }
    }

    /// @func previous()
    ///
    /// @desc
    ///    Return previous element from storage.
    ///
    /// @context GgDsLinkedList
    ///
    static previous = function()
    {
        if is_undefined(cursor)
        {
            cursor = head;
        }
        else
        {
            cursor = cursor.previous;
        }
    }

    /// @func get_cursor()
    ///
    /// @desc
    ///    Возвращает специальный указатель.
    ///
    /// @return {Struct.GgDsLinkedListItem}
    ///
    /// @context GgDsLinkedList
    ///
    static get_cursor = function()
    {
        return cursor;
    }

    /// @func length()
    ///
    /// @desc
    ///    Return the "count" of the linked list, ie: the
    ///    number of items that have been added into it.
    ///
    /// @return {Real}
    ///
    /// @context GgDsLinkedList
    ///
    static length = function()
    {
        return count;
    }

    /// @func clear()
    ///
    /// @desc
    ///    Clear all data from the given
    ///    linked list data-structure.
    ///
    /// @context GgDsLinkedList
    ///
    static clear = function()
    {
        repeat count
        {
            remove(0);
        }

        head = undefined;
        tail = undefined;
        cursor = undefined;
    }

    #endregion public

    #endregion methods
}
