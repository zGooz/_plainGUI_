
/// @func GgTheme()
///
/// @desc
///    Component for storing components, which
///    determine the appearance of the widget.
///
/// @context undefined
///
function GgTheme() constructor
{
    #region methods

    #region public

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgTheme
    ///
    static free = function()
    {
        disable.free();
        delete disable;

        normal.free();
        delete normal;

        over.free();
        delete over;

        hold.free();
        delete hold;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    disable = new GgSkin();
    normal = new GgSkin();
    over = new GgSkin();
    hold = new GgSkin();

    #endregion public

    #endregion variables
}
