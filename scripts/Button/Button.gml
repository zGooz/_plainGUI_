
/// @func GgButton([_area], [_precisioned_area])
///
/// @desc
///    Base object for all buttons.
///
/// @arg {Struct.GgWorkArea} _area
///    Widget workspace.
///
/// @arg {Struct.GgShape} _precisioned_area
///    A more precise mask to check for a mouse click.
///
/// @context undefined
///
function GgButton(_area = undefined, _precisioned_area = undefined)
: GgWidget() constructor
{
    #region methods

    #region public

    /// @func delete_resouces()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgButton
    ///
    static delete_resouces = function()
    {
        ev_click.free();
        delete ev_click;
    }

    #endregion public

    #region private

    /// @func on_click()
    ///
    /// @desc
    ///    Response to a click, according to
    ///    the usual or more precise area.
    ///
    /// @context GgButton
    ///
    /// @ignore
    ///
    static on_click = function()
    {
        if is_undefined(precisioned_area)
        {
            ev_click.notify();
        }
        else
        {
            if precisioned_area.check()
            {
                ev_click.notify();
            }
        }
    }

    #endregion private

    #endregion methods

    #region events

    ev_click = new GgEvent();

    #endregion events

    #region variables

    #region private

    /// @ignore
    precisioned_area = _precisioned_area;

    #endregion private

    #endregion variables

    #region settings

    area.ev_click.subscribe("click", method(self, on_click));
    super_free = method(self, delete_resouces);

    #endregion settings
}
