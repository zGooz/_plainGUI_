
/// @func GgLinkedListWalker()
///
/// @desc
///    Linked-list store extension.
///
/// @context GgLinkedList
///
function GgLinkedListWalker(): GgLinkedList() constructor
{
    #region variables

    #region public

    flow = GG_FLOW.TO_TAIL;

    #endregion public

    #endregion variables

    #region methods

    #region public

    /// @func step()
    ///
    /// @desc
    ///    Returns the next element of the list.
    ///
    /// @context GgLinkedListWalker
    ///    
    static step = function()
    {
        if flow == GG_FLOW.TO_HEAD
        {
            content.previous();
        }
        else if flow == GG_FLOW.TO_TAIL
        {
            content.next();
        }
    }

    /// @func get_element()
    ///
    /// @desc
    ///    Returns a special pointer.
    ///
    /// @return {Struct.GgDsLinkedListItem}
    ///
    /// @context GgLinkedListWalker
    ///
    static get_element = function()
    {
        content.get_cursor();
    }

    #endregion public

    #endregion methods

    #region settings

    ds_linked_list_destroy(content);

    /// @ignore
    content = ds_linked_list_closed_create();

    #endregion settings
}
