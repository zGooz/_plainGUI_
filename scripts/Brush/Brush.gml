
/// @func GgBrush([_color], [_alpha])
///
/// @desc
///    This component is used to store
///    and provide color and transparency.
///
/// @arg {Constant.Color} _color;
///    Brush color.
///
/// @arg {Real} _alpha
///    The alpha-channel value (transparency) of the brush.
///
/// @context undefined
///
function GgBrush(_color = c_white, _alpha = 1) constructor
{
    #region methods

    #region public

    /// @func set_decoration([_color], [_alpha])
    ///
    /// @desc
    ///    Set the color and transparency (alpha channel) of the brush.
    ///
    /// @arg {Constant.Color} _color;
    ///    brush color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha channel value (transparency) of the brush.
    ///
    /// @context GgBrush
    ///
    static set_decoration = function(_color, _alpha)
    {
        if _alpha < 0 || _alpha > 1
        {
            // exception
        }

        color = _color;
        alpha = _alpha;
    }

    /// @func get_opacity()
    ///
    /// @desc
    ///    Returns the opacity of the brush.
    ///
    /// @return {Real}
    ///
    /// @context GgBrush
    ///
    static get_opacity = function()
    {
        return 1 - alpha;
    }

    #endregion public

    #endregion methods

    #region variables

    #region public

    color = _color;
    alpha = _alpha;

    #endregion public

    #endregion variables
}
