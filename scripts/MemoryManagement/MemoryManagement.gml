
/// @func gg_free_item(_item)
///
/// @desc
///    Default element removal function.
///
/// @arg {Struct} _item
///    Element to remove
///
/// @context undefined
///
function gg_free_item(_item)
{
    if is_undefined(_item)
    {
        return;
    }

    if is_struct(_item)
    {
        if variable_struct_exists(_item, "clear")
        {
            _item.clear();
        }

        if variable_struct_exists(_item, "free")
        {
            _item.free();
        }

        delete _item;
    }
}
