
/// @func ds_tree_create()
///
/// @desc
///    Data structure representing from itself a tree.
///
/// @context undefined
///
function ds_tree_create()
{
    return new GgDsTree();
}

/// @func ds_tree_item_create(_name, _value)
///
/// @desc
///    Creates an element node, which can be
///    add to the tree (more than one).
///
/// @arg {String} _name
///    The name of the element in the tree.
///
/// @arg {Any} _value
///    Auxiliary data, for storage|use.
///
/// @context undefined
///
function ds_tree_item_create(_name, _value)
{
    return new GgDsTreeNode(_value, _name, false);
}

/// @func ds_tree_folder_create(_name)
///
/// @desc
///    Creates a folder element, which can be
///    add to the tree (more than one).
///
/// @arg {String} _name
///    Name of the folder in the tree.
///
/// @context undefined
///
function ds_tree_folder_create(_name)
{
    return new GgDsTreeNode(, _name, true);
}

/// @func ds_tree_include_node(_id, _child, [_parent])
///
/// @desc
///    Put the node in a tree.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Struct.GgDsTreeNode} _child
///    A branch of the tree, for addition.
///
/// @arg {Struct.GgDsTreeNode} _parent
///    Branch of the tree to which we are adding.
///
/// @context undefined
///
function ds_tree_include_node(_id, _child, _parent = undefined)
{
    if is_undefined(_parent)
    {
        _parent = _id.root;
    }

    _id.include(_child, _parent);
}

/// @func ds_tree_exclude_node(_id, _child, [_parent])
///
/// @desc
///    Kick the node in a tree.
///    Note: Извлекаемая ветка не удаляется.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Struct.GgDsTreeNode} _child
///    Tree branch to retrieve.
///
/// @arg {Struct.GgDsTreeNode} _parent
///    The branch of the tree that we are
///    extracting from the tree.
///
/// @context undefined
///
function ds_tree_exclude_node(_id, _child, _parent = undefined)
{
    if is_undefined(_parent)
    {
        _parent = _id.root;
    }

    _id.exclude(_child, _parent);
}

/// @func ds_tree_size(_id, [_with_closed])
///
/// @desc
///    Returns the number of items and folders.
///    By default, the function skips closed folders.
///    But the passed argument changes this behavior.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Bool} _with_closed
///    Whether to allow bypassing closed folders.
///
/// @return {Real}
///
/// @context undefined
///
function ds_tree_size(_id, _with_closed = false)
{
    return _id.count(_with_closed);
}

/// @func ds_tree_height(_id, [_with_closed])
///
/// @desc
///    Returns the height of the tree (nesting depth).
///    By default, the function skips closed folders.
///    But the passed argument changes this behavior.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Bool} _with_closed
///    Whether to allow bypassing closed folders.
///
/// @return {Real}
///
/// @context undefined
///
function ds_tree_height(_id, _with_closed = false)
{
    return _id.height(_with_closed);
}

/// @func ds_tree_width(_id, [_with_closed])
///
/// @desc
///    Returns the width of the tree.
///    By default, the function skips closed folders.
///    But the passed argument changes this behavior.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Bool} _with_closed
///    Whether to allow bypassing closed folders.
///
/// @return {Real}
///
/// @context undefined
///
function ds_tree_width(_id, _with_closed = false)
{
    return _id.width(_with_closed);
}

/// @func ds_tree_destroy(_id)
///
/// @desc
///    Delete the tree, and deallocate
///    the memory it occupies.
///
/// @arg {Struct.GgDsTree} _id;
///    The tree we are working with.
///
/// @arg {Bool} _with_closed
///    Whether to allow bypassing closed folders.
///
/// @return {Real}
///
/// @context undefined
///
function ds_tree_destroy(_id)
{
    _id.free();
}
