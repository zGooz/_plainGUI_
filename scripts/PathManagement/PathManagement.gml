
/// @func gg_path_top_left_corner_pos(_source)
///
/// @desc
///    Calculates the positions of the
///    upper left corner of the path, for external use.
///
/// @arg {Asset.GmPath} _source
///    The path that is used when calculating the point.
///
/// @return {Struct.GgPosition}
///
/// @context undefined
///
function gg_path_top_left_corner_pos(_source)
{
    var _count = path_get_number(_source);

    if ! _count
    {
        // exception
    }

    var _x = 99999999;
    var _y = 99999999;

    for (var _i = 0; _i < _count; _i += 1)
    {
        var _px = path_get_point_x(_source, _i);
        var _py = path_get_point_y(_source, _i);

        _x = min(_x, _px);
        _y = min(_y, _py);
    }

    return new GgPosition(_x, _y);
}

/// @func gg_dist_to_top_left_corner(_source, _corner)
///
/// @desc
///    Calculates an array of pairs [x0, y0, x1, y1, ...],
///    what are the distances between positions
///    waypoints and top left corner point,
///    for external use.
///
/// @arg {Asset.GmPath} _source
///    Path that is used when calculating offsets.
///
/// @arg {Struct.GgPosition} _corner
///    Point relative to which
///    calculations will be made.
///
/// @return {Array<Real>}
///
/// @context undefined
///
function gg_dist_to_top_left_corner(_source, _corner)
{
    var _count = path_get_number(_source);

    if ! _count
    {
        // exception
    }

    var _distances = [_count * 2];
    var _j = 0;

    for (var _i = 0; _i < _count; _i += 1)
    {
        var _px = path_get_point_x(_source, _i);
        var _py = path_get_point_y(_source, _i);

        _distances[_j + 0] = _px - _corner.x;
        _distances[_j + 1] = _py - _corner.y;
        _j += 2;
    }

    return _distances;
}

/// @func gg_shift_to_top_left_corner(_distances)
///
/// @desc
///    Shifts offsets by the value of the first point.
///    So that the first point becomes upper left corner.
///
/// @arg {Array<Real>} _distances
///    Array of waypoints, if left top
///    path angle, take as origin.
///
/// @context undefined
///
function gg_shift_to_top_left_corner(_distances)
{
    var _count = array_length(_distances);

    if ! _count
    {
        // exception
    }

    var _offset_x = _distances[@ 0];
    var _offset_y = _distances[@ 1];

    for (var _i = 0; _i < _count; _i += 2)
    {
        _distances[@ _i + 0] -= _offset_x;
        _distances[@ _i + 1] -= _offset_y;
    }
}

/// @func gg_point_in_polygon(_offset, _corner, _distances)
///
/// @desc
///    Checks if a point belongs to a polygon.
///
/// @arg {Struct.GgPosition} _offset
///    Polygon shift.
///
/// @arg {Struct.GgPosition} _corner
///    The position of the top left polygon.
///
/// @arg {Array<Real>} _distances
///    Distances from the upper left corner of
///    the polygon to all its points.
///
/// @return {Bool}
///
/// @context undefined
///
function gg_point_in_polygon(_offset, _corner, _distances)
{
    var _count = array_length(_distances);

    if ! _count
    {
        // exception
    }

    var _hover = 1;
    var _mx = GG_GUI_MOUSE_X;
    var _my = GG_GUI_MOUSE_Y;
    var _is_not_exists = is_undefined(_offset);
    var _px = (_is_not_exists ? 0 : _offset.x) + _corner.x;
    var _py = (_is_not_exists ? 0 : _offset.y) + _corner.y;

    for (var _i = 0; _i < _count; _i += 2)
    {
        var _j = _i + 2 == _count ? 0 : _i + 2;
        var _ax = _px + _distances[@ _i + 0] - _mx;
        var _ay = _py + _distances[@ _i + 1] - _my;
        var _bx = _px + _distances[@ _j + 0] - _mx;
        var _by = _py + _distances[@ _j + 1] - _my;

        if _ay ^ _by >= 0
        {
            if _ax | _ay == 0 || _bx | _by == 0
            {
                return false;
            }

            if _ay | _by != 0
            {
                _hover *= 1;
                continue;
            }

            _hover *= _ax ^ _bx >> 32 + 1;
            continue;
        }

        var _skew_product = _ax * _by - _ay * _bx;
        _hover *= (_skew_product >> 32 ^ _ay >> 31)
                - (-_skew_product >> 32 ^ _ay >> 31);
    }

    return _hover == -1;
}
