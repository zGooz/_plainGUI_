
/// @func GgWorkArea(_transform)
///
/// desc
///    This component is required for more fine-grained
///    check to see if it has focus one or another
///    component, if in focus, then whether it is
///    being manipulated with the mouse, etc.
///
/// @arg {Struct.GgTransform} _transform
///    Transform to orient the image.
///
/// @context undefined
///
function GgWorkArea(_transform) constructor
{
    #region methods

    #region public

    /// @func monitor()
    ///
    /// @desc
    ///    Checks the state of the area, having
    ///    focus, holding down the mouse button
    ///    call necessary events.
    ///
    /// @context GgWorkArea
    ///
    static monitor = function()
    {
        if is_undefined(transform)
        {
            // exception
        }

        var _cursor_is_over = transform.cursor_is_over();

        mouse_cursor_on = _cursor_is_over;

        if _cursor_is_over
        {
            if cursor == GG_CURSOR.LEAVE
            {
                ev_over_start.notify();
            }

            cursor = GG_CURSOR.ENTER;
        }
        else
        {
            if cursor == GG_CURSOR.ENTER
            {
                ev_over_end.notify();
            }

            cursor = GG_CURSOR.LEAVE;
        }

        if mouse_check_button_pressed(mb_left)
        {
            has_focus = _cursor_is_over;
            has_mb_hold = true;

            if _cursor_is_over
            {
                ev_poke.notify();
            }
        }
        
        if mouse_check_button_released(mb_left)
        {
            has_mb_hold = false;

            if _cursor_is_over
            {
                ev_click.notify();
            }
        }
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgWorkArea
    ///
    static free = function()
    {
        ev_over_start.free();
        delete ev_over_start;

        ev_over_end.free();
        delete ev_over_end;

        ev_poke.free();
        delete ev_poke;

        ev_click.free();
        delete ev_click;
    }

    #endregion public

    #endregion methods

    #region events

    ev_over_start = new GgEvent();
    ev_over_end = new GgEvent();
    ev_poke = new GgEvent();
    ev_click = new GgEvent();

    #endregion events

    #region variables

    #region public

    has_focus = false;
    mouse_cursor_on = false;
    has_mb_hold = false;

    #endregion public

    #region private

    /// @ignore
    transform = _transform;

    /// ignore
    cursor = false;

    #endregion private

    #endregion variables

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgWorkArea
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();

        transform.draw_gizmos(_color, _alpha);

        var _status = string_ext (
            "focus: {0}\nhover: {1}\nmouse button hold: {2}",
            [has_focus, mouse_cursor_on, has_mb_hold]
        );

        var _text_x = transform.position.x + 10;
        var _text_y = transform.position.y + 10;

        draw_set_color(_color);
        draw_set_alpha(_alpha);

        draw_text(_text_x, _text_y, _status);

        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    #endregion debug
}
