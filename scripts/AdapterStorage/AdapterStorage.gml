
/// @func GgAdapterStorage()
///
/// @desc
///    The parent class of a non-enumerable store.
///
/// @context undefined
///
/// @ignore
///
function GgAdapterStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = undefined;

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func push(_item)
    ///
    /// @desc
    ///    Pushes an item into storage.
    ///    
    /// @arg {Any} _item
    ///    The element to add.
    ///
    /// @context GgAdapterStorage
    ///
    static push = function(_item){}

    /// @func get()
    ///
    /// @desc
    ///    Gets the element, but does not remove it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorage
    ///
    static get = function(){}

    /// @func put()
    ///
    /// @desc
    ///    Gets an element and removes it.
    ///
    /// @return {Any}
    ///
    /// @context GgAdapterStorage
    ///
    static put = function(){}

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {Real}
    ///
    /// @context GgAdapterStorage
    ///
    static count = function(){}

    /// @func process_all(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {Any} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgAdapterStorage
    ///
    static process_all = function(_proc, _data = undefined)
    {
        repeat count()
        {
            _proc(put(), _data);
        }
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    The procedure that is applied to
    ///    an element when cleared|deleted.
    ///
    /// @context GgSequenceStorage
    ///
    static clear = function(_proc) {}

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgSequenceStorage
    ///
    static free = function(){}

    #endregion public

    #endregion methods
}
