
/// @func GgSequenceStorageStruct()
///
/// @context GgSequenceStorage
///
function GgSequenceStorageStruct(): GgSequenceStorage() constructor
{
    #region variables

    #region private

    /// @ignore
    content = {};

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func append(_item, _id)
    ///
    /// @desc
    ///    Adds an element to the store.
    ///
    /// @arg {each} _item
    ///    Element to add.
    ///
    /// @arg {string} _id
    ///    Identifier under which the element is stored.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static append = function(_item, _id)
    {
        if variable_struct_exists(content, _id)
        {
            // exception
        }
        else
        {
            content[$ _id] = _item;
        }
    }

    /// @func remove(_id)
    ///
    /// @desc
    ///    Removes the element from the storage.
    ///
    /// @arg {string} _id
    ///    ID of the element to remove.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static remove = function(_id)
    {
        if variable_struct_exists(content, _id)
        {
            variable_struct_remove(content, _id);
        }
        else
        {
            // exception
        }
    }

    /// @func change(_item, _id)
    ///
    /// @desc
    ///    Edits an element or replaces
    ///    one element with another.
    ///
    /// @arg {each} _item
    ///    Element to edit.
    ///
    /// @arg {string} _id
    ///    Element ID, to edit|replace.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static change = function(_item, _id)
    {
        if variable_struct_exists(content, _id)
        {
            content[$ _id] = _item;
        }
        else
        {
            // exception
        }
    }

    /// @func get(_id)
    ///
    /// @desc
    ///    Returning an element from storage.
    ///
    /// @arg {string} _id
    ///    Element ID.
    ///
    /// @return {each}
    ///
    /// @context GgSequenceStorageStruct
    ///
    static get = function(_id)
    {
        var _item;

        try
        {
            _item = content[$ _id];
        }
        catch (_error)
        {
            _item = undefined;
        }

        return _item;
    }

    /// @func count()
    ///
    /// @desc
    ///    Returns the number of elements.
    ///
    /// @return {real}
    ///
    /// @context GgSequenceStorageStruct
    ///
    static count = function()
    {
        return variable_struct_names_count(content);
    }

    /// @func foreach(_proc, _data)
    ///
    /// @desc
    ///    The function to iterate over all storage elements.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    each argument while iterating.
    ///    Its signature is "function(item, data)",
    ///    where data, auxiliary data.
    ///
    /// @arg {each} _data
    ///    Auxiliary data for the procedure
    ///    iteration of arguments.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static foreach = function(_proc, _data = undefined)
    {
        var _ids = variable_struct_get_names(content);

        for (var _i = 0; _i < count(); _i += 1)
        {
            _proc(get(_ids[@ _i]), _data);
        }
    }

    /// @func clear(_proc)
    ///
    /// @desc
    ///    Clears the storage of contents.
    ///
    /// @arg {function} _proc
    ///    Procedure that applies to
    ///    element when clearing|deleting.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static clear = function(_proc)
    {
        foreach(_proc);
        content = {};
    }

    /// @func free()
    ///
    /// @desc
    ///    Deletes a repository.
    ///
    /// @context GgSequenceStorageStruct
    ///
    static free = function()
    {
        delete content;
    }

    #endregion public

    #endregion methods
}
