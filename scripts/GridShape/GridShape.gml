
/// @func GgGridShape(_position)
///
/// @desc
///    The component checks for clicks on grid cells.
///
/// @arg {Struct.GgPosition} _position
///    The point that is used as the top left corner.
///
/// @context undefined
///
function GgGridShape(_position): GgShape() constructor
{
    #region events

    ev_click = new GgEvent();

    #endregion events

    #region variables

    #region private

    /// @ignore
    anchor = _position;

    /// @ignore
    corner = new GgPosition(anchor.x, anchor.y);

    /// @ignore
    gap = {
        horizontal: 0,
        vertical: 0,
    };

    /// @ignore
    cell = {
        width: 1,
        height: 1,
    };

    /// @ignore
    field = {
        width: 1,
        height: 1,
    };

    /// @ignore
    view = {
        left: 0,
        top: 0,
        width: 1,
        height: 1,
    };

    /// @ignore
    scroll = {
        factor_x: 1,
        factor_y: 1,
    };

    #endregion private

    #endregion variables

    #region methods

    #region public

    /// @func scroll_to_left(_step)
    ///
    /// @desc
    ///    Scrolls the grid to the left.
    ///
    /// @arg {Real} _step
    ///    Scroll step.
    ///
    /// @context GgGridShape
    ///
    static scroll_to_left = function(_step)
    {
        scrolling(_step, -1, 0);
    }

    /// @func scroll_to_right(_step)
    ///
    /// @desc
    ///    Scrolls the grid to the right.
    ///
    /// @arg {Real} _step
    ///    Scroll step.
    ///
    /// @context GgGridShape
    ///
    static scroll_to_right = function(_step)
    {
        scrolling(_step, 1, 0);
    }

    /// @func scroll_to_up(_step)
    ///
    /// @desc
    ///    Scrolls the grid to the up.
    ///
    /// @arg {Real} _step
    ///    Scroll step.
    ///
    /// @context GgGridShape
    ///
    static scroll_to_up = function(_step)
    {
        scrolling(_step, 0, -1);
    }

    /// @func scroll_to_down(_step)
    ///
    /// @desc
    ///    Scrolls the grid to the down.
    ///
    /// @arg {Real} _step
    ///    Scroll step.
    ///
    /// @context GgGridShape
    ///
    static scroll_to_down = function(_step)
    {
        scrolling(_step, 0, 1);
    }

    /// @func look_at(_percent_hor, _percent_ver)
    ///
    /// @desc
    ///    Fixes the position of the visible area.
    ///
    /// @context GgRadianShape
    ///
    static look_at = function(_percent_hor, _percent_ver)
    {
        var _total_hor = max(0, field.width - view.width);
        var _total_ver = max(0, field.height - view.height);
        var _left = _percent_hor * _total_hor;
        var _top = _percent_ver * _total_ver;

        view.left = clamp(_left, 0, _total_hor);
        view.top = clamp(_top, 0, _total_ver);
        place_corner();
    }

    /// @func set_view_size(_width, _height)
    ///
    /// @desc
    ///    Sets the size of the workspace.
    ///
    /// @arg {Real} _width
    ///    Width of work area.
    ///
    /// @arg {Real} _height
    ///    Height of work area.
    ///
    /// @context GgGridShape
    ///
    static set_view_size = function(_width, _height)
    {
        if _width <= 0
        {
            // exception
        }

        if _height <= 0
        {
            // exception
        }

        view.width = _width;
        view.height = _height;
    }

    /// @func set_gap([_horizontal], [_vertical])
    ///
    /// @desc
    ///    Sets the gaps between cells.
    ///
    /// @arg {Real} _horizontal
    ///    The amount of indent between cells, horizontally.
    ///
    /// @arg {Real} _vertical
    ///    Indent value between cells, vertically.
    ///
    /// @context GgGridShape
    ///
    static set_gap = function(_horizontal = 0, _vertical = 0)
    {
        if _horizontal < 0
        {
            // exception
        }

        if _vertical < 0
        {
            // exception
        }

        gap.horizontal = _horizontal;
        gap.vertical = _vertical;
    }

    /// @func set_cell_size([_width], [_height])
    ///
    /// @desc
    ///    Sets the size of grid cells.
    ///
    /// @arg {Real} _width
    ///    Cell width.
    ///
    /// @arg {Real} _height
    ///    Cell height.
    ///
    /// @context GgGridShape
    ///
    static set_cell_size = function(_width = 1, _height = 1)
    {
        if _width <= 0
        {
            // exception
        }

        if _height <= 0
        {
            // exception
        }

        cell.width = _width;
        cell.height = _height;
    }

    /// @func set_grid_size([_width], [_height])
    ///
    /// @desc
    ///    Sets the grid size.
    ///
    /// @arg {Real}_width
    ///    Grid width.
    ///
    /// @arg {Real}_height
    ///    Grid height.
    ///
    /// @context GgGridShape
    ///
    static set_grid_size = function(_width = 1, _height = 1)
    {
        if _width <= 0
        {
            // exception
        }

        if _height <= 0
        {
            // exception
        }

        field.width = ceil(_width);
        field.height = ceil(_height);
    }

    /// @func check(_i, _j)
    ///
    /// @desc
    ///    Function to check if whether there was a
    ///    mouse click within the given boundaries.
    ///
    /// @arg {Real} _i
    ///    Horizontal cell index.
    ///
    /// @arg {Real} _j
    ///    Vertical cell index.
    ///
    /// @return {Bool}
    ///
    /// @context GgRadianShape
    ///
    static check = function(_i, _j)
    {
        var _mx = GG_GUI_MOUSE_X;
        var _my = GG_GUI_MOUSE_Y;
        var _width_with_gap = cell.width + gap.horizontal;
        var _height_with_gap = cell.height + gap.vertical;

        var _tlx = anchor.x;
        var _tly = anchor.y;
        var _view_width = _width_with_gap * view.width;
        var _view_height = _height_with_gap * view.height;
        var _brx = _tlx + _view_width - gap.horizontal;
        var _bry = _tly + _view_height - gap.vertical;

        if ! point_in_rectangle(_mx, _my, _tlx, _tly, _brx, _bry)
        {
            return false;
        }

        var _ci = floor((_mx - corner.x) / _width_with_gap);
        var _cj = floor((_my - corner.y) / _height_with_gap);

        ev_click.notify({i: _ci, j: _cj});

        if _i == _ci && _j == _cj
        {
            var _cx = corner.x + _ci * _width_with_gap;
            var _cy = corner.y + _cj * _height_with_gap;

            return (_mx - _cx) < cell.width
                && (_my - _cy) < cell.height;
        }

        return false;
    }

    /// @func free()
    ///
    /// @desc
    ///    Frees the occupied memory.
    ///
    /// @context GgRadianShape
    ///
    static free = function()
    {
        delete gap;
        delete cell;
        delete field;
        delete view;
        delete scroll;
        delete corner;

        ev_click.free();
        delete ev_click;
    }

    #endregion public

    #region private

    /// @func scrolling(_step, _dir_hor, _dir_ver)
    ///
    /// @desc
    ///    Scrolls the grid in two directions.
    ///
    /// @arg {Real} _step
    ///    Scroll step.
    ///
    /// @arg {Real} _dir_hor
    ///    Horizontal direction of scrolling.
    ///
    /// @arg {Real} _dir_ver
    ///    Vertical direction of scrolling.
    ///
    /// @context GgRadianShape
    ///
    /// @ignore
    ///
    static scrolling = function(_step, _dir_hor, _dir_ver)
    {
        if _step <= 0
        {
            // exception
        }

        var _total_hor = max(0, field.width - view.width);
        var _total_ver = max(0, field.height - view.height);

        view.left = clamp(view.left + _step * _dir_hor, 0, _total_hor);
        view.top = clamp(view.top + _step * _dir_ver, 0, _total_ver);
        place_corner();
    }

    /// @func place_corner()
    ///
    /// @desc
    ///    Changes the location of the grid.
    ///
    /// @context GgRadianShape
    ///
    /// @ignore
    ///
    static place_corner = function()
    {
        var _width_with_gap = cell.width + gap.horizontal;
        var _height_with_gap = cell.height + gap.vertical;
        var _view_width = _width_with_gap * view.left;
        var _view_height = _height_with_gap * view.top;

        corner.x = anchor.x - _view_width;
        corner.y = anchor.y - _view_height;
    }

    #endregion private

    #endregion methods

    #region debug

    /// @func draw_gizmos([_color], [_alpha])
    ///
    /// @desc
    ///    Draw debug info.
    ///
    /// @arg {Constant.Color} _color
    ///    Render color.
    ///
    /// @arg {Real} _alpha
    ///    The alpha value (transparency) for render.
    ///
    /// @context GgRadianShape
    ///
    static draw_gizmos = function(_color = c_white, _alpha = 1)
    {
        var _color_old = draw_get_color();
        var _alpha_old = draw_get_alpha();
        draw_set_color(_color);
        draw_set_alpha(_alpha);

        for (var _i = 0; _i < field.width; _i += 1)
        {
            var _rx = corner.x + (cell.width + gap.horizontal) * _i;

            for (var _j = 0; _j < field.height; _j += 1)
            {
                var _ry = corner.y + (cell.height + gap.vertical) * _j;
                //draw_text(_rx, _ry, check(_i, _j));

                draw_rectangle (
                    _rx, _ry, _rx + cell.width, _ry + cell.height, true
                );
            }
        }

        var _width_with_gap = cell.width + gap.horizontal;
        var _height_with_gap = cell.height + gap.vertical;
        var _tlx = anchor.x;
        var _tly = anchor.y;
        var _view_width = _width_with_gap * view.width - gap.horizontal;
        var _view_height = _height_with_gap * view.height - gap.vertical;
        var _brx = _tlx + _view_width;
        var _bry = _tly + _view_height;

        draw_set_alpha(0.1);
        draw_rectangle(_tlx, _tly, _brx, _bry, false);
        draw_set_color(_color_old);
        draw_set_alpha(_alpha_old);
    }

    #endregion debug
}
