{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Source Code Pro Medium",
  "styleName": "Medium",
  "size": 14.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": true,
  "TTFName": "${project_dir}\\fonts\\Font1\\Font1.png",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":11,"h":23,"character":32,"shift":11,"offset":0,},
    "33": {"x":22,"y":77,"w":4,"h":23,"character":33,"shift":11,"offset":4,},
    "34": {"x":13,"y":77,"w":7,"h":23,"character":34,"shift":11,"offset":2,},
    "35": {"x":2,"y":77,"w":9,"h":23,"character":35,"shift":11,"offset":1,},
    "36": {"x":235,"y":52,"w":9,"h":23,"character":36,"shift":11,"offset":1,},
    "37": {"x":222,"y":52,"w":11,"h":23,"character":37,"shift":11,"offset":0,},
    "38": {"x":209,"y":52,"w":11,"h":23,"character":38,"shift":11,"offset":0,},
    "39": {"x":204,"y":52,"w":3,"h":23,"character":39,"shift":11,"offset":4,},
    "40": {"x":196,"y":52,"w":6,"h":23,"character":40,"shift":11,"offset":3,},
    "41": {"x":188,"y":52,"w":6,"h":23,"character":41,"shift":11,"offset":2,},
    "42": {"x":28,"y":77,"w":9,"h":23,"character":42,"shift":11,"offset":1,},
    "43": {"x":177,"y":52,"w":9,"h":23,"character":43,"shift":11,"offset":1,},
    "44": {"x":158,"y":52,"w":5,"h":23,"character":44,"shift":11,"offset":3,},
    "45": {"x":147,"y":52,"w":9,"h":23,"character":45,"shift":11,"offset":1,},
    "46": {"x":140,"y":52,"w":5,"h":23,"character":46,"shift":11,"offset":3,},
    "47": {"x":129,"y":52,"w":9,"h":23,"character":47,"shift":11,"offset":1,},
    "48": {"x":118,"y":52,"w":9,"h":23,"character":48,"shift":11,"offset":1,},
    "49": {"x":107,"y":52,"w":9,"h":23,"character":49,"shift":11,"offset":1,},
    "50": {"x":96,"y":52,"w":9,"h":23,"character":50,"shift":11,"offset":1,},
    "51": {"x":84,"y":52,"w":10,"h":23,"character":51,"shift":11,"offset":0,},
    "52": {"x":71,"y":52,"w":11,"h":23,"character":52,"shift":11,"offset":0,},
    "53": {"x":165,"y":52,"w":10,"h":23,"character":53,"shift":11,"offset":0,},
    "54": {"x":39,"y":77,"w":10,"h":23,"character":54,"shift":11,"offset":1,},
    "55": {"x":51,"y":77,"w":9,"h":23,"character":55,"shift":11,"offset":1,},
    "56": {"x":62,"y":77,"w":9,"h":23,"character":56,"shift":11,"offset":1,},
    "57": {"x":47,"y":102,"w":9,"h":23,"character":57,"shift":11,"offset":1,},
    "58": {"x":40,"y":102,"w":5,"h":23,"character":58,"shift":11,"offset":3,},
    "59": {"x":33,"y":102,"w":5,"h":23,"character":59,"shift":11,"offset":3,},
    "60": {"x":23,"y":102,"w":8,"h":23,"character":60,"shift":11,"offset":2,},
    "61": {"x":12,"y":102,"w":9,"h":23,"character":61,"shift":11,"offset":1,},
    "62": {"x":2,"y":102,"w":8,"h":23,"character":62,"shift":11,"offset":1,},
    "63": {"x":235,"y":77,"w":9,"h":23,"character":63,"shift":11,"offset":1,},
    "64": {"x":222,"y":77,"w":11,"h":23,"character":64,"shift":11,"offset":0,},
    "65": {"x":209,"y":77,"w":11,"h":23,"character":65,"shift":11,"offset":0,},
    "66": {"x":197,"y":77,"w":10,"h":23,"character":66,"shift":11,"offset":1,},
    "67": {"x":185,"y":77,"w":10,"h":23,"character":67,"shift":11,"offset":1,},
    "68": {"x":173,"y":77,"w":10,"h":23,"character":68,"shift":11,"offset":1,},
    "69": {"x":163,"y":77,"w":8,"h":23,"character":69,"shift":11,"offset":2,},
    "70": {"x":153,"y":77,"w":8,"h":23,"character":70,"shift":11,"offset":2,},
    "71": {"x":140,"y":77,"w":11,"h":23,"character":71,"shift":11,"offset":0,},
    "72": {"x":129,"y":77,"w":9,"h":23,"character":72,"shift":11,"offset":1,},
    "73": {"x":118,"y":77,"w":9,"h":23,"character":73,"shift":11,"offset":1,},
    "74": {"x":107,"y":77,"w":9,"h":23,"character":74,"shift":11,"offset":1,},
    "75": {"x":95,"y":77,"w":10,"h":23,"character":75,"shift":11,"offset":1,},
    "76": {"x":84,"y":77,"w":9,"h":23,"character":76,"shift":11,"offset":2,},
    "77": {"x":73,"y":77,"w":9,"h":23,"character":77,"shift":11,"offset":1,},
    "78": {"x":60,"y":52,"w":9,"h":23,"character":78,"shift":11,"offset":1,},
    "79": {"x":47,"y":52,"w":11,"h":23,"character":79,"shift":11,"offset":0,},
    "80": {"x":35,"y":52,"w":10,"h":23,"character":80,"shift":11,"offset":1,},
    "81": {"x":10,"y":27,"w":11,"h":23,"character":81,"shift":11,"offset":0,},
    "82": {"x":236,"y":2,"w":10,"h":23,"character":82,"shift":11,"offset":1,},
    "83": {"x":224,"y":2,"w":10,"h":23,"character":83,"shift":11,"offset":1,},
    "84": {"x":211,"y":2,"w":11,"h":23,"character":84,"shift":11,"offset":0,},
    "85": {"x":200,"y":2,"w":9,"h":23,"character":85,"shift":11,"offset":1,},
    "86": {"x":187,"y":2,"w":11,"h":23,"character":86,"shift":11,"offset":0,},
    "87": {"x":173,"y":2,"w":12,"h":23,"character":87,"shift":11,"offset":0,},
    "88": {"x":160,"y":2,"w":11,"h":23,"character":88,"shift":11,"offset":0,},
    "89": {"x":147,"y":2,"w":11,"h":23,"character":89,"shift":11,"offset":0,},
    "90": {"x":135,"y":2,"w":10,"h":23,"character":90,"shift":11,"offset":1,},
    "91": {"x":2,"y":27,"w":6,"h":23,"character":91,"shift":11,"offset":4,},
    "92": {"x":124,"y":2,"w":9,"h":23,"character":92,"shift":11,"offset":1,},
    "93": {"x":103,"y":2,"w":7,"h":23,"character":93,"shift":11,"offset":1,},
    "94": {"x":92,"y":2,"w":9,"h":23,"character":94,"shift":11,"offset":1,},
    "95": {"x":80,"y":2,"w":10,"h":23,"character":95,"shift":11,"offset":1,},
    "96": {"x":73,"y":2,"w":5,"h":23,"character":96,"shift":11,"offset":2,},
    "97": {"x":62,"y":2,"w":9,"h":23,"character":97,"shift":11,"offset":1,},
    "98": {"x":50,"y":2,"w":10,"h":23,"character":98,"shift":11,"offset":1,},
    "99": {"x":38,"y":2,"w":10,"h":23,"character":99,"shift":11,"offset":1,},
    "100": {"x":27,"y":2,"w":9,"h":23,"character":100,"shift":11,"offset":1,},
    "101": {"x":15,"y":2,"w":10,"h":23,"character":101,"shift":11,"offset":1,},
    "102": {"x":112,"y":2,"w":10,"h":23,"character":102,"shift":11,"offset":1,},
    "103": {"x":23,"y":27,"w":10,"h":23,"character":103,"shift":11,"offset":1,},
    "104": {"x":144,"y":27,"w":9,"h":23,"character":104,"shift":11,"offset":1,},
    "105": {"x":35,"y":27,"w":7,"h":23,"character":105,"shift":11,"offset":1,},
    "106": {"x":14,"y":52,"w":8,"h":23,"character":106,"shift":11,"offset":0,},
    "107": {"x":2,"y":52,"w":10,"h":23,"character":107,"shift":11,"offset":1,},
    "108": {"x":237,"y":27,"w":10,"h":23,"character":108,"shift":11,"offset":1,},
    "109": {"x":224,"y":27,"w":11,"h":23,"character":109,"shift":11,"offset":0,},
    "110": {"x":213,"y":27,"w":9,"h":23,"character":110,"shift":11,"offset":1,},
    "111": {"x":201,"y":27,"w":10,"h":23,"character":111,"shift":11,"offset":1,},
    "112": {"x":189,"y":27,"w":10,"h":23,"character":112,"shift":11,"offset":1,},
    "113": {"x":178,"y":27,"w":9,"h":23,"character":113,"shift":11,"offset":1,},
    "114": {"x":167,"y":27,"w":9,"h":23,"character":114,"shift":11,"offset":2,},
    "115": {"x":24,"y":52,"w":9,"h":23,"character":115,"shift":11,"offset":1,},
    "116": {"x":155,"y":27,"w":10,"h":23,"character":116,"shift":11,"offset":1,},
    "117": {"x":133,"y":27,"w":9,"h":23,"character":117,"shift":11,"offset":1,},
    "118": {"x":120,"y":27,"w":11,"h":23,"character":118,"shift":11,"offset":0,},
    "119": {"x":106,"y":27,"w":12,"h":23,"character":119,"shift":11,"offset":0,},
    "120": {"x":94,"y":27,"w":10,"h":23,"character":120,"shift":11,"offset":1,},
    "121": {"x":81,"y":27,"w":11,"h":23,"character":121,"shift":11,"offset":0,},
    "122": {"x":70,"y":27,"w":9,"h":23,"character":122,"shift":11,"offset":1,},
    "123": {"x":60,"y":27,"w":8,"h":23,"character":123,"shift":11,"offset":2,},
    "124": {"x":55,"y":27,"w":3,"h":23,"character":124,"shift":11,"offset":4,},
    "125": {"x":44,"y":27,"w":9,"h":23,"character":125,"shift":11,"offset":1,},
    "126": {"x":58,"y":102,"w":9,"h":23,"character":126,"shift":11,"offset":1,},
    "9647": {"x":69,"y":102,"w":12,"h":23,"character":9647,"shift":18,"offset":3,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":1025,"upper":1025,},
    {"lower":1040,"upper":1103,},
    {"lower":1105,"upper":1105,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": true,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "__gui__",
    "path": "folders/__gui__.yy",
  },
  "resourceVersion": "1.0",
  "name": "_gui_font_",
  "tags": [],
  "resourceType": "GMFont",
}